package ru.optap

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import androidx.work.WorkManager

class OptapBroadcastReceiver : BroadcastReceiver() {

    companion object {
        const val ACTION_CANCEL = "ru.optap.notifications.action_cancel"
        const val WORKER_TAG = "WORKER_TAG"
    }

    override fun onReceive(context: Context, intent: Intent) {
        val tag = intent.getStringExtra(WORKER_TAG)
        tag?.let { WorkManager.getInstance(context).cancelAllWorkByTag(it) }
    }

}