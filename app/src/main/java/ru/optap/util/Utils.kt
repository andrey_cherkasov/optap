package ru.optap.util

import android.annotation.SuppressLint
import android.content.Context
import android.database.Cursor
import android.net.Uri
import android.provider.MediaStore
import android.provider.Settings
import com.nguyenhoanglam.imagepicker.model.ImagePickerConfig
import ru.optap.R
import javax.inject.Inject

class Utils @Inject constructor(private val context: Context) {

    @SuppressLint("ResourceType")
    fun getSingleImagePickerConfig(): ImagePickerConfig {
        return ImagePickerConfig(
            statusBarColor = context.getString(R.color.primaryColor),
            toolbarColor = context.getString(R.color.primaryColor),
            backgroundColor = context.getString(R.color.white),
            selectedIndicatorColor = context.getString(R.color.secondaryColor),
            isMultipleMode = false,
            isFolderMode = true,
            isShowCamera = true
        )
    }

    @SuppressLint("ResourceType")
    fun getMultipleImagePickerConfig(): ImagePickerConfig {
        return ImagePickerConfig(
            statusBarColor = context.getString(R.color.primaryColor),
            toolbarColor = context.getString(R.color.primaryColor),
            backgroundColor = context.getString(R.color.white),
            selectedIndicatorColor = context.getString(R.color.secondaryColor),
            isMultipleMode = true,
            isFolderMode = true,
            isShowCamera = true
        )
    }

    @SuppressLint("HardwareIds")
    fun getDeviceId(): String =
        Settings.Secure.getString(context.contentResolver, Settings.Secure.ANDROID_ID)

    fun getRealPathFromUri(contentUri: Uri): String {
        var cursor: Cursor? = null
        return try {
            cursor = context.contentResolver.query(contentUri, null, null, null, null)
            val colIndex = cursor!!.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
            cursor.moveToFirst()
            cursor.getString(colIndex)
        } finally {
            cursor?.close()
        }
    }

}