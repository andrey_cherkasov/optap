package ru.optap.util

import com.google.firebase.messaging.FirebaseMessagingService
import dagger.hilt.android.AndroidEntryPoint
import ru.optap.data.local.SharedPreferencesHelper
import javax.inject.Inject

@AndroidEntryPoint
class OptapFirebaseMessagingService : FirebaseMessagingService() {

    @Inject
    lateinit var preferencesHelper: SharedPreferencesHelper

    override fun onNewToken(token: String) {
        preferencesHelper.saveFCMToken(token)
    }

}