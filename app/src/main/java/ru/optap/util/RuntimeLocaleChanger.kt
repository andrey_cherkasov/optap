package ru.optap.util

import android.content.Context
import android.content.res.Configuration
import dagger.hilt.EntryPoint
import dagger.hilt.InstallIn
import dagger.hilt.android.EntryPointAccessors
import dagger.hilt.components.SingletonComponent
import ru.optap.data.local.db.LocaleDao
import java.util.*

class RuntimeLocaleChanger {

    @EntryPoint
    @InstallIn(SingletonComponent::class)
    interface LocaleDaoEntryPoint {
        fun localeDao(): LocaleDao
    }

    fun wrapContext(context: Context): Context {
        val appContext = context.applicationContext ?: throw IllegalStateException()
        val hiltEntryPoint = EntryPointAccessors.fromApplication(
            appContext,
            LocaleDaoEntryPoint::class.java
        )

        val newConfig = Configuration()
        newConfig.setLocale(Locale(hiltEntryPoint.localeDao().getSelectedLocaleSimple()))
        return context.createConfigurationContext(newConfig)
    }

}