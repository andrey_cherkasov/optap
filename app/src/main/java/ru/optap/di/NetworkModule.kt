package ru.optap.di

import android.content.Context
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import ru.optap.BuildConfig
import ru.optap.data.local.SharedPreferencesHelper
import ru.optap.data.local.db.LocaleDao
import ru.optap.data.remote.interceptors.AuthInterceptor
import ru.optap.data.remote.interceptors.LoggingInterceptor
import ru.optap.data.remote.interceptors.NetworkConnectionInterceptor
import ru.optap.util.Utils
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class NetworkModule {

    @Provides
    @Singleton
    fun provideLoggingInterceptor() = LoggingInterceptor()

    @Provides
    @Singleton
    fun provideNetworkConnectionInterceptor(@ApplicationContext context: Context) =
        NetworkConnectionInterceptor(context)

    @Provides
    @Singleton
    fun provideAuthInterceptor(
        preferencesHelper: SharedPreferencesHelper,
        localeDao: LocaleDao,
        utils: Utils
    ) = AuthInterceptor(preferencesHelper, localeDao, utils)

    @Provides
    @Singleton
    fun provideOkhttpClient(
        networkConnectionInterceptor: NetworkConnectionInterceptor,
        authInterceptor: AuthInterceptor,
        loggingInterceptor: LoggingInterceptor
    ) = OkHttpClient.Builder()
        .addInterceptor(networkConnectionInterceptor)
        .addInterceptor(authInterceptor)
        .addInterceptor(loggingInterceptor)
        .build()

    @Provides
    @Singleton
    fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit = Retrofit.Builder()
        .baseUrl(BuildConfig.BASE_URL)
        .client(okHttpClient)
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(MoshiConverterFactory.create())
        .build()

}