package ru.optap.di

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import ru.optap.BuildConfig
import ru.optap.data.local.db.OptapDatabase
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class DataBaseModule {

    @Singleton
    @Provides
    fun provideOptapDatabase(@ApplicationContext context: Context): OptapDatabase =
        Room.databaseBuilder(context, OptapDatabase::class.java, BuildConfig.APP_DB_NAME)
            .allowMainThreadQueries().build()

}