package ru.optap.di

import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import ru.optap.data.local.db.CatalogDao
import ru.optap.data.local.db.CategoryDao
import ru.optap.data.local.db.OptapDatabase
import ru.optap.data.local.db.ProductDao
import ru.optap.data.remote.AuthApiInterface
import ru.optap.data.remote.DataApiInterface
import ru.optap.ui.cart.CartRepository
import ru.optap.ui.cart.data.OrderJsonMapper
import ru.optap.ui.catalog.data.CategoryMapper
import ru.optap.ui.catalogs.data.CatalogMapper
import ru.optap.ui.category.data.ProductMapper
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class AppDataModule {

    @Singleton
    @Provides
    fun provideAuthApiInterface(retrofit: Retrofit): AuthApiInterface =
        retrofit.create(AuthApiInterface::class.java)

    @Singleton
    @Provides
    fun provideDataApiInterface(retrofit: Retrofit): DataApiInterface =
        retrofit.create(DataApiInterface::class.java)

    @Singleton
    @Provides
    fun provideCartRepository(
        moshi: Moshi,
        productDao: ProductDao,
        categoryDao: CategoryDao,
        catalogDao: CatalogDao,
        categoryMapper: CategoryMapper,
        productMapper: ProductMapper,
        catalogMapper: CatalogMapper,
        orderJsonMapper: OrderJsonMapper,
        dataApi: DataApiInterface
    ) = CartRepository(
        moshi,
        productDao,
        categoryDao,
        catalogDao,
        categoryMapper,
        productMapper,
        catalogMapper,
        orderJsonMapper,
        dataApi
    )

    @Singleton
    @Provides
    fun provideLocaleDao(dataBase: OptapDatabase) = dataBase.localeDao()

    @Singleton
    @Provides
    fun provideCountryDao(dataBase: OptapDatabase) = dataBase.countryDao()

    @Singleton
    @Provides
    fun provideImageProductDao(dataBase: OptapDatabase) = dataBase.imageProductDao()

    @Singleton
    @Provides
    fun provideProductDao(dataBase: OptapDatabase) = dataBase.productDao()

    @Singleton
    @Provides
    fun provideCategoryDao(dataBase: OptapDatabase) = dataBase.categoryDao()

    @Singleton
    @Provides
    fun provideCatalogDao(dataBase: OptapDatabase) = dataBase.catalogDao()

}