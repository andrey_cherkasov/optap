package ru.optap.di

import android.content.Context
import com.pddstudio.preferences.encrypted.EncryptedPreferences
import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import ru.optap.BuildConfig
import ru.optap.data.local.SharedPreferencesHelper
import ru.optap.ui.cart.data.OrderJsonMapper
import ru.optap.ui.catalog.data.CategoryMapper
import ru.optap.ui.catalogs.data.CatalogMapper
import ru.optap.ui.category.data.ProductMapper
import ru.optap.ui.customers.data.CustomerMapper
import ru.optap.ui.order.data.OrderDetailMapper
import ru.optap.ui.orders.data.OrderMapper
import ru.optap.util.Utils
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class SystemServiceModule {

    @Provides
    @Singleton
    fun provideEncryptedSharedPreferences(@ApplicationContext context: Context): EncryptedPreferences =
        EncryptedPreferences.Builder(context)
            .withEncryptionPassword(BuildConfig.SHARED_PREFERENCES_PASSWORD).build()

    @Provides
    @Singleton
    fun provideEncryptedSharedPreferencesEditor(
        encryptedPreferences: EncryptedPreferences
    ): EncryptedPreferences.EncryptedEditor = encryptedPreferences.edit()

    @Provides
    @Singleton
    fun provideUtils(@ApplicationContext context: Context) = Utils(context)

    @Provides
    @Singleton
    fun provideMoshi(): Moshi = Moshi.Builder().build()

    @Singleton
    @Provides
    fun provideCategoryMapper() = CategoryMapper()

    @Singleton
    @Provides
    fun provideProductMapper() = ProductMapper()

    @Singleton
    @Provides
    fun provideCustomerMapper() = CustomerMapper()

    @Singleton
    @Provides
    fun provideOrderMapper() = OrderMapper()

    @Singleton
    @Provides
    fun provideOrderDetailMapper(
        categoryMapper: CategoryMapper,
        productMapper: ProductMapper,
        preferencesHelper: SharedPreferencesHelper
    ) = OrderDetailMapper(categoryMapper, productMapper, preferencesHelper)

    @Singleton
    @Provides
    fun provideCatalogMapper() = CatalogMapper()

    @Singleton
    @Provides
    fun provideOrderJsonMapper() = OrderJsonMapper()

}