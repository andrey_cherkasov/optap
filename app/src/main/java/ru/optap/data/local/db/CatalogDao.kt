package ru.optap.data.local.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import ru.optap.ui.catalogs.data.CatalogDTO

@Dao
interface CatalogDao {

    @Insert
    fun insert(catalog: CatalogDTO)

    @Query("SELECT * FROM Catalog LIMIT 1")
    fun get(): CatalogDTO

    @Query("DELETE FROM Catalog")
    fun clear()

}