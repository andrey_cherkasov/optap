package ru.optap.data.local.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import io.reactivex.Completable
import io.reactivex.Single
import ru.optap.ui.category.data.ProductDTO

@Dao
interface ProductDao {

    @Query("SELECT * FROM Products")
    fun getAll(): List<ProductDTO>

    @Query("SELECT * FROM Products WHERE category_id = :categoryId")
    fun getAll(categoryId: Int): List<ProductDTO>

    @Query("SELECT * FROM Products WHERE id = :id")
    fun get(id: Int): ProductDTO?

    @Query("SELECT SUM(count * price) FROM Products")
    fun getTotal(): Single<Int>

    @Insert
    fun insert(product: ProductDTO)

    @Update
    fun update(product: ProductDTO): Completable

    @Query("DELETE FROM Products WHERE id = :id")
    fun delete(id: Int)

    @Query("DELETE FROM Products WHERE category_id = :categoryId")
    fun deleteByCategory(categoryId: Int)

    @Query("DELETE FROM Products")
    fun clear()

}