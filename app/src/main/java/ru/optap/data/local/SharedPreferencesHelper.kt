package ru.optap.data.local

import com.pddstudio.preferences.encrypted.EncryptedPreferences
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import ru.optap.data.model.User
import java.io.IOException
import javax.inject.Inject

class SharedPreferencesHelper @Inject constructor(
    private val encryptedPreferences: EncryptedPreferences,
    private val encryptedEditor: EncryptedPreferences.EncryptedEditor,
    private val moshi: Moshi
) {

    companion object {
        private const val USER = "USER"
        private const val FIRST_LAUNCH = "FIRST_LAUNCH"
        private const val FCM_TOKEN = "FCM_TOKEN"
    }

    fun saveUser(user: User) {
        val adapter: JsonAdapter<User> = moshi.adapter(User::class.java)
        encryptedEditor.putString(USER, adapter.toJson(user)).apply()
    }

    fun loadUser(): User? {
        val adapter: JsonAdapter<User> = moshi.adapter(User::class.java)
        val userJsonString = encryptedPreferences.getString(USER, "")
        return try {
            adapter.fromJson(userJsonString)
        } catch (e: IOException) {
            null
        }
    }

    fun deleteUser() {
        encryptedEditor.putString(USER, "").apply()
    }

    fun saveFCMToken(token: String) {
        encryptedEditor.putString(FCM_TOKEN, token).apply()
    }

    fun loadFCMToken(): String {
        return encryptedPreferences.getString(FCM_TOKEN, "")
    }

    fun isFirstLaunch(): Boolean {
        return encryptedPreferences.getBoolean(FIRST_LAUNCH, true)
    }

    fun setFirstLaunch() {
        encryptedEditor.putBoolean(FIRST_LAUNCH, false).apply()
    }

}