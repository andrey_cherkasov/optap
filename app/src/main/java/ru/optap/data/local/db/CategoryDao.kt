package ru.optap.data.local.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import ru.optap.ui.catalog.data.CategoryDTO

@Dao
interface CategoryDao {

    @Query("SELECT * FROM Categories")
    fun getAll(): List<CategoryDTO>

    @Query("SELECT * FROM Categories WHERE id = :id")
    fun get(id: Int): CategoryDTO?

    @Insert
    fun insert(category: CategoryDTO)

    @Query("DELETE FROM Categories WHERE id = :id")
    fun delete(id: Int)

    @Query("DELETE FROM Categories")
    fun clear()

}