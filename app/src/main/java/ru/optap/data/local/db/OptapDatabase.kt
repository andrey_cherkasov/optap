package ru.optap.data.local.db

import androidx.room.Database
import androidx.room.RoomDatabase
import ru.optap.data.model.ImageProduct
import ru.optap.ui.catalog.data.CategoryDTO
import ru.optap.ui.catalogs.data.CatalogDTO
import ru.optap.ui.category.data.ProductDTO
import ru.optap.ui.dialog.country.data.Country
import ru.optap.ui.dialog.locale.data.Locale


@Database(
    entities = [
        Locale::class,
        Country::class,
        ImageProduct::class,
        ProductDTO::class,
        CategoryDTO::class,
        CatalogDTO::class
    ],
    version = 1
)
abstract class OptapDatabase : RoomDatabase() {

    abstract fun localeDao(): LocaleDao

    abstract fun countryDao(): CountryDao

    abstract fun imageProductDao(): ImageProductDao

    abstract fun productDao(): ProductDao

    abstract fun categoryDao(): CategoryDao

    abstract fun catalogDao(): CatalogDao

}