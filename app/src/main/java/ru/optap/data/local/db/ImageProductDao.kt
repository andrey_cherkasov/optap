package ru.optap.data.local.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ru.optap.data.model.ImageProduct

@Dao
interface ImageProductDao {

    @Query("SELECT * FROM ImageProduct WHERE category_id = :categoryId")
    fun getAll(categoryId: Int): List<ImageProduct>

    @Query("SELECT * FROM ImageProduct WHERE (status = 0 OR status = 3) AND category_id = :categoryId")
    fun getUnUploaded(categoryId: Int): MutableList<ImageProduct>

    @Query("UPDATE ImageProduct SET status = :status WHERE id = :id")
    fun setStatus(id: Int, status: Int)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(items: List<ImageProduct>)

    @Query("DELETE FROM ImageProduct WHERE category_id = :categoryId")
    fun deleteByCategory(categoryId: Int)

}