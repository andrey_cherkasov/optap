package ru.optap.data.local.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Completable
import io.reactivex.Observable
import ru.optap.ui.dialog.country.data.Country

@Dao
interface CountryDao {

    @Query("SELECT * FROM Country")
    fun getCountries(): Observable<List<Country>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(countries: List<Country>): Completable

    @Query("SELECT * FROM Country WHERE code = :code")
    fun getCountry(code: String): Country?
}