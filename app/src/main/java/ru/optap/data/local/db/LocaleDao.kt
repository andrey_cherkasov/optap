package ru.optap.data.local.db

import androidx.room.*
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import ru.optap.ui.dialog.locale.data.Locale

@Dao
interface LocaleDao {

    @Query("SELECT * FROM locale")
    fun getAll(): Observable<List<Locale>>

    @Query(
        "UPDATE locale SET selected = ABS(selected - 1) WHERE id IN" +
                "(SELECT id FROM locale WHERE selected = 1 OR id = :selectedLocaleId)"
    )
    fun setSelected(selectedLocaleId: Int): Completable

    @Query("SELECT locale FROM locale WHERE selected = 1")
    fun getSelectedLocaleSimple(): String

    @Query("SELECT locale FROM locale WHERE selected = 1")
    fun getSelectedLocale(): Single<String>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(locales: List<Locale>): Completable

}