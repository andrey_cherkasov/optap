package ru.optap.data.remote.interceptors

import android.util.Log
import io.reactivex.schedulers.Schedulers
import okhttp3.Interceptor
import okhttp3.Response
import ru.optap.data.local.SharedPreferencesHelper
import ru.optap.data.local.db.LocaleDao
import ru.optap.util.Utils
import javax.inject.Inject

class AuthInterceptor @Inject constructor(
    private val preferencesHelper: SharedPreferencesHelper,
    private val localeDao: LocaleDao,
    private val utils: Utils
) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val user = preferencesHelper.loadUser()
        val newUrlBuilder = request.url.newBuilder()

        if (user != null) {
            newUrlBuilder.addQueryParameter("user_id", user.id.toString())
                .addQueryParameter("access_token", user.token)
                .addQueryParameter("catalog_id", user.catalogId.toString())
        }

        newUrlBuilder.apply {
            addQueryParameter(
                "lang",
                localeDao.getSelectedLocale().subscribeOn(Schedulers.io()).blockingGet()
            )
            addQueryParameter("device_id", utils.getDeviceId())
        }


        return chain.proceed(
            request.newBuilder()
                .url(newUrlBuilder.build())
                .build()
        )
    }
}