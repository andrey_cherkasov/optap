package ru.optap.data.remote

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.hilt.work.HiltWorker
import androidx.work.Worker
import androidx.work.WorkerParameters
import dagger.assisted.Assisted
import dagger.assisted.AssistedInject
import io.reactivex.disposables.CompositeDisposable
import okhttp3.MultipartBody
import ru.optap.BuildConfig
import ru.optap.OptapBroadcastReceiver
import ru.optap.R
import ru.optap.data.local.db.ImageProductDao
import ru.optap.data.model.ImageProduct
import ru.optap.ui.base.exception.ApiErrorException
import ru.optap.ui.base.exception.ServerErrorException
import ru.optap.ui.category.CategoryActivity
import ru.optap.ui.category.data.ProductMapper
import java.io.File


@HiltWorker
class CreateProductWorker @AssistedInject constructor(
    @Assisted val context: Context,
    @Assisted workerParams: WorkerParameters,
    private val dataApi: DataApiInterface,
    private val imageProductDao: ImageProductDao,
    private val productMapper: ProductMapper
) : Worker(context, workerParams) {

    companion object {
        const val CATEGORY_ID = "CATEGORY_ID"
    }

    private var categoryId: Int = 0
    private val disposable = CompositeDisposable()
    private lateinit var cancelPendingIntent: PendingIntent
    private val notificationManager: NotificationManager =
        context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager


    override fun doWork(): Result {
        categoryId = inputData.getInt(CATEGORY_ID, 0)

        // init cancel intent
        cancelPendingIntent = PendingIntent.getBroadcast(
            context,
            0,
            Intent(context, OptapBroadcastReceiver::class.java).apply {
                action = OptapBroadcastReceiver.ACTION_CANCEL
                putExtra(OptapBroadcastReceiver.WORKER_TAG, categoryId.toString())
            },
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) PendingIntent.FLAG_IMMUTABLE else 0
        )

        // init notification channel
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                categoryId.toString(),
                BuildConfig.APPLICATION_ID,
                NotificationManager.IMPORTANCE_LOW
            )
            notificationManager.createNotificationChannel(channel)
        }

        // init notification
        val builder = NotificationCompat.Builder(context, categoryId.toString())
            .setSmallIcon(R.drawable.ic_upload)
            .setContentTitle(context.getString(R.string.notification_title))
            .setOngoing(true)
            .setOnlyAlertOnce(true)
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            .addAction(
                R.drawable.ic_close,
                context.getString(R.string.cancel),
                cancelPendingIntent
            )

        var unUploadedImageProducts = imageProductDao.getUnUploaded(categoryId)
        var progress = 0
        while (unUploadedImageProducts.isNotEmpty() && !isStopped) {
            val imageFile = File(unUploadedImageProducts[0].image)
            imageProductDao.setStatus(unUploadedImageProducts[0].id, ImageProduct.IN_HANDLING)

            val requestBody = ProgressEmittingRequestBody("", imageFile)
            val picture = MultipartBody.Part.createFormData("picture", imageFile.name, requestBody)
            val createProductCall = dataApi.createProduct(categoryId, picture)
            disposable.add(
                requestBody.progressSubject.subscribe({
                    if (!isStopped) {
                        if (progress != it) {
                            progress = it
                            builder.setContentText(
                                context.getString(
                                    R.string.notification_text,
                                    unUploadedImageProducts.size
                                )
                            ).setProgress(100, progress, false)
                            notificationManager.notify(categoryId, builder.build())
                        }
                    } else {
                        createProductCall.cancel()
                        disposable.dispose()
                        disposable.clear()
                        imageProductDao.deleteByCategory(categoryId)
                        notificationManager.cancel(categoryId)
                    }
                }, { t -> t.printStackTrace() })
            )

            try {
                val result = createProductCall.execute()
                if (!result.isSuccessful) throw ServerErrorException()
                if (result.body() != null && !result.body()!!.meta.success)
                    throw ApiErrorException(result.body()!!.meta.message)

                val product = productMapper.ToVo().map(result.body()!!.data!!)
                imageProductDao.setStatus(unUploadedImageProducts[0].id, ImageProduct.UPLOADED)

                val productIntent = Intent().apply {
                    action = CategoryActivity.ACTION_NEW_PRODUCT + categoryId
                    putExtra(CategoryActivity.PRODUCT, product)
                }

                context.sendBroadcast(productIntent)
            } catch (e: Exception) {
                imageProductDao.setStatus(unUploadedImageProducts[0].id, ImageProduct.ERROR)
                return Result.retry()
            }
            unUploadedImageProducts = imageProductDao.getUnUploaded(categoryId)
        }

        imageProductDao.deleteByCategory(categoryId)
        notificationManager.cancel(categoryId)
        disposable.clear()

        return Result.success()
    }

}