package ru.optap.data.remote

import io.reactivex.Single
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.http.*
import ru.optap.data.model.DataList
import ru.optap.data.model.Response
import ru.optap.data.model.ResponseData
import ru.optap.data.model.Subscription
import ru.optap.ui.catalog.data.Category
import ru.optap.ui.catalogs.data.Catalog
import ru.optap.ui.category.data.Product
import ru.optap.ui.customers.data.Customer
import ru.optap.ui.order.data.OrderDetail
import ru.optap.ui.orders.data.Order

interface DataApiInterface {

    @GET("/categories")
    fun getVendorCatalog(
        @Query("page") page: Int,
        @Query("is_deleted") showDeletedItems: Int,
    ): Single<ResponseData<DataList<Category>>>


    @GET("/customers/catalogs/{id}/categories")
    fun getCatalog(
        @Path("id") catalogId: Int,
        @Query("page") page: Int
    ): Single<ResponseData<DataList<Category>>>

    @Multipart
    @POST("/categories/update/{id}")
    fun updateCategory(
        @Path("id") id: Int,
        @Query("parent_id") parentId: Int,
        @Query("name") name: String,
        @Query("description") description: String,
        @Query("price") price: Int,
        @Query("pack_count") packCount: Int,
        @Query("box_count") boxCount: Int,
        @Query("apply_old") applyOld: Boolean,
        @Part file: MultipartBody.Part
    ): Single<ResponseData<Category>>

    @GET("categories/{category_id}/categories")
    fun getSubCategories(@Path("category_id") id: Int): Single<ResponseData<DataList<Category>>>

    @GET("/categories/{id}/products/get")
    fun getProducts(
        @Path("id") id: Int,
        @Query("page") page: Int,
        @Query("is_deleted") showDeletedItems: Int,
    ): Single<ResponseData<DataList<Product>>>

    @Multipart
    @POST("/categories/{id}/products/create")
    fun createProduct(
        @Path("id") categoryId: Int,
        @Part file: MultipartBody.Part,
    ): Call<ResponseData<Product>>

    @Multipart
    @POST("/categories/{id}/products/create")
    fun createProductRx(
        @Path("id") categoryId: Int,
        @Part file: MultipartBody.Part,
    ): Single<ResponseData<Product>>

    @GET("/categories/{id}/products/remove")
    fun deleteProducts(
        @Path("id") categoryId: Int,
        @Query("item_ids[]") itemIds: List<Int>
    ): Single<Response>

    @GET("/categories/{id}/products/restore")
    fun restoreProducts(
        @Path("id") categoryId: Int,
        @Query("item_ids[]") itemIds: List<Int>,
    ): Single<Response>

    @GET("/categories/remove/{id}")
    fun deleteCategory(@Path("id") id: Int): Single<Response>

    @GET("categories/restore/{id}")
    fun restoreCategory(@Path("id") id: Int): Single<Response>

    @GET("/categories/{id}/products/{product_id}/update")
    fun updateProduct(
        @Path("product_id") productId: Int,
        @Path("id") categoryId: Int,
        @Query("name") name: String,
        @Query("description") description: String,
        @Query("pack_count") packCount: Int,
        @Query("box_count") boxCount: Int,
        @Query("price") price: Int
    ): Single<ResponseData<Product>>

    @GET("/vendors/customers/")
    fun getCustomers(@Query("page") page: Int): Single<ResponseData<DataList<Customer>>>

    @POST("/vendors/invite/{phone}")
    fun sendSms(@Path("phone") phone: String, @Query("message") message: String): Single<Response>

    @GET("/vendors/customers/remove/{user_id}")
    fun deleteCustomer(@Path("user_id") userId: Int): Single<Response>

    @GET("/orders")
    fun getOrders(@Query("page") page: Int): Single<ResponseData<DataList<Order>>>

    @GET("/orders/{id}/remove")
    fun deleteOrder(@Path("id") id: Int): Single<Response>

    @POST("/orders/invoice/{id}/send")
    fun sendOrderToEmail(@Path("id") id: Int): Single<Response>

    @GET("/orders/{id}/get")
    fun getOrder(@Path("id") id: Int): Single<ResponseData<OrderDetail>>

    @GET("/orders/{id}/{item_id}/delete")
    fun deleteOrderProduct(
        @Path("id") orderId: Int,
        @Path("item_id") productId: Int
    ): Call<Response>

    @GET("/orders/{id}/category/{category_id}/delete")
    fun deleteOrderCategory(
        @Path("id") orderId: Int,
        @Path("category_id") categoryId: Int
    ): Call<Response>

    @GET("/orders/{id}/accept")
    fun acceptOrder(@Path("id") id: Int): Single<Response>

    @GET("/orders/{id}/{item_id}/edit")
    fun editOrderProduct(
        @Path("id") orderId: Int,
        @Path("item_id") itemId: Int,
        @Query("count") count: Int,
        @Query("count_type") countType: Int
    ): Single<Response>

    @GET("/customers/catalogs/favorites")
    fun getFavoriteCatalogs(@Query("page") page: Int): Single<ResponseData<DataList<Catalog>>>

    @GET("/customers/catalogs/favorites/remove/{catalog_id}")
    fun deleteFavoriteCatalog(@Path("catalog_id") catalogId: Int): Single<Response>

    @GET("customers/catalogs/subscription/check/{id}")
    fun checkSubscription(@Path("id") id: Int): Single<ResponseData<Subscription>>

    @GET("customers/catalogs/subscription/change/{id}")
    fun performSubscription(@Path("id") id: Int): Single<ResponseData<Subscription>>

    @GET("/customers/catalogs/favorites/check/{catalog_id}")
    fun checkFavorite(@Path("catalog_id") catalogId: Int): Single<Response>

    @GET("/customers/catalogs/favorites/add/{id}")
    fun addToFavorites(@Path("id") catalogId: Int): Single<Response>

    @GET("/customers/catalogs/favorites/remove/{catalog_id}")
    fun deleteFavorites(@Path("catalog_id") catalogId: Int): Single<Response>

    @POST("/orders/add")
    fun makeOrder(@Query("item_ids") json: String): Call<Response>

    @GET("/customers/catalogs/latest")
    fun getLatestCatalogs(): Single<ResponseData<DataList<Catalog>>>

    @GET("/customers/categories/search")
    fun searchCategories(
        @Query("search") search: String,
        @Query("page") page: Int
    ): Call<ResponseData<DataList<Category>>>

    @GET("/customers/catalogs/search")
    fun searchCatalogs(@Query("s") search: String): Call<ResponseData<DataList<Catalog>>>

}