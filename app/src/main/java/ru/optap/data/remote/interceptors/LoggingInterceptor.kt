package ru.optap.data.remote.interceptors

import android.util.Log
import okhttp3.Interceptor
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import ru.optap.BuildConfig

class LoggingInterceptor(
    private val interceptor: HttpLoggingInterceptor = HttpLoggingInterceptor()
        .setLevel(if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE)
) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        return interceptor.intercept(chain)
    }

}