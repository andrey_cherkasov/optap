package ru.optap.data.remote

import io.reactivex.Single
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.http.*
import ru.optap.data.model.Response
import ru.optap.data.model.ResponseData
import ru.optap.data.model.User

interface AuthApiInterface {

    @POST("/auth/login")
    fun login(
        @Query("country_id") countryId: String,
        @Query("username") username: String,
        @Query("password") password: String,
        @Query("fcm_token") token: String
    ): Single<ResponseData<User>>

    @POST("/auth/registration")
    fun registration(
        @Query("country_id") countryId: String,
        @Query("username") username: String,
        @Query("fullname") fullName: String,
        @Query("password") password: String,
        @Query("role_id") userType: Int,
        @Query("center_id") centerId: Int,
        @Query("tradecenter_code") inviteCode: String,
        @Query("pavilion_number") address: String,
        @Query("catalog_type") catalogType: Int,
        @Query("fcm_token") token: String
    ): Single<ResponseData<User>>

    @GET("/auth/activate/{phone}/{code}")
    fun activateAccount(
        @Path("phone") phone: String,
        @Path("code") code: String,
    ): Single<Response>

    @POST("/auth/restore/{phone}")
    fun restore(@Path("phone") phone: String): Single<Response>

    @GET("/terms-json")
    fun termsVendor(): Single<ResponseData<String>>

    @GET("/terms-json-buyers/")
    fun termsCustomer(): Single<ResponseData<String>>

    @GET("/users/info")
    fun getUser(): Call<ResponseData<User>>

    @Multipart
    @POST("/users/update")
    fun saveUser(
        @Query("email") email: String,
        @Query("name") name: String,
        @Query("city") city: String,
        @Query("catalog_type") catalogType: Int,
        @Query("search_show") searchShow: Int,
        @Query("pavilion_number") address: String,
        @Query("center_id") centerId: Int,
        @Part file: MultipartBody.Part
    ): Single<ResponseData<User>>

    @POST("/users/password/change")
    fun changePassword(
        @Query("oldpass") currentPassword: String,
        @Query("newpass") newPassword: String,
    ): Single<Response>

    @POST("/users/remove")
    fun deleteAccount(): Single<Response>

}