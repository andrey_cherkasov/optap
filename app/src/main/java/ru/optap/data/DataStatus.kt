package ru.optap.data

enum class DataStatus {
    LOADING,
    SUCCESS,
    ERROR
}