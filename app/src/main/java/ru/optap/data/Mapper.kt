package ru.optap.data

interface Mapper<I, O> {
    fun map(input: I): O
}