package ru.optap.data

import ru.optap.data.DataStatus.LOADING
import ru.optap.data.DataStatus.SUCCESS
import ru.optap.data.DataStatus.ERROR

data class Resource<out T>(val status: DataStatus, val data: T?, val throwable: Throwable?) {
    companion object {
        fun <T> success(data: T?): Resource<T> = Resource(SUCCESS, data, null)
        fun <T> error(error: Throwable, data: T?): Resource<T> = Resource(ERROR, data, error)
        fun <T> loading(data: T?): Resource<T> = Resource(LOADING, data, null)
    }
}