package ru.optap.data

import ru.optap.ui.dialog.country.data.Country
import ru.optap.ui.dialog.locale.data.Locale

class DataGenerator {

    companion object {
        fun getAvailableLocales(): List<Locale> {
            var locale: String = java.util.Locale.getDefault().language
            locale = if ("en, ru, zh, vi".contains(locale, ignoreCase = true)) locale else "en"
            return listOf(
                Locale(1, "English", "en", "en" == locale),
                Locale(2, "Русский", "ru", "ru" == locale),
                Locale(3, "中文", "zh", "zh" == locale),
                Locale(4, "Tiếng Việt", "vi", "vi" == locale)
            )
        }

        fun getCountries(): List<Country> {
            return listOf(
                Country(1, "Россия", "RU", "7", "ic_ru"),
                Country(2, "Беларусь", "BY", "375", "ic_by"),
                Country(3, "Україна", "UA", "380", "ic_ua"),
                Country(4, "Cuba", "CU", "53", "ic_cu"),
                Country(5, "中华人民共和国", "CN", "86", "ic_cn"),
            )
        }
    }

}