package ru.optap.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class ImageProduct(
    @PrimaryKey(autoGenerate = true) val id: Int,
    @ColumnInfo(name = "image") val image: String,
    @ColumnInfo(name = "category_id") val categoryId: Int,
    @ColumnInfo(name = "status") val status: Int
) {
    companion object {
        const val CREATED = 0
        const val IN_HANDLING = 1
        const val UPLOADED = 2
        const val ERROR = 3
    }
}