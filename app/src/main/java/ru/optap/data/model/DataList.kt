package ru.optap.data.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import ru.optap.ui.base.listview.PaginationScrollListener

@JsonClass(generateAdapter = true)
data class DataList<T>(
    @Json(name = "data") val dataList: List<T>,
    @Json(name = "curr_page") val page: Int,
    @Json(name = "total") val total: Int,
) {
    val lastPage: Int get() = total / PaginationScrollListener.ITEMS_PER_PAGE + 1
}