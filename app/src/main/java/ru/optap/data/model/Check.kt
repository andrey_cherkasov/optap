package ru.optap.data.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Check(
    @Json(name = "is_active") var isActive: Int
)
