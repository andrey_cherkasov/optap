package ru.optap.data.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Meta(
    @Json(name = "success") var success: Boolean,
    @Json(name = "message") var message: String
)