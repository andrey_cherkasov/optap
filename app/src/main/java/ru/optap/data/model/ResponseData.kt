package ru.optap.data.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ResponseData<T>(
    @Json(name = "meta") val meta: Meta,
    @Json(name = "data") val data: T?
)