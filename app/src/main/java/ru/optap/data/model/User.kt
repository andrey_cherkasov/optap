package ru.optap.data.model

import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.parcelize.Parcelize


@Parcelize
@JsonClass(generateAdapter = true)
data class User(
    @Json(name = "id") val id: Int,
    @Json(name = "email") val email: String?,
    @Json(name = "username") val userName: String,
    @Json(name = "fullname") val fullName: String,
    @Json(name = "access_token") val token: String?,
    @Json(name = "role_id") val role: Int,
    @Json(name = "catalog_id") val catalogId: Int?,
    @Json(name = "center_id") val centerId: Int?,
    @Json(name = "catalog_type") val catalogType: Int?,
    @Json(name = "city") val city: String?,
    @Json(name = "image") val image: String?,
    @Json(name = "search_show") val searchShow: Int?,
    @Json(name = "pavilion_number") val pavilionNumber: String?,
    @Json(name = "blocked") val blocked: Int?
) : Parcelable {

    companion object {
        const val CUSTOMER = 1
        const val VENDOR = 2
    }

}