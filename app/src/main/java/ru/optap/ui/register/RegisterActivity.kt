package ru.optap.ui.register

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.SpannableString
import android.text.Spanned
import android.text.method.LinkMovementMethod
import android.text.method.PasswordTransformationMethod
import android.text.style.ClickableSpan
import android.view.LayoutInflater
import android.view.View
import androidx.activity.viewModels
import androidx.core.view.isVisible
import dagger.hilt.android.AndroidEntryPoint
import ru.optap.R
import ru.optap.data.DataStatus
import ru.optap.ui.dialog.country.data.Country
import ru.optap.data.model.User
import ru.optap.databinding.ActivityRegisterBinding
import ru.optap.ui.base.view.BaseActivity
import ru.optap.ui.base.view.LoadingView
import ru.optap.ui.dialog.country.CountryDialog
import ru.optap.ui.dialog.country.CountryDialogViewModel
import ru.optap.ui.dialog.locale.LocaleDialog
import ru.optap.ui.dialog.locale.LocaleDialogViewModel
import ru.optap.ui.dialog.terms.TermsOfUseDialog
import ru.optap.ui.login.LoginActivity
import ru.optap.ui.main.MainActivity
import ru.optap.ui.restore.RestoreActivity
import ru.tinkoff.decoro.MaskImpl
import ru.tinkoff.decoro.parser.UnderscoreDigitSlotsParser
import ru.tinkoff.decoro.slots.Slot
import ru.tinkoff.decoro.watchers.MaskFormatWatcher
import java.util.*

@AndroidEntryPoint
class RegisterActivity : BaseActivity(), RegisterView, LoadingView<User> {

    private lateinit var binding: ActivityRegisterBinding
    private val viewModel: RegisterViewModel by viewModels()
    private val localeViewModel: LocaleDialogViewModel by viewModels()
    private val countryViewModel: CountryDialogViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRegisterBinding.inflate(LayoutInflater.from(this))
        setContentView(binding.root)

        val slots: Array<Slot> = UnderscoreDigitSlotsParser().parseSlots("(___) ___ - __ - __")
        val mask: MaskImpl = MaskImpl.createTerminated(slots)
        mask.isHideHardcodedHead = true
        MaskFormatWatcher(mask).installOn(binding.phoneEditText)

        binding.passwordEditText.transformationMethod = PasswordTransformationMethod()

        val spannableString = SpannableString(getString(R.string.personal_data_processing))
        spannableString.setSpan(
            object : ClickableSpan() {
                override fun onClick(widget: View) = openTermsOfUseDialog()
            },
            resources.getInteger(R.integer.terms_span_start),
            resources.getInteger(R.integer.terms_span_end),
            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        binding.personalDataTextView.text = spannableString
        binding.personalDataTextView.movementMethod = LinkMovementMethod.getInstance()

        binding.signInButton.setOnClickListener { openLoginScreen() }
        binding.restoreButton.setOnClickListener { openRestoreScreen() }
        binding.supportTelTextView.setOnClickListener { openDialer() }
        binding.langImageButton.setOnClickListener { openLangDialog() }
        binding.countryLogo.setOnClickListener { openCountryDialog() }
        binding.personalDataCheckBox.setOnCheckedChangeListener { _, isChecked ->
            viewModel.setAgreement(isChecked)
        }
        binding.userTypeRadioGroup.setOnCheckedChangeListener { _, checkedId ->
            viewModel.setUserType(
                if (checkedId == R.id.customerRadioButton) User.CUSTOMER else User.VENDOR
            )
        }

        binding.signUpButton.setOnClickListener {
            viewModel.register(
                name = binding.nameEditText.text.toString(),
                city = binding.cityEditText.text.toString(),
                countryId = countryViewModel.getSelectCountry().value!!.code,
                phone = binding.phoneEditText.text.toString(),
                password = binding.passwordEditText.text.toString(),
                inviteCode = binding.inviteEditText.text.toString(),
                address = binding.addressEditText.text.toString(),
                catalogType = 0
            )
        }

        viewModel.getUserType().observe(this) {
            binding.catalogTypeGroup.isVisible = it == User.VENDOR
        }
        viewModel.getAgreement().observe(this) { binding.signUpButton.isEnabled = it }
        viewModel.getValidation().observe(this) { showValidationErrors(it) }
        viewModel.getUser().observe(this) {
            when (it.status) {
                DataStatus.LOADING -> showLoading(true)
                DataStatus.SUCCESS -> it.data?.let { it1 -> setData(it1) }
                DataStatus.ERROR -> handleError(it.throwable)
            }
        }

        localeViewModel.getLocales().observe(this) {
            binding.langImageButton.visibility = View.VISIBLE
        }
        countryViewModel.getCountries().observe(this) {
            binding.countryLogo.visibility = View.VISIBLE
        }

        countryViewModel.getSelectCountry().observe(this) { changeCountry(it) }
    }

    override fun onResume() {
        super.onResume()
        binding.phoneInputLayout.hint = getString(R.string.phone)
        binding.passwordInputLayout.hint = getString(R.string.password)
        viewModel.getValidation().value?.let { showValidationErrors(it) }
    }

    override fun showLoading(loading: Boolean) {
        binding.loadingLayout.root.isVisible = loading
    }

    override fun setData(data: User) {}

    override fun showValidationErrors(validation: RegisterValidationState) {
        binding.nameInputLayout.error =
            if (validation.name) "" else getString(R.string.invalid_field_empty)

        binding.cityInputLayout.error =
            if (validation.city) "" else getString(R.string.invalid_field_empty)

        binding.phoneInputLayout.error =
            if (validation.phone) "" else getString(R.string.invalid_phone)

        binding.passwordInputLayout.error =
            if (validation.password) "" else getString(R.string.invalid_password)

        binding.inviteInputLayout.error =
            if (validation.inviteCode) "" else getString(R.string.invalid_field_empty)

        binding.addressInputLayout.error =
            if (validation.address) "" else getString(R.string.invalid_field_empty)
    }

    override fun openLangDialog() {
        LocaleDialog().show(supportFragmentManager, LocaleDialog.TAG)
    }

    override fun openCountryDialog() {
        CountryDialog().show(supportFragmentManager, CountryDialog.TAG)
    }

    override fun openTermsOfUseDialog() {
        viewModel.getUserType().value?.let {
            TermsOfUseDialog.getInstance(it).show(supportFragmentManager, TermsOfUseDialog.TAG)
        }
    }

    override fun changeCountry(country: Country) {
        binding.countryLogo.setImageResource(
            resources.getIdentifier(
                country.icon,
                "drawable",
                packageName
            )
        )
        binding.phoneInputLayout.prefixText = String.format(
            Locale.getDefault(),
            getString(R.string.country_code_placeholder),
            country.code
        )
    }

    override fun openMainScreen(user: User) {
        startActivity(Intent(this, MainActivity::class.java).apply {
            flags =
                Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
            putExtra(MainActivity.USER, user)
        })
    }

    override fun openLoginScreen() {
        startActivity(Intent(this, LoginActivity::class.java))
    }

    override fun openRestoreScreen() {
        startActivity(Intent(this, RestoreActivity::class.java))
    }

    override fun openDialer() {
        startActivity(Intent(Intent.ACTION_DIAL).apply {
            data = Uri.parse("tel:" + getString(R.string.support_phone))
        })
    }

}