package ru.optap.ui.register

data class RegisterValidationState(
    var name: Boolean,
    var city: Boolean,
    var phone: Boolean,
    var password: Boolean,
    var inviteCode: Boolean,
    var address: Boolean
) {
    fun isInvalidData(): Boolean = !(name && city && phone && password && inviteCode && address)
}