package ru.optap.ui.register

import ru.optap.ui.dialog.country.data.Country
import ru.optap.data.model.User

interface RegisterView {
    fun showValidationErrors(validation: RegisterValidationState)
    fun openLangDialog()
    fun openCountryDialog()
    fun openTermsOfUseDialog()
    fun changeCountry(country: Country)
    fun openMainScreen(user: User)
    fun openLoginScreen()
    fun openRestoreScreen()
    fun openDialer()
}