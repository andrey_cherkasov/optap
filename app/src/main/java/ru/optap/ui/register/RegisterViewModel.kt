package ru.optap.ui.register

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ru.optap.data.Resource
import ru.optap.data.local.SharedPreferencesHelper
import ru.optap.data.model.User
import ru.optap.data.remote.AuthApiInterface
import ru.optap.ui.base.viewmodel.BaseViewModel
import javax.inject.Inject

@HiltViewModel
class RegisterViewModel @Inject constructor(
    private val authApi: AuthApiInterface,
    private val preferencesHelper: SharedPreferencesHelper
) : BaseViewModel() {

    private val userLiveData: MutableLiveData<Resource<User>> = MutableLiveData()
    private val userTypeLiveData: MutableLiveData<Int> = MutableLiveData(User.CUSTOMER)
    private val agreementLiveData: MutableLiveData<Boolean> = MutableLiveData(false)
    private val validationLiveData: MutableLiveData<RegisterValidationState> =
        MutableLiveData(
            RegisterValidationState(
                name = true,
                city = true,
                phone = true,
                password = true,
                inviteCode = true,
                address = true
            )
        )


    fun getUser(): LiveData<Resource<User>> = userLiveData
    fun getUserType(): LiveData<Int> = userTypeLiveData
    fun getAgreement(): LiveData<Boolean> = agreementLiveData
    fun getValidation(): LiveData<RegisterValidationState> = validationLiveData

    fun setUserType(userType: Int) {
        userTypeLiveData.value = userType
    }

    fun setAgreement(agree: Boolean) {
        agreementLiveData.value = agree
    }

    fun register(
        name: String,
        city: String,
        countryId: String,
        phone: String,
        password: String,
        inviteCode: String,
        address: String,
        catalogType: Int
    ) {
        val login = phone.trim().replace("[^0-9]".toRegex(), "")
        validationLiveData.value = RegisterValidationState(
            name = name.isNotEmpty(),
            city = city.isNotEmpty(),
            phone = login.length == 10,
            password = password.length >= 6,
            inviteCode = (userTypeLiveData.value == User.CUSTOMER) || inviteCode.isNotEmpty(),
            address = (userTypeLiveData.value == User.CUSTOMER) || address.isNotEmpty()
        )

        if (validationLiveData.value!!.isInvalidData()) return

        disposable.add(
            authApi.registration(
                countryId,
                login,
                name,
                password,
                userTypeLiveData.value!!,
                0,
                inviteCode,
                address,
                catalogType,
                preferencesHelper.loadFCMToken()
            )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({}, {})
        )

    }


}