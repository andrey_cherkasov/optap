package ru.optap.ui.customers.data

import ru.optap.data.Mapper

class CustomerMapper : Mapper<Customer, CustomerVO> {
    override fun map(input: Customer): CustomerVO {
        return CustomerVO(
            input.id,
            input.username ?: "",
            input.fullname ?: "",
            input.city ?: "",
            input.image ?: ""
        )
    }
}