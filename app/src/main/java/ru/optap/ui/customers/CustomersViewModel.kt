package ru.optap.ui.customers

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ru.optap.data.DataStatus
import ru.optap.data.Resource
import ru.optap.data.model.DataList
import ru.optap.data.remote.DataApiInterface
import ru.optap.ui.base.exception.ApiErrorException
import ru.optap.ui.base.exception.ServerErrorException
import ru.optap.ui.base.listview.adapter.EmptyList
import ru.optap.ui.base.listview.adapter.IListItem
import ru.optap.ui.base.listview.adapter.Loading
import ru.optap.ui.base.viewmodel.BaseListViewModel
import ru.optap.ui.customers.data.CustomerMapper
import ru.optap.ui.customers.data.CustomerVO
import javax.inject.Inject

@HiltViewModel
class CustomersViewModel @Inject constructor(
    private val dataApi: DataApiInterface,
    private val customerMapper: CustomerMapper
) : BaseListViewModel() {

    private val customersLiveData: MutableLiveData<Resource<DataList<IListItem>>> =
        MutableLiveData()

    val customers: LiveData<Resource<DataList<IListItem>>> get() = customersLiveData


    fun refresh() {
        page = 1
        customersLiveData.value = Resource.success(DataList(listOf(Loading()), page, 0))
        load()
    }

    fun load() {
        if (customers.value?.status == DataStatus.LOADING) return
        if (page > customers.value?.data?.lastPage ?: 1) return

        disposable.add(
            dataApi.getCustomers(page)
                .subscribeOn(Schedulers.io())
                .doOnSuccess { response ->
                    if (!response.meta.success) throw ApiErrorException(response.meta.message)
                }
                .map { it.data }
                .map {
                    DataList<IListItem>(
                        it.dataList.map { customer -> customerMapper.map(customer) },
                        page,
                        it.total
                    )
                }
                .doOnSubscribe {
                    customersLiveData.value = Resource.loading(customers.value?.data)
                }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it != null) {
                        val list = ArrayList<IListItem>(customers.value?.data!!.dataList.size + 30)
                        list.addAll(customers.value?.data?.dataList ?: listOf())
                        list.removeLast()

                        if (page == 1 && it.dataList.isEmpty()) list.add(EmptyList())
                        list.addAll(it.dataList)
                        if (page < it.lastPage) list.add(Loading())
                        customersLiveData.value = Resource.success(DataList(list, page, it.total))
                        page++
                    } else {
                        throw ServerErrorException()
                    }
                }, { t -> customersLiveData.value = Resource.error(t, null) })
        )
    }

    fun deleteCustomer(position: Int) {
        val customersList = customers.value?.data?.dataList?.toMutableList()
        disposable.add(
            dataApi.deleteCustomer((customersList?.get(position) as CustomerVO).id)
                .subscribeOn(Schedulers.io())
                .doOnSuccess { if (!it.meta.success) throw ApiErrorException(it.meta.message) }
                .observeOn(AndroidSchedulers.mainThread())
                .map { it.meta.message }
                .subscribe({
                    customersList.removeAt(position)
                    customersLiveData.value = Resource.success(
                        DataList(
                            customersList,
                            page,
                            customers.value?.data?.total?.minus(1) ?: 0
                        )
                    )
                }, { })
        )
    }

}