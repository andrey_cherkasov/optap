package ru.optap.ui.customers.data

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Customer(
    @Json(name = "user_id") val id: Int,
    @Json(name = "username") val username: String?,
    @Json(name = "fullname") val fullname: String?,
    @Json(name = "city") val city: String?,
    @Json(name = "image") val image: String?
)
