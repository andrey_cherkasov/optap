package ru.optap.ui.customers

import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import ru.optap.R
import ru.optap.databinding.ItemCustomerBinding
import ru.optap.ui.base.listview.adapter.BaseDelegateAdapter
import ru.optap.ui.base.listview.adapter.BaseViewHolder
import ru.optap.ui.base.listview.adapter.IListItem
import ru.optap.ui.customers.data.CustomerVO

class CustomersDelegateAdapter : BaseDelegateAdapter {

    companion object {
        const val ID = R.layout.item_customer
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = CustomerViewHolder(
        ItemCustomerBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
    )

    override fun onBindViewHolder(holder: BaseViewHolder, item: IListItem) = holder.onBind(item)

    override fun isForViewType(item: IListItem) = item is CustomerVO

    override fun id() = ID


    inner class CustomerViewHolder(private val binding: ItemCustomerBinding) :
        BaseViewHolder(binding) {
        override fun onBind(item: IListItem) {
            val customer = item as CustomerVO
            Glide.with(binding.image.context)
                .load(customer.image)
                .transform(CircleCrop())
                .placeholder(R.drawable.preloader)
                .into(binding.image)
            binding.title.text = customer.fullname
            binding.subtitle.text = customer.username
        }
    }
}