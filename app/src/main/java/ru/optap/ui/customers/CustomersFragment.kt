package ru.optap.ui.customers

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import ru.optap.data.DataStatus
import ru.optap.databinding.FragmentCustomersBinding
import ru.optap.ui.base.view.BaseFragment
import ru.optap.ui.base.listview.*
import ru.optap.ui.base.listview.adapter.BasePagingAdapter
import ru.optap.ui.base.listview.adapter.EmptyListDelegateAdapter
import ru.optap.ui.base.listview.adapter.IListItem
import ru.optap.ui.base.listview.adapter.LoadingDelegateAdapter
import ru.optap.ui.customers.data.CustomerVO
import ru.optap.ui.dialog.invite.InviteDialog


@AndroidEntryPoint
class CustomersFragment : BaseFragment(), CustomersView {

    private var _binding: FragmentCustomersBinding? = null
    private val viewModel: CustomersViewModel by viewModels()
    private lateinit var adapter: BasePagingAdapter

    private val binding get() = _binding!!


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentCustomersBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initList()
        initObservers()

        binding.fab.setOnClickListener { showInviteDialog() }
        binding.refreshLayout.setOnRefreshListener {
            binding.refreshLayout.isRefreshing = false
            viewModel.refresh()
        }

        if (savedInstanceState == null) viewModel.refresh()
    }

    private fun initList() {
        val customersDelegateAdapter = CustomersDelegateAdapter()

        val adapterDelegates = mapOf(
            CustomersDelegateAdapter.ID to customersDelegateAdapter,
            LoadingDelegateAdapter.ID to LoadingDelegateAdapter(),
            EmptyListDelegateAdapter.ID to EmptyListDelegateAdapter()
        )

        val diffUtil = object : DiffUtil.ItemCallback<IListItem>() {
            override fun areItemsTheSame(oldItem: IListItem, newItem: IListItem) =
                if (oldItem is CustomerVO && newItem is CustomerVO) {
                    oldItem.id == newItem.id
                } else {
                    false
                }

            override fun areContentsTheSame(oldItem: IListItem, newItem: IListItem) =
                if (oldItem is CustomerVO && newItem is CustomerVO) {
                    oldItem == newItem
                } else {
                    false
                }
        }

        val layoutManager = LinearLayoutManager(context)
        val scrollListener = object : PaginationScrollListener(layoutManager) {
            override fun loadMore() = viewModel.load()
        }

        val swipeController = SwipeController(object : SwipeControllerActions() {
            override fun onRightClicked(position: Int) = viewModel.deleteCustomer(position)
        }, ItemTouchHelper.LEFT)
        val itemTouchHelper = ItemTouchHelper(swipeController)
        itemTouchHelper.attachToRecyclerView(binding.list)

        adapter = BasePagingAdapter(adapterDelegates, diffUtil)
        binding.list.apply {
            this.adapter = this@CustomersFragment.adapter
            this.layoutManager = layoutManager
            addOnScrollListener(scrollListener)
        }
    }

    private fun initObservers() {
        viewModel.customers.observe(viewLifecycleOwner) {
            when (it.status) {
                DataStatus.LOADING -> {}
                DataStatus.SUCCESS -> adapter.submitList(it.data?.dataList)
                DataStatus.ERROR -> handleError(it.throwable)
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun showInviteDialog() = InviteDialog().show(parentFragmentManager, InviteDialog.TAG)

}