package ru.optap.ui.customers.data

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import ru.optap.ui.base.listview.adapter.IListItem

@Parcelize
data class CustomerVO(
    val id: Int,
    val username: String,
    val fullname: String,
    val city: String,
    val image: String
) : IListItem, Parcelable