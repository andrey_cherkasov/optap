package ru.optap.ui.order.data

import ru.optap.BuildConfig
import ru.optap.data.Mapper
import ru.optap.data.local.SharedPreferencesHelper
import ru.optap.data.model.User
import ru.optap.ui.base.listview.adapter.IListItem
import ru.optap.ui.catalog.data.CategoryMapper
import ru.optap.ui.category.data.ProductMapper
import ru.tinkoff.decoro.Mask
import ru.tinkoff.decoro.MaskImpl
import ru.tinkoff.decoro.parser.UnderscoreDigitSlotsParser
import ru.tinkoff.decoro.slots.Slot
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*


class OrderDetailMapper(
    private val categoryMapper: CategoryMapper,
    private val productMapper: ProductMapper,
    val preferencesHelper: SharedPreferencesHelper
) : Mapper<OrderDetail, OrderDetailVO> {

    override fun map(input: OrderDetail): OrderDetailVO {
        val result = mutableListOf<IListItem>()

        val inputFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd HH:m:s", Locale.getDefault())
        val outputFormat: DateFormat = SimpleDateFormat("HH:mm dd.MM.yyyy", Locale.getDefault())
        var dateString = ""
        try {
            val date = inputFormat.parse(input.createdAt ?: "")
            if (date != null) {
                date.time = date.time + TimeZone.getDefault().rawOffset
                dateString = outputFormat.format(date)
            }
        } catch (e: Exception) {
        }

        val slots: Array<Slot> = UnderscoreDigitSlotsParser().parseSlots("(___) ___ - __ - __")
        val phoneMask: Mask = MaskImpl.createTerminated(slots)
        phoneMask.insertFront(input.phone)

        val user = preferencesHelper.loadUser()
        if (user?.role == User.CUSTOMER) {
            result.add(
                OrderDetailInfo(
                    input.id,
                    input.userId,
                    input.catalogId,
                    dateString,
                    input.statusId,
                    input.vendorId,
                    input.fullname ?: "",
                    "+7" + input.phone,
                    "+7 $phoneMask",
                    BuildConfig.BASE_URL + input.image,
                    input.statusName ?: "",
                    input.isAccepted ?: 0,
                    input.catalogInfo?.tradeCenter ?: "",
                    input.catalogInfo?.pavilionNumber ?: "",
                )
            )
        }

        val categories = (input.categories ?: listOf())
            .map { category -> categoryMapper.ToVo().map(category) }
        val products = (input.products ?: listOf()).map { product ->
            productMapper.ToVo().map(product)
        }

        val categoryIterator = categories.iterator()
        while (categoryIterator.hasNext()) {
            val category = categoryIterator.next()
            result.add(category)
            result.addAll(products.filter { product -> product.categoryId == category.id })
        }

        return OrderDetailVO(
            input.id,
            input.userId,
            input.catalogId,
            input.createdAt ?: "",
            input.updatedAt ?: "",
            input.statusId,
            input.vendorId,
            input.fullname ?: "",
            input.phone ?: "",
            BuildConfig.BASE_URL + input.image,
            input.statusName ?: "",
            input.totalPrice ?: 0f,
            input.isAccepted ?: 0,
            input.catalogInfo?.tradeCenter ?: "",
            input.catalogInfo?.pavilionNumber ?: "",
            result
        )
    }

}