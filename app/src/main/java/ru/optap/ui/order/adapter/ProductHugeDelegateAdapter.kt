package ru.optap.ui.order.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.Glide
import ru.optap.R
import ru.optap.databinding.ItemProductHugeBinding
import ru.optap.ui.base.listview.adapter.BaseDelegateAdapter
import ru.optap.ui.base.listview.adapter.BaseViewHolder
import ru.optap.ui.base.listview.adapter.IListItem
import ru.optap.ui.category.data.ProductVO
import ru.optap.ui.order.data.ViewMode


class ProductHugeDelegateAdapter(
    private val listener: (product: ProductVO, position: Int) -> Unit
) : BaseDelegateAdapter {

    companion object {
        const val ID = R.layout.item_product_huge
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ProductHugeViewHolder(
        ItemProductHugeBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
    )

    override fun onBindViewHolder(holder: BaseViewHolder, item: IListItem) = holder.onBind(item)

    override fun isForViewType(item: IListItem) =
        item is ProductVO && item.viewMode == ViewMode.HUGE && item.expand

    override fun id() = ID


    inner class ProductHugeViewHolder(private val binding: ItemProductHugeBinding) :
        BaseViewHolder(binding) {
        override fun onBind(item: IListItem) {
            val product = item as ProductVO

            binding.text.text =
                if (product.name == "") binding.root.context.getString(R.string.untitled) else product.name
            binding.price.text = product.priceString
            binding.count.text = product.countString
            binding.totalPrice.text = product.totalPrice.toString()
            Glide.with(binding.image.context)
                .load(product.image)
                .placeholder(R.drawable.preloader)
                .into(binding.image)

            binding.root.setOnClickListener { listener(product, absoluteAdapterPosition) }
        }
    }

}