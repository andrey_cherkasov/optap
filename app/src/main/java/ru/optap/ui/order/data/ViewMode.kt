package ru.optap.ui.order.data

class ViewMode {
    companion object {
        const val HUGE = 1
        const val TINY = 2
    }
}