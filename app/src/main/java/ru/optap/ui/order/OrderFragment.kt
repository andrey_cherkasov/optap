package ru.optap.ui.order

import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import ru.optap.R
import ru.optap.data.DataStatus
import ru.optap.data.model.User
import ru.optap.databinding.FragmentOrderBinding
import ru.optap.ui.base.listview.SwipeController
import ru.optap.ui.base.listview.SwipeControllerActions
import ru.optap.ui.base.listview.adapter.BasePagingAdapter
import ru.optap.ui.base.listview.adapter.IListItem
import ru.optap.ui.base.listview.adapter.LoadingDelegateAdapter
import ru.optap.ui.base.view.BaseFragment
import ru.optap.ui.catalog.data.CategoryVO
import ru.optap.ui.category.data.ProductVO
import ru.optap.ui.order.adapter.*
import ru.optap.ui.order.contract.OrderItemActivityResultContract
import ru.optap.ui.order.data.OrderDetailInfo
import ru.optap.ui.order.data.ViewMode
import ru.optap.ui.orders.data.OrderVO

@AndroidEntryPoint
class OrderFragment : BaseFragment(), OrderView {

    companion object {
        const val ORDER = "ORDER"
    }

    private var _binding: FragmentOrderBinding? = null
    private val viewModel: OrderViewModel by viewModels()
    private lateinit var adapter: BasePagingAdapter
    private var viewModeMenu: MenuItem? = null

    private val binding get() = _binding!!

    private val launcher = registerForActivityResult(OrderItemActivityResultContract()) {
        it?.let { it1 -> viewModel.editOrderItem(it1) }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentOrderBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val order = arguments?.getParcelable<OrderVO>(ORDER)
        if (order != null) {
            viewModel.setOrder(order)
        } else {
            return
        }

        initList()
        initObservers()

        if (viewModel.user?.role == User.CUSTOMER) {
            binding.fab.translationX = 500F
        } else {
            binding.fab.setOnClickListener { showAcceptDialog() }
        }

        if (savedInstanceState == null) viewModel.refresh()
    }

    private fun initList() {
        val categoryTinyDelegateAdapter = CategoryTinyDelegateAdapter { _, position ->
            viewModel.performCategory(position)
        }
        val productTinyDelegateAdapter = ProductTinyDelegateAdapter { product, _ ->
            openOrderDetailScreen(product)
        }
        val productHugeDelegateAdapter = ProductHugeDelegateAdapter { product, _ ->
            openOrderDetailScreen(product)
        }

        val adapterDelegates = mapOf(
            CategoryTinyDelegateAdapter.ID to categoryTinyDelegateAdapter,
            ProductTinyDelegateAdapter.ID to productTinyDelegateAdapter,
            ProductHugeDelegateAdapter.ID to productHugeDelegateAdapter,
            ProductCollapsedDelegateAdapter.ID to ProductCollapsedDelegateAdapter(),
            LoadingDelegateAdapter.ID to LoadingDelegateAdapter(),
            OrderInfoDelegateAdapter.ID to OrderInfoDelegateAdapter()
        )

        val diffUtil = object : DiffUtil.ItemCallback<IListItem>() {
            override fun areItemsTheSame(oldItem: IListItem, newItem: IListItem) =
                if (oldItem is CategoryVO && newItem is CategoryVO) {
                    oldItem.id == newItem.id
                } else if (oldItem is ProductVO && newItem is ProductVO) {
                    oldItem.id == newItem.id
                } else if (oldItem is OrderDetailInfo && newItem is OrderDetailInfo) {
                    oldItem.id == newItem.id
                } else {
                    false
                }

            override fun areContentsTheSame(oldItem: IListItem, newItem: IListItem) =
                if (oldItem is CategoryVO && newItem is CategoryVO) {
                    oldItem == newItem
                } else if (oldItem is ProductVO && newItem is ProductVO) {
                    oldItem == newItem
                } else if (oldItem is OrderDetailInfo && newItem is OrderDetailInfo) {
                    oldItem == newItem
                } else {
                    false
                }
        }

        val swipeController = SwipeController(object : SwipeControllerActions() {
            override fun onRightClicked(position: Int) = viewModel.deleteItem(position)
        }, ItemTouchHelper.LEFT)
        val itemTouchHelper = ItemTouchHelper(swipeController)
        itemTouchHelper.attachToRecyclerView(binding.list)

        adapter = BasePagingAdapter(adapterDelegates, diffUtil)
        binding.list.apply {
            this.adapter = this@OrderFragment.adapter
            this.layoutManager = LinearLayoutManager(context)
        }
    }

    private fun initObservers() {
        viewModel.orderDetail.observe(viewLifecycleOwner) {
            when (it.status) {
                DataStatus.LOADING -> {}
                DataStatus.SUCCESS -> adapter.submitList(it.data?.list)
                DataStatus.ERROR -> handleError(it.throwable)
            }
        }
        viewModel.order.observe(viewLifecycleOwner) {
            (activity as AppCompatActivity).supportActionBar?.apply {
                title = getString(R.string.order_number_title, it.id)
                subtitle = getString(R.string.total_price_subtitle, it.totalPriceString)
            }
        }
        viewModel.viewMode.observe(viewLifecycleOwner) { setViewMode(it) }
        viewModel.message.observe(viewLifecycleOwner) { showToast(it) }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.fragment_order, menu)
        viewModeMenu = menu.findItem(R.id.view_mode)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.view_mode -> viewModel.changeViewMode()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        (activity as AppCompatActivity).supportActionBar?.subtitle = ""
        _binding = null
    }

    override fun setViewMode(viewMode: Int) {
        if (viewMode == ViewMode.TINY) {
            viewModeMenu?.setIcon(R.drawable.ic_view_grid)
        } else if (viewMode == ViewMode.HUGE) {
            viewModeMenu?.setIcon(R.drawable.ic_format_list_bulleted_type)
        }
    }

    override fun showAcceptDialog() {
        AlertDialog.Builder(requireContext())
            .setMessage(R.string.accept_text_dialog)
            .setPositiveButton(R.string.ok) { _, _ -> viewModel.accept() }
            .setNegativeButton(R.string.cancel) { dialog, _ -> dialog.dismiss() }
            .create()
            .show()
    }

    override fun openOrderDetailScreen(product: ProductVO) {
        launcher.launch(product)
        viewModel.saveProductPrice(product)
    }

}