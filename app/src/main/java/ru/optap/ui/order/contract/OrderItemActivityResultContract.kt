package ru.optap.ui.order.contract

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.activity.result.contract.ActivityResultContract
import ru.optap.ui.category.data.ProductVO
import ru.optap.ui.orderItem.OrderItemActivity

class OrderItemActivityResultContract : ActivityResultContract<ProductVO, ProductVO?>() {
    override fun createIntent(context: Context, input: ProductVO) =
        Intent(context, OrderItemActivity::class.java).putExtra(OrderItemActivity.PRODUCT, input)

    override fun parseResult(resultCode: Int, intent: Intent?): ProductVO? = when {
        resultCode != Activity.RESULT_OK -> null
        else -> intent?.getParcelableExtra(OrderItemActivity.PRODUCT)
    }
}