package ru.optap.ui.order.data

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import ru.optap.ui.catalog.data.Category
import ru.optap.ui.category.data.Product

@JsonClass(generateAdapter = true)
data class OrderDetail(
    @Json(name = "id") val id: Int,
    @Json(name = "user_id") val userId: Int,
    @Json(name = "catalog_id") val catalogId: Int,
    @Json(name = "created_at") val createdAt: String? = null,
    @Json(name = "updated_at") val updatedAt: String? = null,
    @Json(name = "status_id") val statusId: Int,
    @Json(name = "vendor_id") val vendorId: Int,
    @Json(name = "fullname") val fullname: String? = null,
    @Json(name = "phone") val phone: String? = null,
    @Json(name = "image") val image: String? = null,
    @Json(name = "status_name") val statusName: String? = null,
    @Json(name = "categories") val categories: List<Category>? = null,
    @Json(name = "items") val products: List<Product>? = null,
    @Json(name = "total_price") val totalPrice: Float? = null,
    @Json(name = "is_accepted") val isAccepted: Int? = null,
    @Json(name = "catalog_info") val catalogInfo: CatalogInfo?
)