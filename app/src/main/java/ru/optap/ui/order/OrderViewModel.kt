package ru.optap.ui.order

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ru.optap.data.Resource
import ru.optap.data.local.SharedPreferencesHelper
import ru.optap.data.remote.DataApiInterface
import ru.optap.ui.base.SingleLiveEvent
import ru.optap.ui.base.exception.ApiErrorException
import ru.optap.ui.base.listview.adapter.Loading
import ru.optap.ui.base.viewmodel.BaseListViewModel
import ru.optap.ui.catalog.data.CategoryVO
import ru.optap.ui.category.data.ProductVO
import ru.optap.ui.order.data.OrderDetailMapper
import ru.optap.ui.order.data.OrderDetailVO
import ru.optap.ui.order.data.ViewMode
import ru.optap.ui.orders.data.OrderVO
import javax.inject.Inject

@HiltViewModel
class OrderViewModel @Inject constructor(
    private val dataApi: DataApiInterface,
    private val orderDetailMapper: OrderDetailMapper,
    val preferencesHelper: SharedPreferencesHelper
) : BaseListViewModel() {

    private val orderLiveData: MutableLiveData<OrderVO> = MutableLiveData()
    private val orderDetailLiveData: MutableLiveData<Resource<OrderDetailVO>> = MutableLiveData()
    private val viewModeLiveData: MutableLiveData<Int> = MutableLiveData(ViewMode.TINY)
    private val messageLiveData: SingleLiveEvent<String> = SingleLiveEvent()

    val order: LiveData<OrderVO> get() = orderLiveData
    val orderDetail: LiveData<Resource<OrderDetailVO>> get() = orderDetailLiveData
    val viewMode: LiveData<Int> get() = viewModeLiveData
    val message: LiveData<String> get() = messageLiveData

    val user = preferencesHelper.loadUser()

    fun setOrder(order: OrderVO) {
        orderLiveData.value = order
    }

    fun refresh() {
        disposable.add(
            dataApi.getOrder(order.value!!.id)
                .subscribeOn(Schedulers.io())
                .doOnSubscribe {
                    orderDetailLiveData.value = Resource.success(
                        OrderDetailVO.createEmpty().copy(list = listOf(Loading()))
                    )
                }
                .doOnSuccess { if (!it.meta.success) throw ApiErrorException(it.meta.message) }
                .map { it.data }
                .map { orderDetailMapper.map(it) }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { orderDetailLiveData.value = Resource.success(it) },
                    { t -> orderDetailLiveData.value = Resource.error(t, null) }
                )
        )
    }

    fun changeViewMode() {
        viewModeLiveData.value =
            if (viewMode.value == ViewMode.TINY) ViewMode.HUGE else ViewMode.TINY

        disposable.add(Single.fromCallable {
            val list = orderDetail.value?.data?.list?.toMutableList() ?: mutableListOf()
            val iterator = list.listIterator()
            while (iterator.hasNext()) {
                val item = iterator.next()
                if (item is ProductVO) iterator.set(item.copy(viewMode = viewMode.value!!))
            }
            orderDetail.value?.data?.copy(list = list)
        }
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { orderDetailLiveData.value = Resource.success(it) },
                { t -> t.printStackTrace() }
            )
        )
    }

    fun deleteItem(position: Int) {
        disposable.add(
            Single.fromCallable {
                val list = orderDetail.value?.data?.list?.toMutableList() ?: mutableListOf()
                val item = list[position]
                var minusPrice = 0

                if (item is CategoryVO) {
                    val response = dataApi.deleteOrderCategory(
                        order.value!!.id,
                        item.id
                    ).execute().body()

                    if (response != null && response.meta.success) {
                        val iterator = list.listIterator(position)
                        iterator.next()
                        iterator.remove()
                        while (iterator.hasNext()) {
                            val listItem = iterator.next()
                            if (listItem is ProductVO) {
                                minusPrice += listItem.price * listItem.count
                                iterator.remove()
                            } else {
                                break
                            }
                        }
                    }
                } else if (item is ProductVO) {
                    val response = dataApi.deleteOrderProduct(
                        order.value!!.id,
                        item.id
                    ).execute().body()

                    if (response != null && response.meta.success) {
                        minusPrice += item.price * item.count
                        val iterator = list.listIterator(position)
                        if (list[position - 1] is CategoryVO
                            && (position + 1 >= list.size || list[position + 1] is CategoryVO)
                        ) {
                            iterator.previous()
                            iterator.remove()
                        }

                        iterator.next()
                        iterator.remove()
                    }
                }

                val newTotalPrice = order.value!!.totalPrice - minusPrice
                orderLiveData.postValue(
                    order.value!!.copy(
                        totalPrice = newTotalPrice,
                        totalPriceString = newTotalPrice.toString()
                    )
                )
                orderDetail.value?.data?.copy(list = list)
            }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { orderDetailLiveData.value = Resource.success(it) },
                    { t -> messageLiveData.value = t.message }
                )
        )
    }

    fun saveProductPrice(product: ProductVO) {
        orderLiveData.value =
            order.value?.copy(totalPrice = order.value!!.totalPrice - product.totalPrice)
    }

    fun editOrderItem(product: ProductVO) {
        val totalPrice = order.value!!.totalPrice + product.totalPrice
        orderLiveData.value = order.value?.copy(
            totalPrice = totalPrice,
            totalPriceString = totalPrice.toString()
        )

        disposable.add(
            dataApi.editOrderProduct(order.value!!.id, product.id, product.count, product.typeId)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSuccess { if (!it.meta.success) throw  ApiErrorException(it.meta.message) }
                .map {
                    val list = orderDetail.value?.data?.list?.toMutableList() ?: mutableListOf()
                    val iterator = list.listIterator()
                    while (iterator.hasNext()) {
                        val item = iterator.next()
                        if (item is ProductVO && item.id == product.id) {
                            val count = product.count * when (product.typeId) {
                                1 -> product.packCount
                                2 -> product.boxCount
                                else -> 1
                            }

                            iterator.set(
                                product.copy(
                                    typeId = 0,
                                    count = count,
                                    countString = count.toString()
                                )
                            )
                            break
                        }
                    }

                    orderDetail.value?.data?.copy(list = list)
                }
                .subscribeOn(Schedulers.io())
                .subscribe(
                    { orderDetailLiveData.value = Resource.success(it) },
                    { t -> messageLiveData.value = t.message }
                )
        )
    }

    fun performCategory(position: Int) {
        disposable.add(Single.fromCallable {
            val list = orderDetail.value?.data?.list?.toMutableList() ?: mutableListOf()
            val category = list[position] as CategoryVO
            list[position] = category.copy(expand = !category.expand)
            for (i in (position + 1) until list.size) {
                val product = list[i] as? ProductVO ?: break
                list[i] = product.copy(expand = !product.expand)
            }
            orderDetail.value?.data?.copy(list = list)
        }
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { orderDetailLiveData.value = Resource.success(it) },
                { t -> t.printStackTrace() }
            )
        )
    }

    fun accept() {
        disposable.add(
            dataApi.acceptOrder(order.value!!.id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { messageLiveData.value = it.meta.message },
                    { t -> messageLiveData.value = t.message }
                )
        )
    }

}