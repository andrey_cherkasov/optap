package ru.optap.ui.order.data

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class CatalogInfo(
    @Json(name = "trade_center") val tradeCenter: String?,
    @Json(name = "pavilion_number") val pavilionNumber: String?
)