package ru.optap.ui.order.data

import ru.optap.ui.base.listview.adapter.IListItem

data class OrderDetailVO(
    val id: Int,
    val userId: Int,
    val catalogId: Int,
    val createdAt: String,
    val updatedAt: String,
    val statusId: Int,
    val vendorId: Int,
    val fullname: String,
    val phone: String,
    val image: String,
    val statusName: String,
    val totalPrice: Float,
    val isAccepted: Int,
    val tradeCenter: String,
    val pavilionNumber: String,
    val list: List<IListItem>
) {
    companion object {
        fun createEmpty(): OrderDetailVO = OrderDetailVO(
            0,
            0,
            0,
            "",
            "",
            0,
            0,
            "",
            "",
            "",
            "",
            0f,
            0,
            "",
            "",
            listOf()
        )
    }
}