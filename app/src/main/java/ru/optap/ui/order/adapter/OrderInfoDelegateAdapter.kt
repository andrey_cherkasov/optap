package ru.optap.ui.order.adapter

import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import ru.optap.R
import ru.optap.databinding.ItemOrderInfoBinding
import ru.optap.ui.base.listview.adapter.BaseDelegateAdapter
import ru.optap.ui.base.listview.adapter.BaseViewHolder
import ru.optap.ui.base.listview.adapter.IListItem
import ru.optap.ui.order.data.OrderDetailInfo

class OrderInfoDelegateAdapter : BaseDelegateAdapter {

    companion object {
        const val ID = R.layout.item_order_info
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = OrderInfoViewHolder(
        ItemOrderInfoBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
    )

    override fun onBindViewHolder(holder: BaseViewHolder, item: IListItem) = holder.onBind(item)

    override fun isForViewType(item: IListItem) = item is OrderDetailInfo

    override fun id() = ID

    inner class OrderInfoViewHolder(private val binding: ItemOrderInfoBinding) :
        BaseViewHolder(binding) {
        override fun onBind(item: IListItem) {
            val orderDetailInfo = item as OrderDetailInfo
            val context = binding.root.context

            if (orderDetailInfo.isAccepted == 1) {
                binding.status.setText(R.string.order_accepted)
                binding.status.setBackgroundColor(
                    ContextCompat.getColor(context, R.color.order_accepted)
                )
            } else {
                binding.status.setText(R.string.order_not_accepted)
                binding.status.setBackgroundColor(
                    ContextCompat.getColor(context, R.color.order_not_accepted)
                )
            }

            binding.catalogIdValue.text = orderDetailInfo.catalogId.toString()
            binding.orderDateValue.text = orderDetailInfo.createdAt
            binding.phoneValue.text = orderDetailInfo.phoneView
            binding.addressValue.text = orderDetailInfo.pavilionNumber

            binding.phoneValue.setOnClickListener {
                context.startActivity(Intent(Intent.ACTION_DIAL).apply {
                    data = Uri.parse("tel:" + orderDetailInfo.phone)
                })
            }
        }
    }

}