package ru.optap.ui.order

import ru.optap.ui.category.data.ProductVO

interface OrderView {
    fun setViewMode(viewMode: Int)
    fun showAcceptDialog()
    fun openOrderDetailScreen(product: ProductVO)
}