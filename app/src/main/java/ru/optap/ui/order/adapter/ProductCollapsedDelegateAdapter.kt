package ru.optap.ui.order.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import ru.optap.R
import ru.optap.databinding.ItemProductCollapsedBinding
import ru.optap.ui.base.listview.adapter.BaseDelegateAdapter
import ru.optap.ui.base.listview.adapter.BaseViewHolder
import ru.optap.ui.base.listview.adapter.IListItem
import ru.optap.ui.category.data.ProductVO

class ProductCollapsedDelegateAdapter : BaseDelegateAdapter {

    companion object {
        const val ID = R.layout.item_product_collapsed
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ProductCollapsedViewHolder(
        ItemProductCollapsedBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
    )

    override fun onBindViewHolder(holder: BaseViewHolder, item: IListItem) = holder.onBind(item)

    override fun isForViewType(item: IListItem) = item is ProductVO && !item.expand

    override fun id() = ID


    inner class ProductCollapsedViewHolder(binding: ItemProductCollapsedBinding) :
        BaseViewHolder(binding) {
        override fun onBind(item: IListItem) {}
    }

}