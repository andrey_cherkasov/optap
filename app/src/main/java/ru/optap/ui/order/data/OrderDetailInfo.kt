package ru.optap.ui.order.data

import ru.optap.ui.base.listview.adapter.IListItem

data class OrderDetailInfo(
    val id: Int,
    val userId: Int,
    val catalogId: Int,
    val createdAt: String,
    val statusId: Int,
    val vendorId: Int,
    val fullname: String,
    val phone: String,
    val phoneView: String,
    val image: String,
    val statusName: String,
    val isAccepted: Int,
    val tradeCenter: String,
    val pavilionNumber: String,
) : IListItem