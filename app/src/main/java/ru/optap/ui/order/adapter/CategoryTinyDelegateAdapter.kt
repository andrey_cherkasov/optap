package ru.optap.ui.order.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import ru.optap.R
import ru.optap.databinding.ItemCategoryTinyBinding
import ru.optap.ui.base.listview.adapter.BaseDelegateAdapter
import ru.optap.ui.base.listview.adapter.BaseViewHolder
import ru.optap.ui.base.listview.adapter.IListItem
import ru.optap.ui.catalog.data.CategoryVO

class CategoryTinyDelegateAdapter(
    private val listener: (category: CategoryVO, position: Int) -> Unit
) : BaseDelegateAdapter {

    companion object {
        const val ID = R.layout.item_category_tiny
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = CategoryTinyViewHolder(
        ItemCategoryTinyBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
    )

    override fun onBindViewHolder(holder: BaseViewHolder, item: IListItem) = holder.onBind(item)

    override fun isForViewType(item: IListItem) = item is CategoryVO

    override fun id() = ID


    inner class CategoryTinyViewHolder(private val binding: ItemCategoryTinyBinding) :
        BaseViewHolder(binding) {
        override fun onBind(item: IListItem) {
            val category = item as CategoryVO

            binding.arrow.rotation = if (category.expand) 90f else 0f

            // TODO image
            binding.text.text = category.name
            Glide.with(binding.image.context)
                .load(R.drawable.preloader)
                .placeholder(R.drawable.preloader)
                .transform(CircleCrop())
                .into(binding.image)

            binding.root.setOnClickListener { listener(category, absoluteAdapterPosition) }
        }
    }

}