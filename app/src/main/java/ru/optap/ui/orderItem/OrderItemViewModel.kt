package ru.optap.ui.orderItem

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import ru.optap.ui.base.viewmodel.BaseViewModel
import ru.optap.ui.category.data.ProductVO

class OrderItemViewModel : BaseViewModel() {

    private val productLiveData: MutableLiveData<ProductVO> = MutableLiveData()

    val product: LiveData<ProductVO> get() = productLiveData

    fun setProduct(product: ProductVO) {
        productLiveData.value = product
    }

    fun setCount(count: Int) {
        productLiveData.value = product.value?.copy(
            count = count, countString = count.toString()
        )
    }

    fun changeCountType(type: Int) {
        productLiveData.value = product.value?.copy(typeId = type)
    }

}