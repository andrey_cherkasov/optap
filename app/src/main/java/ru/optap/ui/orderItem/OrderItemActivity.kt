package ru.optap.ui.orderItem

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.core.content.ContextCompat
import androidx.core.widget.addTextChangedListener
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import ru.optap.R
import ru.optap.databinding.ActivityOrderItemBinding
import ru.optap.ui.base.view.BaseActivity
import ru.optap.ui.category.data.ProductVO
import ru.optap.util.Utils
import javax.inject.Inject
import kotlin.math.max


class OrderItemActivity : BaseActivity() {

    companion object {
        const val PRODUCT = "PRODUCT"
    }

    @Inject
    lateinit var utils: Utils

    private lateinit var binding: ActivityOrderItemBinding
    private val viewModel: OrderItemViewModel by viewModels()
    private var product: ProductVO? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityOrderItemBinding.inflate(LayoutInflater.from(this))
        setContentView(binding.root)
        setSupportActionBar(binding.appBarMain.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = getString(R.string.edit_item)

        val selectedColor = ContextCompat.getColor(this, R.color.secondaryColor)
        val unselectedColor = binding.pack.textColors.defaultColor

        product = intent.getParcelableExtra(PRODUCT)
        product?.let {
            Glide.with(this)
                .load(it.image)
                .placeholder(R.drawable.preloader)
                .transform(CircleCrop())
                .into(binding.image)

            binding.name.text = if (it.name == "") getString(R.string.untitled) else it.name
            binding.price.text = it.priceString
            binding.packCount.text = getString(R.string.amount_per_pack_value, it.packCount)
            binding.boxCount.text = getString(R.string.amount_per_box_value, it.boxCount)
            binding.countEditText.setText(it.count.toString())

            binding.pack.isEnabled = it.packCount != 0
            binding.box.isEnabled = it.boxCount != 0

            viewModel.setProduct(it)
        }

        viewModel.product.observe(this) {
            val factor = when (it.typeId) {
                1 -> it.packCount
                2 -> it.boxCount
                else -> 1
            }
            binding.totalPrice.text = (it.price * it.count * factor).toString()
//            binding.totalPrice.text = it.totalPriceString

            binding.pcs.setTextColor(unselectedColor)
            binding.pack.setTextColor(unselectedColor)
            binding.box.setTextColor(unselectedColor)

            when (it.typeId) {
                1 -> binding.pack.setTextColor(selectedColor)
                2 -> binding.box.setTextColor(selectedColor)
                else -> binding.pcs.setTextColor(selectedColor)
            }
        }


        binding.minus.setOnClickListener {
            binding.countEditText.setText(max(viewModel.product.value!!.count - 1, 0).toString())
        }
        binding.plus.setOnClickListener {
            binding.countEditText.setText(viewModel.product.value?.count?.plus(1).toString())
        }

        binding.pcs.setOnClickListener { viewModel.changeCountType(0) }
        binding.pack.setOnClickListener { viewModel.changeCountType(1) }
        binding.box.setOnClickListener { viewModel.changeCountType(2) }

        binding.countEditText.addTextChangedListener { text ->
            viewModel.setCount(text.toString().toIntOrNull() ?: 0)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.activity_catalog_instance, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.ok) {
            val result = Intent()
            result.putExtra(PRODUCT, viewModel.product.value)
            setResult(RESULT_OK, result)
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

}