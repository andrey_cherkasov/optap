package ru.optap.ui.user

import ru.optap.ui.user.data.UserValidationState

interface UserView {
    fun showValidationErrors(validation: UserValidationState)
    fun showPasswordDialog()
    fun showDeleteAccountDialog()
}