package ru.optap.ui.user.data

data class UserValidationState(
    var email: Boolean,
) {
    fun isInvalidData(): Boolean = !email
}