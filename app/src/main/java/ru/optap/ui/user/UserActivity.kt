package ru.optap.ui.user

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.core.view.isVisible
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.nguyenhoanglam.imagepicker.ui.imagepicker.registerImagePicker
import dagger.hilt.android.AndroidEntryPoint
import ru.optap.BuildConfig
import ru.optap.R
import ru.optap.data.DataStatus
import ru.optap.data.model.User
import ru.optap.databinding.ActivityUserBinding
import ru.optap.ui.base.view.BaseActivity
import ru.optap.ui.base.view.LoadingView
import ru.optap.ui.dialog.password.PasswordDialog
import ru.optap.ui.login.LoginActivity
import ru.optap.ui.user.data.CatalogType
import ru.optap.ui.user.data.UserValidationState
import ru.optap.util.Utils
import javax.inject.Inject

@AndroidEntryPoint
class UserActivity : BaseActivity(), LoadingView<User>, UserView {

    companion object {
        const val USER = "USER"
    }

    @Inject
    lateinit var utils: Utils

    private lateinit var binding: ActivityUserBinding
    private val viewModel: UserViewModel by viewModels()
    private lateinit var user: User

    private val launcher = registerImagePicker { images ->
        if (images.isNotEmpty()) viewModel.setImage(images[0])
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityUserBinding.inflate(LayoutInflater.from(this))
        setContentView(binding.root)
        user = intent.getParcelableExtra(USER)!!

        setSupportActionBar(binding.appBarMain.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setTitle(R.string.user_info)
        supportActionBar?.setSubtitle(
            if (user.role == User.VENDOR) R.string.user_vendor else R.string.user_customer
        )

        user.let {
            binding.emailEditText.setText(it.email)
            binding.nameEditText.setText(it.fullName)
            binding.addressEditText.setText(it.pavilionNumber)
            binding.catalogTypeRadioGroup.check(
                if (user.catalogType == CatalogType.OPENED) R.id.catalog_opened else R.id.catalog_closed
            )

            Glide.with(this)
                .load(BuildConfig.BASE_URL + it.image)
                .placeholder(R.drawable.preloader)
                .transform(CircleCrop())
                .into(binding.image)
            binding.userTypeGroup.isVisible = user.role == User.VENDOR
        }

        binding.editImageButton.setOnClickListener { launcher.launch(utils.getSingleImagePickerConfig()) }
        binding.changePasswordButton.setOnClickListener { showPasswordDialog() }
        binding.deleteAccountButton.setOnClickListener { showDeleteAccountDialog() }

        viewModel.user.observe(this) {
            showLoading(false)
            when (it.status) {
                DataStatus.LOADING -> showLoading(true)
                DataStatus.SUCCESS -> it.data?.let { it1 -> setData(it1) }
                DataStatus.ERROR -> handleError(it.throwable)
            }
        }

        viewModel.image.observe(this) {
            Glide.with(this)
                .load(it.uri)
                .transform(CircleCrop())
                .into(binding.image)
        }

        viewModel.deleteAccount.observe(this) {
            when (it.status) {
                DataStatus.LOADING -> {}
                DataStatus.SUCCESS -> {
                    it.data?.let { it1 -> showToast(it1) }
                    startActivity(Intent(this, LoginActivity::class.java).apply {
                        flags = (Intent.FLAG_ACTIVITY_CLEAR_TOP
                                or Intent.FLAG_ACTIVITY_CLEAR_TASK
                                or Intent.FLAG_ACTIVITY_NEW_TASK)
                    })
                    finish()
                }
                DataStatus.ERROR -> handleError(it.throwable)
            }
        }

        viewModel.userValidation.observe(this) { showValidationErrors(it) }
        viewModel.message.observe(this) { showToast(it) }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.activity_user, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.ok) {
            val catalogType =
                if (binding.catalogTypeRadioGroup.checkedRadioButtonId == R.id.catalog_opened) {
                    CatalogType.OPENED
                } else {
                    CatalogType.CLOSED
                }

            viewModel.saveUser(
                binding.emailEditText.text.toString(),
                binding.nameEditText.text.toString(),
                "city",
                catalogType,
                0,
                binding.addressEditText.text.toString(),
                0
            )
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun showLoading(loading: Boolean) {
        binding.loadingLayout.root.isVisible = loading
    }

    override fun setData(data: User) {
        val result = Intent().putExtra(USER, data)
        setResult(Activity.RESULT_OK, result)
        finish()
    }

    override fun showValidationErrors(validation: UserValidationState) {
        binding.emailInputLayout.error =
            if (validation.email) "" else getString(R.string.invalid_field_email)
    }

    override fun showPasswordDialog() =
        PasswordDialog().show(supportFragmentManager, PasswordDialog.TAG)

    override fun showDeleteAccountDialog() {
        AlertDialog.Builder(this)
            .setMessage(R.string.want_to_delete_acc)
            .setPositiveButton(R.string.yes) { _, _ -> viewModel.deleteAccount() }
            .setNegativeButton(R.string.no) { dialog, _ -> dialog.dismiss() }
            .show()
    }

}