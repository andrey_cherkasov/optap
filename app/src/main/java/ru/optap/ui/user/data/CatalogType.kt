package ru.optap.ui.user.data

class CatalogType {
    companion object {
        const val OPENED = 0
        const val CLOSED = 1
    }
}