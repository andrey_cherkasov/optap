package ru.optap.ui.user.contract

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.activity.result.contract.ActivityResultContract
import ru.optap.data.model.User
import ru.optap.ui.user.UserActivity

class UserActivityResultContract : ActivityResultContract<User, User?>() {
    override fun createIntent(context: Context, input: User) =
        Intent(context, UserActivity::class.java).putExtra(UserActivity.USER, input)

    override fun parseResult(resultCode: Int, intent: Intent?): User? = when {
        resultCode != Activity.RESULT_OK -> null
        else -> intent?.getParcelableExtra(UserActivity.USER)
    }
}