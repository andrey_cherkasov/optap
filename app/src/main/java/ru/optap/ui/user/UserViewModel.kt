package ru.optap.ui.user

import android.net.Uri
import android.util.Patterns
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.nguyenhoanglam.imagepicker.model.Image
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.MultipartBody
import ru.optap.data.Resource
import ru.optap.data.local.SharedPreferencesHelper
import ru.optap.data.model.User
import ru.optap.data.remote.AuthApiInterface
import ru.optap.data.remote.ProgressEmittingRequestBody
import ru.optap.ui.base.SingleLiveEvent
import ru.optap.ui.base.exception.ApiErrorException
import ru.optap.ui.base.viewmodel.BaseViewModel
import ru.optap.ui.user.data.UserValidationState
import ru.optap.util.Utils
import java.io.File
import javax.inject.Inject

@HiltViewModel
class UserViewModel @Inject constructor(
    private val authApi: AuthApiInterface,
    private val preferencesHelper: SharedPreferencesHelper,
    private val utils: Utils
) : BaseViewModel() {

    private val userLiveData: MutableLiveData<Resource<User>> = MutableLiveData()
    private val deleteAccountLiveData: MutableLiveData<Resource<String>> = MutableLiveData()
    private val imageLiveData: MutableLiveData<Image> = MutableLiveData()
    private val messageLiveData: SingleLiveEvent<String> = SingleLiveEvent()
    private val userValidationLiveData: MutableLiveData<UserValidationState> =
        MutableLiveData(UserValidationState(true))

    val user: LiveData<Resource<User>> = userLiveData
    val deleteAccount: LiveData<Resource<String>> = deleteAccountLiveData
    val image: LiveData<Image> = imageLiveData
    val message: LiveData<String> = messageLiveData
    val userValidation: LiveData<UserValidationState> = userValidationLiveData

    fun setImage(image: Image) {
        imageLiveData.value = image
    }

    fun saveUser(
        email: String,
        name: String,
        city: String,
        catalogType: Int,
        searchShow: Int,
        address: String,
        centerId: Int
    ) {
        userValidationLiveData.value = UserValidationState(
            Patterns.EMAIL_ADDRESS.matcher(email).matches()
        )
        if (userValidationLiveData.value!!.isInvalidData()) return

        val imagePath = utils.getRealPathFromUri(image.value?.uri ?: Uri.EMPTY)
        val imageFile = File(imagePath)
        val requestBody = ProgressEmittingRequestBody("", imageFile)
        val picture = MultipartBody.Part.createFormData("picture", imageFile.name, requestBody)

        disposable.add(
            authApi.saveUser(
                email,
                name,
                city,
                catalogType,
                searchShow,
                address,
                centerId,
                picture
            )
                .subscribeOn(Schedulers.io())
                .doOnSubscribe { userLiveData.value = Resource.loading(null) }
                .doOnSuccess { if (!it.meta.success) throw  ApiErrorException(it.meta.message) }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    {
                        val user = preferencesHelper.loadUser()?.copy(
                            email = email,
                            fullName = name,
                            city = city,
                            catalogType = catalogType,
                            searchShow = searchShow,
                            pavilionNumber = address,
                            centerId = centerId,
                            image = it.data?.image ?: ""
                        )
                        user?.let { it1 -> preferencesHelper.saveUser(it1) }
                        userLiveData.value = Resource.success(user)
                        messageLiveData.value = it.meta.message
                    },
                    { t -> Resource.error(t, null) }
                )
        )
    }

    fun deleteAccount() {
        disposable.add(
            authApi.deleteAccount()
                .subscribeOn(Schedulers.io())
                .doOnSubscribe { deleteAccountLiveData.value = Resource.loading(null) }
                .doOnSuccess { if (!it.meta.success) throw ApiErrorException(it.meta.message) }
                .map { it.meta.message }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    {
                        preferencesHelper.deleteUser()
                        deleteAccountLiveData.value = Resource.success(it)
                    },
                    { t -> deleteAccountLiveData.value = Resource.error(t, null) }
                )
        )
    }

}