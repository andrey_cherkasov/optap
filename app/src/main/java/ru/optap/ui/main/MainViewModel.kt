package ru.optap.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import ru.optap.data.model.User
import ru.optap.ui.base.viewmodel.BaseViewModel

class MainViewModel : BaseViewModel() {

    private val userLiveData: MutableLiveData<User> = MutableLiveData()

    val user: LiveData<User> get() = userLiveData

    fun setUser(user: User) {
        userLiveData.value = user
    }

}