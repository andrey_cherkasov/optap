package ru.optap.ui.main

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.core.view.GravityCompat
import androidx.core.view.isVisible
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.NavigationUI.navigateUp
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.google.android.material.navigation.NavigationView
import dagger.hilt.android.AndroidEntryPoint
import ru.optap.BuildConfig
import ru.optap.R
import ru.optap.data.local.SharedPreferencesHelper
import ru.optap.data.model.User
import ru.optap.databinding.ActivityMainBinding
import ru.optap.databinding.NavHeaderMainBinding
import ru.optap.ui.base.view.BaseActivity
import ru.optap.ui.dialog.invite.InviteDialog
import ru.optap.ui.dialog.locale.LocaleDialog
import ru.optap.ui.dialog.locale.LocaleDialogViewModel
import ru.optap.ui.dialog.support.SupportDialog
import ru.optap.ui.login.LoginActivity
import ru.optap.ui.user.contract.UserActivityResultContract
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : BaseActivity(), NavigationView.OnNavigationItemSelectedListener, MainView {

    companion object {
        const val USER = "USER"
    }

    @Inject
    lateinit var preferencesHelper: SharedPreferencesHelper

    private lateinit var binding: ActivityMainBinding
    private lateinit var headerBinding: NavHeaderMainBinding
    private val viewModel: MainViewModel by viewModels()
    private val localeViewModel: LocaleDialogViewModel by viewModels()
    private var appBarConfiguration: AppBarConfiguration? = null
    private var navController: NavController? = null

    private val topLevelDest: Set<Int> = setOf(
        R.id.nav_my_catalog,
        R.id.nav_search,
        R.id.nav_customers,
        R.id.nav_favorite_catalogs,
        R.id.nav_orders,
        R.id.nav_cart,
    )

    private val userResultLauncher = registerForActivityResult(UserActivityResultContract()) {
        it?.let { it1 -> viewModel.setUser(it1) }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val user: User = intent.getParcelableExtra(USER)!!
        binding = ActivityMainBinding.inflate(LayoutInflater.from(this))
        setContentView(binding.root)
        setSupportActionBar(binding.appBarMain.toolbar)

        initNavigationDrawer()
        initObservers()

        viewModel.setUser(user)
    }

    private fun initNavigationDrawer() {
        appBarConfiguration = AppBarConfiguration.Builder(topLevelDest)
            .setOpenableLayout(binding.drawerLayout)
            .build()

        val navHostFragment: NavHostFragment =
            supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment

        navController = navHostFragment.navController

        navController?.let {
            it.setGraph(R.navigation.mobile_navigation)
            NavigationUI.setupActionBarWithNavController(this, it, appBarConfiguration!!)
            NavigationUI.setupWithNavController(binding.navView, it)
        }

        binding.navView.setNavigationItemSelectedListener(this::onNavigationItemSelected)

        headerBinding = NavHeaderMainBinding.bind(binding.navView.getHeaderView(0))
        headerBinding.lang.setOnClickListener { openLangDialog() }
        headerBinding.root.setOnClickListener { openUserScreen() }

        localeViewModel.getLocales().observe(this) { headerBinding.lang.isVisible = true }
    }

    private fun initObservers() {
        viewModel.user.observe(this) { user ->
            Glide.with(this)
                .load(BuildConfig.BASE_URL + user.image)
                .placeholder(R.drawable.preloader)
                .transform(CircleCrop())
                .into(headerBinding.avatar)
            headerBinding.username.text = user.fullName
            user.catalogId?.let {
                headerBinding.catalogId.text =
                    String.format(getString(R.string.your_catalog_id), user.catalogId)
            }

            val menu = binding.navView.menu
            if (user.role == User.VENDOR) {
                menu.findItem(R.id.nav_favorite_catalogs).isVisible = false
                menu.findItem(R.id.nav_cart).isVisible = false
                menu.findItem(R.id.nav_search).isVisible = false
            } else {
                menu.findItem(R.id.nav_my_catalog).isVisible = false
                menu.findItem(R.id.nav_customers).isVisible = false
                navController?.popBackStack()
                navController?.navigate(R.id.nav_search)
            }

        }
    }

    override fun onBackPressed() {
        if (binding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            binding.drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        return navController?.let {
            appBarConfiguration?.let { it1 -> navigateUp(it, it1) }
        } == true || super.onSupportNavigateUp()
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        binding.drawerLayout.closeDrawers()
        when (item.itemId) {
            R.id.nav_logout -> {
                preferencesHelper.deleteUser()
                startActivity(Intent(this, LoginActivity::class.java).apply {
                    flags = (Intent.FLAG_ACTIVITY_CLEAR_TOP
                            or Intent.FLAG_ACTIVITY_CLEAR_TASK
                            or Intent.FLAG_ACTIVITY_NEW_TASK)
                })
                return false
            }
            R.id.nav_invite_customer -> {
                openInviteDialog()
                return false
            }
            R.id.nav_support -> {
                openSupportDialog()
                return false
            }
        }


        navController?.currentDestination?.id?.let { navController?.popBackStack(it, true) }
        navController?.navigate(item.itemId)
        binding.navView.setCheckedItem(item.itemId)
        return false
    }

    override fun openLangDialog() = LocaleDialog().show(supportFragmentManager, LocaleDialog.TAG)

    override fun openSupportDialog() =
        SupportDialog().show(supportFragmentManager, SupportDialog.TAG)

    override fun openInviteDialog() = InviteDialog().show(supportFragmentManager, InviteDialog.TAG)

    override fun openUserScreen() = userResultLauncher.launch(viewModel.user.value)

}