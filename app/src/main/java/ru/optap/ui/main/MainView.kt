package ru.optap.ui.main

interface MainView {
    fun openLangDialog()
    fun openSupportDialog()
    fun openInviteDialog()
    fun openUserScreen()
}