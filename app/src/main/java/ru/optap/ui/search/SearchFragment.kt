package ru.optap.ui.search

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.GridLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import ru.optap.R
import ru.optap.data.DataStatus
import ru.optap.databinding.FragmentSearchBinding
import ru.optap.ui.base.listview.PaginationScrollListener
import ru.optap.ui.base.listview.adapter.BasePagingAdapter
import ru.optap.ui.base.listview.adapter.EmptyListDelegateAdapter
import ru.optap.ui.base.listview.adapter.IListItem
import ru.optap.ui.base.listview.adapter.LoadingDelegateAdapter
import ru.optap.ui.base.view.BaseFragment
import ru.optap.ui.catalog.CatalogDelegateAdapter
import ru.optap.ui.catalog.CatalogFragment
import ru.optap.ui.catalog.data.CategoryVO
import ru.optap.ui.catalogs.data.CatalogVO
import ru.optap.ui.category.contract.CategoryActivityResultContract
import ru.optap.ui.dialog.invite.contract.ContactResultContract
import ru.optap.ui.search.adapter.CatalogsDelegateAdapter
import ru.optap.ui.search.adapter.CatalogsLabelDelegateAdapter
import ru.optap.ui.search.adapter.CategoriesLabelDelegateAdapter


@AndroidEntryPoint
class SearchFragment : BaseFragment(), SearchView {

    private var _binding: FragmentSearchBinding? = null
    private val viewModel: SearchViewModel by viewModels()
    private lateinit var adapter: BasePagingAdapter

    private val binding get() = _binding!!

    private val launcher = registerForActivityResult(CategoryActivityResultContract()) {}

    private val contactsLauncher = registerForActivityResult(ContactResultContract()) {
        it?.let { it1 -> viewModel.getPhoneNumber(it1) }
    }

    private val requestPermissionLauncher =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) {
            if (it) contactsLauncher.launch(null)
        }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentSearchBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initList()
        initObservers()

        binding.chooseContact.setOnClickListener { showChooseContactScreen() }
        binding.searchEditText.setOnEditorActionListener { v, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) viewModel.refreshSearch(v.text.toString())
            val imm =
                requireContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
            true
        }

        binding.searchEditText.addTextChangedListener { text ->
            if (text.toString().isEmpty()) viewModel.refresh()
        }
    }

    private fun initList() {
        val catalogsDelegateAdapter = CatalogsDelegateAdapter { openCatalogScreen(it) }
        val catalogDelegateAdapter = CatalogDelegateAdapter()
        catalogDelegateAdapter.setOnItemListViewClickListener { item, _ -> openCategoryScreen(item) }

        val adapterDelegates = mapOf(
            CatalogsDelegateAdapter.ID to catalogsDelegateAdapter,
            CatalogDelegateAdapter.ID to catalogDelegateAdapter,
            LoadingDelegateAdapter.ID to LoadingDelegateAdapter(),
            EmptyListDelegateAdapter.ID to EmptyListDelegateAdapter(),
            CatalogsLabelDelegateAdapter.ID to CatalogsLabelDelegateAdapter(),
            CategoriesLabelDelegateAdapter.ID to CategoriesLabelDelegateAdapter()
        )

        val diffUtil = object : DiffUtil.ItemCallback<IListItem>() {
            override fun areItemsTheSame(oldItem: IListItem, newItem: IListItem) =
                if (oldItem is CatalogVO && newItem is CatalogVO) {
                    oldItem.id == newItem.id
                } else if (oldItem is CategoryVO && newItem is CategoryVO) {
                    oldItem.id == newItem.id
                } else false

            override fun areContentsTheSame(oldItem: IListItem, newItem: IListItem) =
                if (oldItem is CatalogVO && newItem is CatalogVO) {
                    oldItem == newItem
                } else if (oldItem is CategoryVO && newItem is CategoryVO) {
                    oldItem == newItem
                } else false
        }

        val layoutManager = GridLayoutManager(requireContext(), 1)
//        layoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
//            override fun getSpanSize(position: Int): Int {
//                return when (adapter.getItemViewType(position)) {
//                    CatalogsDelegateAdapter.ID -> 2
//                    CatalogDelegateAdapter.ID -> 2
//                    else -> 4
//                }
//            }
//        }

        val scrollListener = object : PaginationScrollListener(layoutManager) {
            override fun loadMore() = viewModel.search(binding.searchEditText.text.toString())
        }

        adapter = BasePagingAdapter(adapterDelegates, diffUtil)
        binding.list.apply {
            this.adapter = this@SearchFragment.adapter
            this.layoutManager = layoutManager
            addOnScrollListener(scrollListener)
        }
    }

    private fun initObservers() {
        viewModel.search.observe(viewLifecycleOwner) {
            when (it.status) {
                DataStatus.LOADING -> {}
                DataStatus.SUCCESS -> adapter.submitList(it.data?.dataList)
                DataStatus.ERROR -> handleError(it.throwable)
            }
        }
        viewModel.selectedPhoneNumber.observe(viewLifecycleOwner) {
            binding.searchEditText.setText(it.toString())
            viewModel.refreshSearch(binding.searchEditText.text.toString())
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun showChooseContactScreen() {
        when (PackageManager.PERMISSION_GRANTED) {
            ContextCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.READ_CONTACTS
            ) -> contactsLauncher.launch(Unit)
            else -> requestPermissionLauncher.launch(Manifest.permission.READ_CONTACTS)
        }
    }

    override fun openCatalogScreen(catalog: CatalogVO) {
        viewModel.setCatalog(catalog)
        findNavController().navigate(
            R.id.nav_my_catalog_search,
            bundleOf(CatalogFragment.CATALOG to catalog)
        )
    }

    override fun openCategoryScreen(category: CategoryVO) =
        launcher.launch(category)

}