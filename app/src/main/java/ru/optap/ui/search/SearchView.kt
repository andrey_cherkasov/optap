package ru.optap.ui.search

import ru.optap.ui.catalog.data.CategoryVO
import ru.optap.ui.catalogs.data.CatalogVO

interface SearchView {
    fun showChooseContactScreen()
    fun openCatalogScreen(catalog: CatalogVO)
    fun openCategoryScreen(category: CategoryVO)
}