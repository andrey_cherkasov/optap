package ru.optap.ui.search.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import ru.optap.R
import ru.optap.databinding.ItemCategoriesLabelBinding
import ru.optap.ui.base.listview.adapter.BaseDelegateAdapter
import ru.optap.ui.base.listview.adapter.BaseViewHolder
import ru.optap.ui.base.listview.adapter.IListItem
import ru.optap.ui.search.data.CategoriesLabel

class CategoriesLabelDelegateAdapter : BaseDelegateAdapter {

    companion object {
        const val ID = R.layout.item_categories_label
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = CategoriesLabelViewHolder(
        ItemCategoriesLabelBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
    )

    override fun onBindViewHolder(holder: BaseViewHolder, item: IListItem) = holder.onBind(item)

    override fun isForViewType(item: IListItem) = item is CategoriesLabel

    override fun id() = ID

    inner class CategoriesLabelViewHolder(private val binding: ItemCategoriesLabelBinding) :
        BaseViewHolder(binding) {
        override fun onBind(item: IListItem) {}
    }
}