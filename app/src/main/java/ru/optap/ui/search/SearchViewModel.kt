package ru.optap.ui.search

import android.annotation.SuppressLint
import android.app.Application
import android.database.Cursor
import android.net.Uri
import android.provider.ContactsContract
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ru.optap.data.DataStatus
import ru.optap.data.Resource
import ru.optap.data.model.DataList
import ru.optap.data.remote.DataApiInterface
import ru.optap.ui.base.exception.ApiErrorException
import ru.optap.ui.base.listview.adapter.EmptyList
import ru.optap.ui.base.listview.adapter.IListItem
import ru.optap.ui.base.listview.adapter.Loading
import ru.optap.ui.base.viewmodel.BaseListViewModel
import ru.optap.ui.cart.CartRepository
import ru.optap.ui.catalog.data.CategoryMapper
import ru.optap.ui.catalogs.data.CatalogMapper
import ru.optap.ui.catalogs.data.CatalogVO
import ru.optap.ui.search.data.CatalogsLabel
import ru.optap.ui.search.data.CategoriesLabel
import javax.inject.Inject

@HiltViewModel
class SearchViewModel @Inject constructor(
    private val app: Application,
    private val dataApi: DataApiInterface,
    private val cartRepository: CartRepository,
    private val catalogMapper: CatalogMapper,
    private val categoryMapper: CategoryMapper
) : BaseListViewModel() {

    private val searchLiveData: MutableLiveData<Resource<DataList<IListItem>>> = MutableLiveData()
    private val selectedPhoneNumberLiveData: MutableLiveData<String> = MutableLiveData()

    val search: LiveData<Resource<DataList<IListItem>>> get() = searchLiveData
    val selectedPhoneNumber: LiveData<String> = selectedPhoneNumberLiveData


    init {
        refresh()
    }

    fun refresh() {
        page = 1
        loadInitialCatalogs()
    }

    private fun loadInitialCatalogs() {
        disposable.add(
            dataApi.getLatestCatalogs()
                .subscribeOn(Schedulers.io())
                .doOnSuccess { response ->
                    if (!response.meta.success) throw ApiErrorException(response.meta.message)
                }
                .map { it.data }
                .map {
                    DataList<IListItem>(
                        it.dataList.map { category -> catalogMapper.ToVo().map(category) },
                        page,
                        it.total
                    )
                }
                .doOnSubscribe {
                    searchLiveData.value = Resource.loading(search.value?.data)
                }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    searchLiveData.value = Resource.success(it)
                }, { t -> searchLiveData.value = Resource.error(t, null) })
        )
    }

    fun refreshSearch(s: String) {
        if (s.isEmpty()) return
        page = 1
        searchLiveData.value = Resource.success(DataList(listOf(Loading()), page, 0))
        search(s)
    }

    fun search(s: String) {
        if (s.isEmpty()) return
        if (search.value?.status == DataStatus.LOADING) return
        if (page > search.value?.data?.lastPage ?: 1) return

        disposable.add(Single.fromCallable {
            val list = search.value?.data?.dataList?.toMutableList() ?: mutableListOf()
            list.removeLast()
            if (page == 1) {
                val result = dataApi.searchCatalogs(s).execute().body()
                result?.data?.dataList?.map { catalogMapper.ToVo().map(it) }?.let {
                    if (it.isNotEmpty()) list.add(CatalogsLabel())
                    list.addAll(it)
                }
            }

            val result = dataApi.searchCategories(s, page).execute().body()
            val categories =
                result?.data?.dataList?.map { categoryMapper.ToVo().map(it) } ?: mutableListOf()
            if (page == 1 && categories.isNotEmpty()) list.add(CategoriesLabel())
            list.addAll(categories)

            if (page == 1 && list.isEmpty()) list.add(EmptyList())
            if (page < result?.data?.lastPage ?: 1) list.add(Loading())

            DataList(list, page, result?.data?.total ?: 0)
        }
            .subscribeOn(Schedulers.io())
            .doOnSubscribe {
                searchLiveData.value = Resource.loading(search.value?.data)
            }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                searchLiveData.value = Resource.success(it)
                page++
            }, { t -> searchLiveData.value = Resource.error(t, null) })
        )
    }

    fun setCatalog(catalog: CatalogVO) = cartRepository.setCatalog(catalog)

    @SuppressLint("Range")
    fun getPhoneNumber(uri: Uri) {
        disposable.add(Single.fromCallable {
            var clearPhoneNumber: String? = null
            val cursor: Cursor? = app.contentResolver?.query(
                uri,
                null,
                null,
                null,
                null
            )

            if (cursor != null && cursor.moveToFirst()) {
                val contactId =
                    cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID))
                val idResults =
                    cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))

                val phoneCursor: Cursor?
                if (idResults.toInt() == 1) {
                    phoneCursor = app.contentResolver!!.query(
                        ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                        null,
                        ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + contactId,
                        null,
                        null
                    )

                    if (phoneCursor != null && phoneCursor.moveToNext()) {
                        val contactNumber = phoneCursor.getString(
                            phoneCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)
                        )

                        clearPhoneNumber = contactNumber.trim().replace("[^0-9]".toRegex(), "")
                        phoneCursor.close()
                    }
                }
                cursor.close()
            }

            clearPhoneNumber
        }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ selectedPhoneNumberLiveData.value = it }, { t -> t.printStackTrace() })
        )
    }

}