package ru.optap.ui.search.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import ru.optap.R
import ru.optap.databinding.ItemCatalogsLabelBinding
import ru.optap.ui.base.listview.adapter.BaseDelegateAdapter
import ru.optap.ui.base.listview.adapter.BaseViewHolder
import ru.optap.ui.base.listview.adapter.IListItem
import ru.optap.ui.search.data.CatalogsLabel

class CatalogsLabelDelegateAdapter : BaseDelegateAdapter {

    companion object {
        const val ID = R.layout.item_catalogs_label
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = CatalogsLabelViewHolder(
        ItemCatalogsLabelBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
    )

    override fun onBindViewHolder(holder: BaseViewHolder, item: IListItem) = holder.onBind(item)

    override fun isForViewType(item: IListItem) = item is CatalogsLabel

    override fun id() = ID


    inner class CatalogsLabelViewHolder(private val binding: ItemCatalogsLabelBinding) :
        BaseViewHolder(binding) {
        override fun onBind(item: IListItem) {}
    }
}