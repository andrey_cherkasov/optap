package ru.optap.ui.search.data

import ru.optap.ui.base.listview.adapter.IListItem

class CategoriesLabel : IListItem {
    override fun equals(other: Any?): Boolean = this === other
    override fun hashCode(): Int = javaClass.hashCode()
}