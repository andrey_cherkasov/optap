package ru.optap.ui.category.data

data class ViewMode(val mode: Int, val position: Int?) {
    companion object {
        const val LIST = 1
        const val GRID = 2
        const val GRID_DELETED_ITEMS = 3
        const val GRID_DELETED_CATEGORY = 4
    }
}