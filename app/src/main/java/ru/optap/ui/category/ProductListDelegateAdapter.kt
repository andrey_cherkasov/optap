package ru.optap.ui.category

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import com.bumptech.glide.Glide
import ru.optap.R
import ru.optap.databinding.ItemProductListBinding
import ru.optap.ui.base.listview.adapter.BaseDelegateAdapter
import ru.optap.ui.base.listview.adapter.BaseViewHolder
import ru.optap.ui.base.listview.adapter.IListItem
import ru.optap.ui.base.listview.adapter.OnItemListViewClickListener
import ru.optap.ui.category.data.ViewMode
import ru.optap.ui.category.data.ProductVO

class ProductListDelegateAdapter : BaseDelegateAdapter {

    companion object {
        const val ID = R.layout.item_product_list
    }

    private var listener: OnItemListViewClickListener<ProductVO>? = null
    private var longListener: OnItemListViewClickListener<ProductVO>? = null
    private var cartListener: ((product: ProductVO) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ProductListViewHolder(
        ItemProductListBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
    )

    override fun onBindViewHolder(holder: BaseViewHolder, item: IListItem) = holder.onBind(item)

    override fun isForViewType(item: IListItem) =
        item is ProductVO && item.viewMode == ViewMode.LIST

    override fun id() = R.layout.item_product_list


    fun setItemClickListener(listener: OnItemListViewClickListener<ProductVO>) {
        this.listener = listener
    }

    fun setItemLongClickListener(listener: OnItemListViewClickListener<ProductVO>) {
        this.longListener = listener
    }

    fun setCartListener(listener: (product: ProductVO) -> Unit) {
        cartListener = listener
    }


    inner class ProductListViewHolder(private val binding: ItemProductListBinding) :
        BaseViewHolder(binding) {
        override fun onBind(item: IListItem) {
            val product = item as ProductVO
            val context = binding.root.context
            Glide.with(binding.image.context).load(product.image).into(binding.image)

            binding.packCount.text =
                context.getString(R.string.amount_per_pack_value, product.packCount)
            binding.packCount.isVisible = product.packCount != 0

            binding.boxCount.text =
                context.getString(R.string.amount_per_box_value, product.boxCount)
            binding.boxCount.isVisible = product.boxCount != 0

            binding.title.text = product.name
            binding.price.text = product.priceString
            binding.description.text = product.description
            binding.description.isVisible = product.description != ""

            binding.root.setOnClickListener {
                listener?.onClick(product, absoluteAdapterPosition)
            }
            binding.root.setOnLongClickListener {
                longListener?.onClick(product, absoluteAdapterPosition)
                true
            }
            if (cartListener != null) {
                binding.add.setOnClickListener { cartListener?.invoke(product) }
            } else {
                binding.add.isVisible = false
            }
        }
    }

}