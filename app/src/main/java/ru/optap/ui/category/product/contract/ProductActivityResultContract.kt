package ru.optap.ui.category.product.contract

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.activity.result.contract.ActivityResultContract
import ru.optap.ui.category.data.ProductVO
import ru.optap.ui.category.product.ProductActivity

class ProductActivityResultContract : ActivityResultContract<ProductVO, ProductVO?>() {
    override fun createIntent(context: Context, input: ProductVO) =
        Intent(context, ProductActivity::class.java).putExtra(ProductActivity.PRODUCT, input)

    override fun parseResult(resultCode: Int, intent: Intent?): ProductVO? = when {
        resultCode != Activity.RESULT_OK -> null
        else -> intent?.getParcelableExtra(ProductActivity.PRODUCT)
    }
}