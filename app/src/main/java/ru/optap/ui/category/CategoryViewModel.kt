package ru.optap.ui.category

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.work.OneTimeWorkRequest
import androidx.work.WorkManager
import androidx.work.workDataOf
import com.nguyenhoanglam.imagepicker.model.Image
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ru.optap.data.Resource
import ru.optap.data.local.SharedPreferencesHelper
import ru.optap.data.local.db.CatalogDao
import ru.optap.data.local.db.ImageProductDao
import ru.optap.data.model.DataList
import ru.optap.data.model.ImageProduct
import ru.optap.data.remote.CreateProductWorker
import ru.optap.data.remote.DataApiInterface
import ru.optap.ui.base.exception.ApiErrorException
import ru.optap.ui.base.exception.ServerErrorException
import ru.optap.ui.base.listview.adapter.EmptyList
import ru.optap.ui.base.listview.adapter.IListItem
import ru.optap.ui.base.listview.adapter.Loading
import ru.optap.ui.base.viewmodel.BaseListViewModel
import ru.optap.ui.catalog.data.CategoryMapper
import ru.optap.ui.catalog.data.CategoryVO
import ru.optap.ui.category.data.ProductMapper
import ru.optap.ui.category.data.ProductVO
import ru.optap.ui.category.data.ViewMode
import ru.optap.util.Utils
import java.util.concurrent.atomic.AtomicBoolean
import javax.inject.Inject


@HiltViewModel
class CategoryViewModel @Inject constructor(
    private val app: Application,
    private val dataApi: DataApiInterface,
    private val categoryMapper: CategoryMapper,
    private val productMapper: ProductMapper,
    private val imageProductDao: ImageProductDao,
    private val utils: Utils,
    catalogDao: CatalogDao,
    preferencesHelper: SharedPreferencesHelper
) : BaseListViewModel() {

    private var subCategoryCount: Int = 0
    private var listLoading: AtomicBoolean = AtomicBoolean(false)

    private val categoryLiveData: MutableLiveData<CategoryVO> = MutableLiveData()
    private val categoryDeleteRestoreLiveData: MutableLiveData<Resource<CategoryVO>> =
        MutableLiveData()
    private val categoryListLiveData: MutableLiveData<Resource<DataList<IListItem>>> =
        MutableLiveData()

    private val categorySelectionModeLiveData: MutableLiveData<Boolean> = MutableLiveData(false)
    private val categoryViewModeLiveData: MutableLiveData<ViewMode> = MutableLiveData()

    val user = preferencesHelper.loadUser()
    var catalog = catalogDao.get()

    val category get():LiveData<CategoryVO> = categoryLiveData
    val categoryDeleteRestore get():LiveData<Resource<CategoryVO>> = categoryDeleteRestoreLiveData
    val categoryList get(): LiveData<Resource<DataList<IListItem>>> = categoryListLiveData
    val categoryViewMode get(): LiveData<ViewMode> = categoryViewModeLiveData
    val categorySelectionMode get(): LiveData<Boolean> = categorySelectionModeLiveData

    fun setCategory(category: CategoryVO) {
        categoryLiveData.value = category
        categoryViewModeLiveData.value =
            ViewMode(
                if (category.isDeleted) ViewMode.GRID_DELETED_CATEGORY else ViewMode.GRID,
                null
            )
    }

    fun invalidateViewMode() {
        categoryViewModeLiveData.value = categoryViewMode.value
    }

    fun refresh() {
        page = 1
        categoryListLiveData.value = Resource.success(DataList(listOf(Loading()), page, 0))

        disposable.add(
            dataApi.getSubCategories(category.value!!.id)
                .subscribeOn(Schedulers.io())
                .map { it.data }
                .map {
                    DataList<IListItem>(
                        it.dataList.map { category -> categoryMapper.ToVo().map(category) },
                        page,
                        it.total
                    )
                }
                .doOnSubscribe { listLoading.set(true) }
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally {
                    listLoading.set(false)
                    load()
                }
                .subscribe({
                    if (it != null) {
                        val list = mutableListOf<IListItem>()
                        list.addAll(categoryList.value?.data?.dataList ?: listOf())
                        list.removeLastOrNull()
                        list.addAll(it.dataList)
                        subCategoryCount = it.total
                        if (it.total > 0 && it.total % 2 != 0) list.add(CategoryVO.createEmpty())
                        list.add(Loading())
                        categoryListLiveData.value =
                            Resource.success(DataList(list, page, it.total))
                    } else {
                        throw ServerErrorException()
                    }
                }, { t -> Resource.error(t, null) })
        )
    }

    fun load() {
        if (listLoading.get()) return
        if (page > categoryList.value?.data?.lastPage ?: 1) return

        val showDeleted = if (ViewMode.GRID_DELETED_ITEMS == categoryViewMode.value?.mode) 1 else 0

        disposable.add(
            dataApi.getProducts(category.value!!.id, page, showDeleted)
                .subscribeOn(Schedulers.io())
                .doOnSuccess { if (!it.meta.success) throw ApiErrorException(it.meta.message) }
                .map { it.data }
                .map {
                    DataList<IListItem>(
                        it.dataList.map { product -> productMapper.ToVo().map(product) },
                        page,
                        it.total
                    )
                }
                .doOnSubscribe { listLoading.set(true) }
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally { listLoading.set(false) }
                .subscribe({
                    if (it != null) {
                        val list = ArrayList<IListItem>(
                            categoryList.value?.data!!.dataList.size + 30
                        )
                        list.addAll(categoryList.value?.data?.dataList ?: listOf())
                        list.removeLastOrNull()
                        list.addAll(it.dataList)
                        if (page == 1 && list.isEmpty()) list.add(EmptyList())
                        if (page < it.lastPage) list.add(Loading())
                        categoryListLiveData.value =
                            Resource.success(DataList(list, page, it.total))
                        page++
                    } else {
                        throw ServerErrorException()
                    }
                }, { t -> Resource.error(t, null) })
        )
    }

    fun setCategorySelectionMode(mode: Boolean) {
        categorySelectionModeLiveData.value = mode
    }

    fun changeDeleteMode() {
        changeViewMode(
            if (categoryViewMode.value?.mode == ViewMode.GRID_DELETED_ITEMS)
                ViewMode(ViewMode.GRID, null)
            else
                ViewMode(ViewMode.GRID_DELETED_ITEMS, null)
        )
        refresh()
    }

    fun changeViewMode(position: Int?) {
        changeViewMode(
            if (categoryViewMode.value?.mode == ViewMode.GRID)
                ViewMode(ViewMode.LIST, position)
            else
                ViewMode(ViewMode.GRID, position)
        )
    }

    private fun changeViewMode(viewMode: ViewMode) {
        disposable.add(Single.fromCallable {
            val list = categoryList.value?.data?.dataList?.toMutableList() ?: mutableListOf()
            val total = categoryList.value?.data?.total ?: 0
            val iterator = list.listIterator()
            while (iterator.hasNext()) {
                val item = iterator.next()
                if (item is ProductVO) iterator.set(item.copy(viewMode = viewMode.mode))
            }
            DataList(list, page, total)
        }
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    categoryViewModeLiveData.value = viewMode
                    categoryListLiveData.value = Resource.success(it)
                },
                { t -> t.printStackTrace() }
            )
        )
    }

    fun selectItem(position: Int) {
        disposable.add(Single.fromCallable {
            val list = categoryList.value?.data?.dataList?.toMutableList() ?: mutableListOf()
            val total = categoryList.value?.data?.total ?: 0
            val item = list[position]
            if (item is ProductVO) {
                list[position] = item.copy(selected = !item.selected)
            }
            DataList(list, page, total)
        }
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { categoryListLiveData.value = Resource.success(it) },
                { t -> t.printStackTrace() }
            )
        )
    }

    fun clickListItem(item: IListItem, position: Int) {
        if (item is ProductVO) {
            if (categorySelectionMode.value!!) {
                selectItem(position)
            } else {
                if (categoryViewMode.value!!.mode == ViewMode.GRID) {
                    changeViewMode(ViewMode(ViewMode.LIST, position))
                }
            }
        }
    }

    fun unselect() {
        disposable.add(Single.fromCallable {
            val list = categoryList.value?.data?.dataList?.toMutableList() ?: mutableListOf()
            val total = categoryList.value?.data?.total ?: 0
            val iterator = list.listIterator()
            while (iterator.hasNext()) {
                val item = iterator.next()
                if (item is ProductVO && item.selected) iterator.set(item.copy(selected = false))
            }

            DataList(list, page, total)
        }
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    categoryListLiveData.value = Resource.success(it)
                    categorySelectionModeLiveData.value = false
                },
                { t -> t.printStackTrace() }
            )
        )
    }

    fun performSelected() {
        val list = categoryList.value!!.data!!.dataList.toMutableList()
        val selectedProductIdsList: MutableList<Int> = mutableListOf()
        for (item in list) {
            if (item is ProductVO && item.selected) selectedProductIdsList.add(item.id)
        }

        val selectedSingle = if (categoryViewMode.value?.mode == ViewMode.GRID) {
            dataApi.deleteProducts(category.value!!.id, selectedProductIdsList)
        } else {
            dataApi.restoreProducts(category.value!!.id, selectedProductIdsList)
        }

        disposable.add(selectedSingle.subscribeOn(Schedulers.io())
            .doOnSuccess { if (!it.meta.success) throw ApiErrorException(it.meta.message) }
            .map {
                val iterator = list.iterator()
                while (iterator.hasNext()) {
                    val item = iterator.next()
                    if (item is ProductVO && item.selected) iterator.remove()
                }

                DataList(list, page, categoryList.value?.data?.total ?: 0)
            }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                categoryListLiveData.value = Resource.success(it)
                categorySelectionModeLiveData.value = false
            }, { t ->
                categoryListLiveData.value = Resource.error(t, categoryList.value!!.data)
            })
        )
    }

    fun performDeleteRestoreCategory() {
        val deleteRestoreSingle =
            if (categoryViewMode.value!!.mode == ViewMode.GRID_DELETED_CATEGORY) {
                dataApi.restoreCategory(category.value!!.id)
            } else {
                dataApi.deleteCategory(category.value!!.id)
            }

        disposable.add(
            deleteRestoreSingle.subscribeOn(Schedulers.io())
                .doOnSubscribe { categoryDeleteRestoreLiveData.value = Resource.loading(null) }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.meta.success) {
                        categoryDeleteRestoreLiveData.value = Resource.success(
                            category.value?.copy(isDeleted = !category.value!!.isDeleted)
                        )
                    } else {
                        throw ApiErrorException(it.meta.message)
                    }
                }, { t -> categoryDeleteRestoreLiveData.value = Resource.error(t, null) })
        )
    }

    fun createSubCategory(category: CategoryVO) {
        disposable.add(Single.fromCallable {
            val list = categoryList.value?.data?.dataList?.toMutableList() ?: mutableListOf()
            val total = categoryList.value?.data?.total ?: 0
            if (list[0] is EmptyList) list.removeAt(0)
            if (subCategoryCount > 0 && subCategoryCount % 2 != 0) {
                list.removeAt(subCategoryCount)
            } else {
                list.add(subCategoryCount, CategoryVO.createEmpty())
            }
            list.add(0, category)

            DataList(list, page, total)
        }
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    categoryListLiveData.value = Resource.success(it)
                    categorySelectionModeLiveData.value = false
                    subCategoryCount++
                },
                { t -> t.printStackTrace() }
            )
        )
    }

    fun deleteEditCategory(category: CategoryVO) {
        disposable.add(Single.fromCallable {
            val list = categoryList.value?.data?.dataList?.toMutableList() ?: mutableListOf()
            val total = categoryList.value?.data?.total ?: 0
            if (category.isDeleted) {
                if (subCategoryCount > 0 && subCategoryCount % 2 != 0) {
                    list.removeAt(subCategoryCount)
                } else {
                    list.add(subCategoryCount, CategoryVO.createEmpty())
                }
            }

            val iterator = list.listIterator()
            while (iterator.hasNext()) {
                val item = iterator.next()
                if (item is CategoryVO && item.id == category.id) {
                    if (category.isDeleted) {
                        iterator.remove()
                    } else {
                        category.isUpdated = 0
                        iterator.set(category)
                    }
                    break
                }
            }

            DataList(list, page, total)
        }
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    categoryListLiveData.value = Resource.success(it)
                    subCategoryCount--
                },
                { t -> t.printStackTrace() }
            )
        )
    }

    fun updateProduct(product: ProductVO) {
        disposable.add(Single.fromCallable {
            val list = categoryList.value?.data?.dataList?.toMutableList() ?: mutableListOf()
            val total = categoryList.value?.data?.total ?: 0

            val iterator = list.listIterator()
            while (iterator.hasNext()) {
                val item = iterator.next()
                if (item is ProductVO && item.id == product.id) iterator.set(product)
            }

            DataList(list, page, total)
        }
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { categoryListLiveData.value = Resource.success(it) },
                { t -> t.printStackTrace() }
            )
        )
    }

    fun addImageProducts(items: List<Image>) {
        disposable.add(
            Single.fromCallable {
                val categoryId = category.value!!.id
                val imageProducts = items.map { item ->
                    ImageProduct(
                        0,
                        utils.getRealPathFromUri(item.uri),
                        categoryId,
                        ImageProduct.CREATED
                    )
                }

                imageProductDao.getAll(categoryId)
                val startWorker: Boolean = imageProductDao.getAll(categoryId).isEmpty()
                imageProductDao.insertAll(imageProducts)
                startWorker
            }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ startWorker ->
                    if (startWorker) {
                        val workRequest =
                            OneTimeWorkRequest.Builder(CreateProductWorker::class.java)
                                .setInputData(
                                    workDataOf(CreateProductWorker.CATEGORY_ID to category.value!!.id)
                                )
                                .addTag(category.value?.id.toString())
                                .build()

                        WorkManager.getInstance(app).enqueue(workRequest)
                    }
                }, { t -> t.printStackTrace() })
        )
    }

    fun addProduct(product: ProductVO) {
        disposable.add(Single.fromCallable {
            val list = categoryList.value?.data?.dataList?.toMutableList() ?: mutableListOf()
            val total = categoryList.value?.data?.total ?: 0
            product.viewMode = categoryViewMode.value?.mode ?: ViewMode.GRID
            if (list[0] is EmptyList) list.removeAt(0)

            if (subCategoryCount > 0 && subCategoryCount % 2 != 0) {
                list.add(subCategoryCount + 1, product)
            } else {
                list.add(subCategoryCount, product)
            }

            DataList(list, page, total)
        }
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { categoryListLiveData.value = Resource.success(it) },
                { t -> t.printStackTrace() }
            )
        )
    }

    fun checkCatalog() = catalog.id == category.value?.catalogId

}