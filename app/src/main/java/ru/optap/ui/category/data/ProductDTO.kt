package ru.optap.ui.category.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Products")
data class ProductDTO(
    @PrimaryKey val id: Int,
    @ColumnInfo(name = "category_id") val categoryId: Int,
    @ColumnInfo(name = "name") var name: String?,
    @ColumnInfo(name = "price") var price: Int?,
    @ColumnInfo(name = "pack_count") var packCount: Int?,
    @ColumnInfo(name = "box_count") var boxCount: Int?,
    @ColumnInfo(name = "image") var image: String?,
    @ColumnInfo(name = "description") var description: String?,
    @ColumnInfo(name = "count") var count: Int,
    @ColumnInfo(name = "type_id") var typeId: Int,
    @ColumnInfo(name = "colored") var colored: Int?
)