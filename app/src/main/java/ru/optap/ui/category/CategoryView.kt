package ru.optap.ui.category

import ru.optap.ui.catalog.data.CategoryVO
import ru.optap.ui.category.data.ProductVO

interface CategoryView {
    fun setViewMode(viewMode: Int)
    fun setDeleteMode(delete: Boolean)
    fun showAddDialog()
    fun showProductDialog(product: ProductVO)
    fun showAlertDialog()
    fun showAddProductWayDialog()
    fun openAddCategoryScreen()
    fun openCameraScreen()
    fun openCategoryScreen(category: CategoryVO)
}