package ru.optap.ui.category.data

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Product(
    @Json(name = "id") val id: Int,
    @Json(name = "category_id") val categoryId: Int,
    @Json(name = "name") val name: String?,
    @Json(name = "price") val price: Int?,
    @Json(name = "pack_count") val packCount: Int?,
    @Json(name = "box_count") val boxCount: Int?,
    @Json(name = "image") val image: String?,
    @Json(name = "description") val description: String?,
    @Json(name = "count") val count: Int?,
    @Json(name = "type_id") val typeId: Int?,
    @Json(name = "colored") val colored: Int?
)