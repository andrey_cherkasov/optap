package ru.optap.ui.category

import android.Manifest
import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.GridLayoutManager
import com.bumptech.glide.Glide
import com.nguyenhoanglam.imagepicker.ui.imagepicker.registerImagePicker
import dagger.hilt.android.AndroidEntryPoint
import jp.wasabeef.glide.transformations.CropCircleWithBorderTransformation
import jp.wasabeef.glide.transformations.internal.Utils
import ru.optap.R
import ru.optap.data.DataStatus
import ru.optap.data.model.User
import ru.optap.databinding.ActivityCategoryBinding
import ru.optap.ui.base.listview.PaginationScrollListener
import ru.optap.ui.base.listview.adapter.BasePagingAdapter
import ru.optap.ui.base.listview.adapter.EmptyListDelegateAdapter
import ru.optap.ui.base.listview.adapter.IListItem
import ru.optap.ui.base.listview.adapter.LoadingDelegateAdapter
import ru.optap.ui.base.view.BaseActivity
import ru.optap.ui.camera.contract.CameraActivityResultContract
import ru.optap.ui.catalog.CatalogDelegateAdapter
import ru.optap.ui.catalog.data.CategoryVO
import ru.optap.ui.catalog.instance.CategoryInstanceActivity
import ru.optap.ui.catalog.instance.contract.CategoryCreateResultContract
import ru.optap.ui.catalog.instance.contract.CategoryUpdateResultContract
import ru.optap.ui.category.contract.CategoryActivityResultContract
import ru.optap.ui.category.data.ProductVO
import ru.optap.ui.category.data.ViewMode
import ru.optap.ui.category.product.contract.ProductActivityResultContract
import ru.optap.ui.dialog.product.ProductDialog
import javax.inject.Inject

@AndroidEntryPoint
class CategoryActivity : BaseActivity(), CategoryView {

    companion object {
        const val ACTION_NEW_PRODUCT = "ru.optap.action.new_product"
        const val PRODUCT = "PRODUCT"
    }

    @Inject
    lateinit var utils: ru.optap.util.Utils

    private val viewModel: CategoryViewModel by viewModels()
    private lateinit var binding: ActivityCategoryBinding
    private lateinit var adapter: BasePagingAdapter
    private lateinit var productGridDelegateAdapter: ProductGridDelegateAdapter

    // menu
    private var viewModeMenu: MenuItem? = null
    private var performSelectedMenu: MenuItem? = null
    private var editCategoryMenu: MenuItem? = null
    private var deleteModeMenu: MenuItem? = null
    private var deleteCategoryMenu: MenuItem? = null

    private var productReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent?) {
            intent?.getParcelableExtra<ProductVO>(PRODUCT)?.let { viewModel.addProduct(it) }
        }
    }

    private val categoryUpdateResultLauncher =
        registerForActivityResult(CategoryUpdateResultContract()) { result ->
            val intent =
                Intent().putExtra(CategoryInstanceActivity.CATEGORY, result)
            setResult(Activity.RESULT_OK, intent)
            result?.let { viewModel.setCategory(it) }
        }

    private val categoryCreateResultLauncher =
        registerForActivityResult(CategoryCreateResultContract()) { result ->
            result?.let { viewModel.createSubCategory(it) }
        }

    private val deleteRestoreResultLauncher =
        registerForActivityResult(CategoryActivityResultContract()) { result ->
            result?.let { viewModel.deleteEditCategory(it) }
        }

    private val productResultLauncher =
        registerForActivityResult(ProductActivityResultContract()) { result ->
            result?.let { viewModel.updateProduct(it) }
        }

    private val cameraResultLauncher =
        registerForActivityResult(CameraActivityResultContract()) { result ->
            if (result != null && result) viewModel.refresh()
        }

    private val imagePickerLauncher = registerImagePicker { viewModel.addImageProducts(it) }

    private val requestPermissionLauncher =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) {
            if (it) openCameraScreen()
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCategoryBinding.inflate(LayoutInflater.from(this))
        setContentView(binding.root)
        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        initList()
        initObservers()

        binding.fab.setOnClickListener { showAddDialog() }

        if (savedInstanceState == null) {
            val category =
                intent.getParcelableExtra<CategoryVO>(CategoryInstanceActivity.CATEGORY)
            if (category != null) {
                viewModel.setCategory(category)
            } else {
                finish()
            }
            viewModel.refresh()
        }
    }

    private fun initList() {
        val catalogDelegateAdapter = CatalogDelegateAdapter()
        productGridDelegateAdapter = ProductGridDelegateAdapter()
        val productListDelegateAdapter = ProductListDelegateAdapter()

        productGridDelegateAdapter.setItemClickListener { item, position ->
            viewModel.clickListItem(item, position)
        }
        productGridDelegateAdapter.setItemLongClickListener { _, position ->
            if (viewModel.user?.role == User.VENDOR) {
                if (!viewModel.categorySelectionMode.value!!) {
                    if (viewModel.categoryViewMode.value?.mode == ViewMode.GRID
                        || viewModel.categoryViewMode.value?.mode == ViewMode.GRID_DELETED_ITEMS
                    ) {
                        viewModel.setCategorySelectionMode(true)
                        viewModel.selectItem(position)
                    }
                }
            }
        }
        productListDelegateAdapter.setItemClickListener { _, _ ->
            // TODO
        }
        productListDelegateAdapter.setItemLongClickListener { item, _ ->
            productResultLauncher.launch(item)
        }
        if (viewModel.user?.role == User.CUSTOMER) {
            productListDelegateAdapter.setCartListener {
                if (viewModel.checkCatalog()) showProductDialog(it) else showAlertDialog()
            }
        }
        catalogDelegateAdapter.setOnItemListViewClickListener { item, _ ->
            if (viewModel.categoryViewMode.value?.mode != ViewMode.GRID_DELETED_CATEGORY) {
                openCategoryScreen(item)
            }
        }

        val adapterDelegates = mapOf(
            CatalogDelegateAdapter.ID to catalogDelegateAdapter,
            ProductGridDelegateAdapter.ID to productGridDelegateAdapter,
            ProductListDelegateAdapter.ID to productListDelegateAdapter,
            LoadingDelegateAdapter.ID to LoadingDelegateAdapter(),
            EmptyListDelegateAdapter.ID to EmptyListDelegateAdapter()
        )

        val diffUtil = object : DiffUtil.ItemCallback<IListItem>() {
            override fun areItemsTheSame(oldItem: IListItem, newItem: IListItem): Boolean {
                return if (oldItem is CategoryVO && newItem is CategoryVO) {
                    oldItem.id == newItem.id
                } else if (oldItem is ProductVO && newItem is ProductVO) {
                    oldItem.id == newItem.id
                } else {
                    false
                }
            }

            override fun areContentsTheSame(oldItem: IListItem, newItem: IListItem): Boolean {
                return if (oldItem is CategoryVO && newItem is CategoryVO) {
                    oldItem == newItem
                } else if (oldItem is ProductVO && newItem is ProductVO) {
                    oldItem == newItem
                } else {
                    false
                }
            }
        }

        val layoutManager = GridLayoutManager(this, 6)
        layoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                return when (adapter.getItemViewType(position)) {
                    CatalogDelegateAdapter.ID -> 3
                    ProductGridDelegateAdapter.ID -> 2
                    else -> 6
                }
            }
        }

        val scrollListener = object : PaginationScrollListener(layoutManager) {
            override fun loadMore() = viewModel.load()
        }

        adapter = BasePagingAdapter(adapterDelegates, diffUtil)
        binding.list.addRecyclerListener { }
        binding.list.apply {
            this.adapter = this@CategoryActivity.adapter
            this.layoutManager = layoutManager
            addOnScrollListener(scrollListener)
        }
    }

    private fun initObservers() {
        viewModel.categoryList.observe(this) {
            when (it.status) {
                DataStatus.LOADING -> {}
                DataStatus.SUCCESS -> {
                    adapter.submitList(it.data?.dataList) {
                        viewModel.categoryViewMode.value?.position?.let { it1 ->
                            binding.list.scrollToPosition(it1)
                        }
                    }
                }
                DataStatus.ERROR -> handleError(it.throwable)
            }
        }

        viewModel.category.observe(this) {
            supportActionBar?.title = if (it.name == "") getString(R.string.untitled) else it.name
            registerReceiver(productReceiver, IntentFilter(ACTION_NEW_PRODUCT + it.id))
            binding.collapsingToolbarLayout.title =
                if (it.name == "") getString(R.string.untitled) else it.name

            Glide.with(this)
                .load(it.image)
                .transform(
                    CropCircleWithBorderTransformation(
                        Utils.toDp(2),
                        ContextCompat.getColor(this, R.color.white)
                    )
                )
                .into(binding.image)
        }

        viewModel.categoryDeleteRestore.observe(this) {
            when (it.status) {
                DataStatus.LOADING -> {}
                DataStatus.SUCCESS -> {
                    val result = Intent().putExtra(CategoryInstanceActivity.CATEGORY, it.data)
                    setResult(Activity.RESULT_OK, result)
                    finish()
                }
                DataStatus.ERROR -> handleError(it.throwable)
            }
        }

        viewModel.categoryViewMode.observe(this) { setViewMode(it.mode) }
        viewModel.categorySelectionMode.observe(this) { setDeleteMode(it) }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.activity_category, menu)
        viewModeMenu = menu.findItem(R.id.view_mode)
        performSelectedMenu = menu.findItem(R.id.perform_selected)
        editCategoryMenu = menu.findItem(R.id.edit_category)
        deleteModeMenu = menu.findItem(R.id.delete_mode)
        deleteCategoryMenu = menu.findItem(R.id.delete_category)
        viewModel.invalidateViewMode()
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
            R.id.view_mode -> viewModel.changeViewMode(null)
            R.id.perform_selected -> viewModel.performSelected()
            R.id.edit_category -> categoryUpdateResultLauncher.launch(viewModel.category.value)
            R.id.delete_category -> viewModel.performDeleteRestoreCategory()
            R.id.delete_mode -> viewModel.changeDeleteMode()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        if (viewModel.categorySelectionMode.value!!) {
            viewModel.unselect()
        } else {
            finish()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(productReceiver)
    }

    override fun setViewMode(viewMode: Int) {
        performSelectedMenu?.isVisible = false
        deleteModeMenu?.isVisible = true

        if (viewMode == ViewMode.GRID_DELETED_ITEMS) {
            viewModeMenu?.setIcon(R.drawable.ic_view_grid)
            performSelectedMenu?.setIcon(R.drawable.ic_delete_restore)
            performSelectedMenu?.setTitle(R.string.restore_selected_products)
            viewModeMenu?.isVisible = false
            editCategoryMenu?.isVisible = false
            deleteCategoryMenu?.setTitle(R.string.delete_category)
            deleteCategoryMenu?.isVisible = false
            deleteModeMenu?.title = getString(R.string.show_current_items)
            binding.fab.isVisible = false
        } else if (viewMode == ViewMode.GRID_DELETED_CATEGORY) {
            viewModeMenu?.isVisible = false
            performSelectedMenu?.isVisible = false
            editCategoryMenu?.isVisible = false
            deleteModeMenu?.isVisible = false
            deleteCategoryMenu?.setTitle(R.string.restore_category)
            deleteCategoryMenu?.isVisible = true
            binding.fab.isVisible = false
        } else {
            viewModeMenu?.isVisible = true
            performSelectedMenu?.setIcon(R.drawable.ic_delete_outline)
            performSelectedMenu?.setTitle(R.string.delete_selected_products)
            editCategoryMenu?.isVisible = true
            deleteCategoryMenu?.setTitle(R.string.delete_category)
            deleteCategoryMenu?.isVisible = true
            deleteModeMenu?.title = getString(R.string.show_deleted_items)
            binding.fab.isVisible = true

            if (viewMode == ViewMode.LIST) {
                viewModeMenu?.setIcon(R.drawable.ic_view_grid)
            } else if (viewMode == ViewMode.GRID) {
                viewModeMenu?.setIcon(R.drawable.ic_format_list_bulleted_type)
            }
        }

        if (viewModel.user?.role == User.CUSTOMER) {
            performSelectedMenu?.isVisible = false
            deleteCategoryMenu?.isVisible = false
            deleteModeMenu?.isVisible = false
            editCategoryMenu?.isVisible = false
            binding.fab.isVisible = false
        }
    }

    override fun setDeleteMode(delete: Boolean) {
        if (delete) {
            performSelectedMenu?.isVisible = true
            viewModeMenu?.isVisible = false
            editCategoryMenu?.isVisible = false
            deleteCategoryMenu?.isVisible = false
            deleteModeMenu?.isVisible = false
            binding.fab.isVisible = false
        } else {
            viewModel.categoryViewMode.value?.mode?.let { setViewMode(it) }
        }
    }

    override fun showAddDialog() {
        AlertDialog.Builder(this)
            .setTitle(R.string.what_to_add)
            .setItems(R.array.what_to_add) { _, which ->
                when (which) {
                    0 -> openAddCategoryScreen()
                    1 -> showAddProductWayDialog()
                }
            }
            .create()
            .show()
    }

    override fun showProductDialog(product: ProductVO) {
        ProductDialog.getInstance(product, viewModel.category.value!!)
            .show(supportFragmentManager, ProductDialog.TAG)
    }

    override fun showAlertDialog() {
        AlertDialog.Builder(this)
            .setMessage(R.string.complete_order_in_one_catalog)
            .setPositiveButton(R.string.ok) { dialog, _ -> dialog.dismiss() }
            .create()
            .show()
    }

    override fun showAddProductWayDialog() {
        AlertDialog.Builder(this)
            .setTitle(R.string.choose_add_product_way)
            .setMessage(R.string.choose_add_product_way_description)
            .setPositiveButton(R.string.camera) { _, _ ->
                when (PackageManager.PERMISSION_GRANTED) {
                    ContextCompat.checkSelfPermission(
                        this,
                        Manifest.permission.CAMERA
                    ) -> openCameraScreen()
                    else -> requestPermissionLauncher.launch(Manifest.permission.CAMERA)
                }
            }
            .setNegativeButton(R.string.gallery) { _, _ ->
                imagePickerLauncher.launch(utils.getMultipleImagePickerConfig())
            }
            .create()
            .show()
    }

    override fun openAddCategoryScreen() =
        categoryCreateResultLauncher.launch(viewModel.category.value?.id)

    override fun openCameraScreen() = cameraResultLauncher.launch(viewModel.category.value)

    override fun openCategoryScreen(category: CategoryVO) {
        deleteRestoreResultLauncher.launch(category)
    }

}
