package ru.optap.ui.category.data

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import ru.optap.ui.base.listview.adapter.IListItem

@Parcelize
data class ProductVO(
    val id: Int,
    val categoryId: Int,
    val name: String,
    val price: Int,
    val priceString: String,
    val packCount: Int,
    val boxCount: Int,
    val image: String,
    val description: String,
    val count: Int,
    val countString: String,
    val typeId: Int,
    val colored: Int,
    val selected: Boolean,
    var viewMode: Int,
    val expand: Boolean
) : IListItem, Parcelable {

    val totalPrice: Int
        get() = price * count * when (typeId) {
            1 -> packCount
            2 -> boxCount
            else -> 1
        }

}