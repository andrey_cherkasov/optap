package ru.optap.ui.category.data

import ru.optap.data.Mapper

class ProductMapper {

    inner class ToVo : Mapper<Product, ProductVO> {
        override fun map(input: Product): ProductVO {
            return ProductVO(
                input.id,
                input.categoryId,
                input.name ?: "",
                input.price ?: 0,
                (input.price ?: 0).toString(),
                input.packCount ?: 0,
                input.boxCount ?: 0,
                input.image ?: "",
                input.description ?: "",
                input.count ?: 1,
                (input.count ?: 1).toString(),
                input.typeId ?: 0,
                input.colored ?: 0,
                false,
                ViewMode.GRID,
                true
            )
        }
    }

    inner class VoToDto : Mapper<ProductVO, ProductDTO> {
        override fun map(input: ProductVO): ProductDTO {
            return ProductDTO(
                input.id,
                input.categoryId,
                input.name,
                input.price,
                input.packCount,
                input.boxCount,
                input.image,
                input.description,
                input.count,
                input.typeId,
                input.colored
            )
        }
    }

    inner class DtoToVO : Mapper<ProductDTO, ProductVO> {
        override fun map(input: ProductDTO): ProductVO {
            return ProductVO(
                input.id,
                input.categoryId,
                input.name ?: "",
                input.price ?: 0,
                (input.price ?: 0).toString(),
                input.packCount ?: 0,
                input.boxCount ?: 0,
                input.image ?: "",
                input.description ?: "",
                input.count,
                input.count.toString(),
                input.typeId,
                input.colored ?: 0,
                false,
                ViewMode.GRID,
                true
            )
        }
    }

}