package ru.optap.ui.category

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import com.bumptech.glide.Glide
import jp.wasabeef.glide.transformations.BlurTransformation
import ru.optap.R
import ru.optap.databinding.ItemProductGridBinding
import ru.optap.ui.base.listview.adapter.BaseDelegateAdapter
import ru.optap.ui.base.listview.adapter.BaseViewHolder
import ru.optap.ui.base.listview.adapter.IListItem
import ru.optap.ui.base.listview.adapter.OnItemListViewClickListener
import ru.optap.ui.category.data.ViewMode
import ru.optap.ui.category.data.ProductVO

class ProductGridDelegateAdapter : BaseDelegateAdapter {

    companion object {
        const val ID = R.layout.item_product_grid
    }

    private var listener: OnItemListViewClickListener<ProductVO>? = null
    private var longListener: OnItemListViewClickListener<ProductVO>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ProductGridViewHolder(
        ItemProductGridBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
    )

    override fun onBindViewHolder(holder: BaseViewHolder, item: IListItem) = holder.onBind(item)

    override fun isForViewType(item: IListItem) =
        item is ProductVO && (item.viewMode == ViewMode.GRID || item.viewMode == ViewMode.GRID_DELETED_ITEMS)

    override fun id() = R.layout.item_product_grid


    fun setItemClickListener(listener: OnItemListViewClickListener<ProductVO>) {
        this.listener = listener
    }

    fun setItemLongClickListener(listener: OnItemListViewClickListener<ProductVO>) {
        this.longListener = listener
    }


    inner class ProductGridViewHolder(private val binding: ItemProductGridBinding) :
        BaseViewHolder(binding) {
        override fun onBind(item: IListItem) {
            val product = item as ProductVO

            var glide = Glide.with(binding.image.context).load(product.image)
            if (product.selected) {
                glide = glide.transform(BlurTransformation(25, 1))
            }
            glide.into(binding.image)
            binding.check.isVisible = product.selected

            binding.root.setOnClickListener {
                listener?.onClick(product, absoluteAdapterPosition)
            }
            binding.root.setOnLongClickListener {
                longListener?.onClick(product, absoluteAdapterPosition)
                true
            }
        }
    }

}