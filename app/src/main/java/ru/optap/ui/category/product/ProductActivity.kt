package ru.optap.ui.category.product

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.core.view.isVisible
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import dagger.hilt.android.AndroidEntryPoint
import ru.optap.R
import ru.optap.data.DataStatus
import ru.optap.databinding.ActivityProductBinding
import ru.optap.ui.base.view.BaseActivity
import ru.optap.ui.base.view.LoadingView
import ru.optap.ui.category.data.ProductVO

@AndroidEntryPoint
class ProductActivity : BaseActivity(), LoadingView<ProductVO> {

    companion object {
        const val PRODUCT = "PRODUCT"
    }

    private lateinit var binding: ActivityProductBinding
    private val viewModel: ProductViewModel by viewModels()
    private lateinit var product: ProductVO

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityProductBinding.inflate(LayoutInflater.from(this))
        setContentView(binding.root)
        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setTitle(R.string.edit_product)

        product = intent.getParcelableExtra(PRODUCT)!!

        product.let {
            binding.nameEditText.setText(it.name)
            binding.descriptionEditText.setText(it.description)
            binding.packCountEditText.setText(it.packCount.toString())
            binding.boxCountEditText.setText(it.boxCount.toString())
            binding.priceEditText.setText(it.priceString)
            Glide.with(this).load(it.image).transform(CircleCrop()).into(binding.image)
        }

        viewModel.product.observe(this) {
            showLoading(false)
            when (it.status) {
                DataStatus.LOADING -> showLoading(true)
                DataStatus.SUCCESS -> it.data?.let { it1 -> setData(it1) }
                DataStatus.ERROR -> handleError(it.throwable)
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.activity_product, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.ok) {
            viewModel.save(
                product.id,
                product.categoryId,
                binding.nameEditText.text.toString(),
                binding.descriptionEditText.text.toString(),
                binding.packCountEditText.text.toString().toIntOrNull() ?: 0,
                binding.boxCountEditText.text.toString().toIntOrNull() ?: 0,
                binding.priceEditText.text.toString().toIntOrNull() ?: 0,
            )
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun showLoading(loading: Boolean) {
        binding.loadingLayout.root.isVisible = loading
    }

    override fun setData(data: ProductVO) {
        val result = Intent().putExtra(PRODUCT, data)
        setResult(Activity.RESULT_OK, result)
        finish()
    }

}