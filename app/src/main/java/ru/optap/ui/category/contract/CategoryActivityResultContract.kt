package ru.optap.ui.category.contract

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.activity.result.contract.ActivityResultContract
import ru.optap.ui.catalog.data.CategoryVO
import ru.optap.ui.catalog.instance.CategoryInstanceActivity
import ru.optap.ui.category.CategoryActivity

class CategoryActivityResultContract : ActivityResultContract<CategoryVO, CategoryVO?>() {

    override fun createIntent(context: Context, input: CategoryVO): Intent =
        Intent(context, CategoryActivity::class.java).putExtra(
            CategoryInstanceActivity.CATEGORY,
            input
        )

    override fun parseResult(resultCode: Int, intent: Intent?): CategoryVO? = when {
        resultCode != Activity.RESULT_OK -> null
        else -> intent?.getParcelableExtra(CategoryInstanceActivity.CATEGORY)
    }

}