package ru.optap.ui.category.product

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ru.optap.data.Resource
import ru.optap.data.remote.DataApiInterface
import ru.optap.ui.base.exception.ApiErrorException
import ru.optap.ui.base.viewmodel.BaseViewModel
import ru.optap.ui.category.data.ProductMapper
import ru.optap.ui.category.data.ProductVO
import ru.optap.ui.category.data.ViewMode
import javax.inject.Inject

@HiltViewModel
class ProductViewModel @Inject constructor(
    private val dataApi: DataApiInterface,
    private val productMapper: ProductMapper
) : BaseViewModel() {

    private val productLiveData: MutableLiveData<Resource<ProductVO>> = MutableLiveData()

    val product: LiveData<Resource<ProductVO>> get() = productLiveData

    fun save(
        productId: Int,
        categoryId: Int,
        name: String,
        description: String,
        packCount: Int,
        boxCount: Int,
        price: Int
    ) {
        disposable.add(
            dataApi.updateProduct(
                productId,
                categoryId,
                name,
                description,
                packCount,
                boxCount,
                price
            )
                .subscribeOn(Schedulers.io())
                .doOnSubscribe { Resource.loading(null) }
                .doOnSuccess { if (!it.meta.success) throw ApiErrorException(it.meta.message) }
                .map { it.data }
                .map { productMapper.ToVo().map(it) }
                .map { it.copy(viewMode = ViewMode.LIST) }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { productLiveData.value = Resource.success(it) },
                    { t -> productLiveData.value = Resource.error(t, null) }
                )
        )
    }

}