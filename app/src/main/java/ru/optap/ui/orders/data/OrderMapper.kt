package ru.optap.ui.orders.data

import ru.optap.data.Mapper
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*


class OrderMapper : Mapper<Order, OrderVO> {
    override fun map(input: Order): OrderVO {
        val inputFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd HH:m:s", Locale.getDefault())
        val outputFormat: DateFormat = SimpleDateFormat("HH:mm dd.MM.yyyy", Locale.getDefault())
        var dateString = ""
        try {
            val date = inputFormat.parse(input.createdAt ?: "")
            if (date != null) {
                date.time = date.time + TimeZone.getDefault().rawOffset
                dateString = outputFormat.format(date)
            }
        } catch (e: Exception) {
        }

        return OrderVO(
            input.id,
            String.format(Locale.getDefault(), "#%d", input.id),
            dateString,
            input.fullname ?: "",
            input.phone ?: "",
            input.image ?: "",
            input.totalPrice ?: 0,
            input.totalPrice.toString(),
            input.colored ?: 0
        )
    }
}