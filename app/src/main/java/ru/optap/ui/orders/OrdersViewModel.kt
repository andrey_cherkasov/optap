package ru.optap.ui.orders

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ru.optap.data.DataStatus
import ru.optap.data.Resource
import ru.optap.data.model.DataList
import ru.optap.data.remote.DataApiInterface
import ru.optap.ui.base.SingleLiveEvent
import ru.optap.ui.base.exception.ApiErrorException
import ru.optap.ui.base.exception.ServerErrorException
import ru.optap.ui.base.listview.adapter.EmptyList
import ru.optap.ui.base.listview.adapter.IListItem
import ru.optap.ui.base.listview.adapter.Loading
import ru.optap.ui.base.viewmodel.BaseListViewModel
import ru.optap.ui.orders.data.OrderMapper
import ru.optap.ui.orders.data.OrderVO
import javax.inject.Inject

@HiltViewModel
class OrdersViewModel @Inject constructor(
    private val dataApi: DataApiInterface,
    private val orderMapper: OrderMapper
) : BaseListViewModel() {

    private val ordersLiveData: MutableLiveData<Resource<DataList<IListItem>>> = MutableLiveData()
    private val messageLiveData: SingleLiveEvent<String> = SingleLiveEvent()

    val orders: LiveData<Resource<DataList<IListItem>>> get() = ordersLiveData
    val message: LiveData<String> get() = messageLiveData


    fun refresh() {
        page = 1
        ordersLiveData.value = Resource.success(DataList(listOf(Loading()), page, 0))
        load()
    }

    fun load() {
        if (orders.value?.status == DataStatus.LOADING) return
        if (page > orders.value?.data?.lastPage ?: 1) return

        disposable.add(
            dataApi.getOrders(page)
                .subscribeOn(Schedulers.io())
                .doOnSuccess { response ->
                    if (!response.meta.success) throw ApiErrorException(response.meta.message)
                }
                .map { it.data }
                .map {
                    DataList<IListItem>(
                        it.dataList.map { order -> orderMapper.map(order) },
                        page,
                        it.total
                    )
                }
                .doOnSubscribe {
                    ordersLiveData.value = Resource.loading(orders.value?.data)
                }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it != null) {
                        val list = ArrayList<IListItem>(orders.value?.data!!.dataList.size + 30)
                        list.addAll(orders.value?.data?.dataList ?: listOf())
                        list.removeLast()

                        if (page == 1 && it.dataList.isEmpty()) list.add(EmptyList())
                        list.addAll(it.dataList)
                        if (page < it.lastPage) list.add(Loading())
                        ordersLiveData.value = Resource.success(DataList(list, page, it.total))
                        page++
                    } else {
                        throw ServerErrorException()
                    }
                }, { t -> ordersLiveData.value = Resource.error(t, null) })
        )
    }

    fun deleteOrder(position: Int) {
        val ordersList = orders.value?.data?.dataList?.toMutableList() ?: mutableListOf()
        disposable.add(
            dataApi.deleteOrder((ordersList[position] as OrderVO).id)
                .subscribeOn(Schedulers.io())
                .doOnSuccess { if (!it.meta.success) throw ApiErrorException(it.meta.message) }
                .observeOn(AndroidSchedulers.mainThread())
                .map { it.meta.message }
                .subscribe({
                    ordersList.removeAt(position)
                    ordersLiveData.value = Resource.success(
                        DataList(
                            ordersList,
                            page,
                            orders.value?.data?.total?.minus(1) ?: 0
                        )
                    )
                }, { t -> messageLiveData.value = t.message })
        )
    }

    fun sendOrderToEmail(position: Int) {
        disposable.add(
            dataApi.sendOrderToEmail((orders.value?.data?.dataList?.get(position) as OrderVO).id)
                .subscribeOn(Schedulers.io())
                .doOnSuccess { if (!it.meta.success) throw ApiErrorException(it.meta.message) }
                .map { it.meta.message }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { messageLiveData.value = it },
                    { t -> messageLiveData.value = t.message }
                )
        )
    }

}