package ru.optap.ui.orders.data

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Order(
    @Json(name = "id") val id: Int,
    @Json(name = "user_id") val userId: Int? = null,
    @Json(name = "catalog_id") val catalogId: Int? = null,
    @Json(name = "created_at") val createdAt: String? = null,
    @Json(name = "updated_at") val updatedAt: String? = null,
    @Json(name = "status_id") val statusId: Int? = null,
    @Json(name = "vendor_id") val vendorId: Int? = null,
    @Json(name = "fullname") val fullname: String? = null,
    @Json(name = "phone") val phone: String? = null,
    @Json(name = "image") val image: String? = null,
    @Json(name = "status_name") val statusName: String? = null,
    @Json(name = "total_price") val totalPrice: Int? = null,
    @Json(name = "colored") val colored: Int? = null,
    @Json(name = "trade_center") val tradeCenter: String? = null,
    @Json(name = "pavilion_number") val pavilionNumber: String? = null
)