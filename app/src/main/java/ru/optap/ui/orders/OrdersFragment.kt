package ru.optap.ui.orders

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import ru.optap.R
import ru.optap.data.DataStatus
import ru.optap.databinding.FragmentOrdersBinding
import ru.optap.ui.base.view.BaseFragment
import ru.optap.ui.base.listview.*
import ru.optap.ui.base.listview.adapter.BasePagingAdapter
import ru.optap.ui.base.listview.adapter.EmptyListDelegateAdapter
import ru.optap.ui.base.listview.adapter.IListItem
import ru.optap.ui.base.listview.adapter.LoadingDelegateAdapter
import ru.optap.ui.orders.data.OrderVO
import ru.optap.ui.order.OrderFragment

@AndroidEntryPoint
class OrdersFragment : BaseFragment(), OrdersView {

    private var _binding: FragmentOrdersBinding? = null
    private val viewModel: OrdersViewModel by viewModels()
    private lateinit var adapter: BasePagingAdapter

    private val binding get() = _binding!!


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentOrdersBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initList()
        initObservers()

        binding.refreshLayout.setOnRefreshListener {
            binding.refreshLayout.isRefreshing = false
            viewModel.refresh()
        }

        if (savedInstanceState == null) viewModel.refresh()
    }

    private fun initList() {
        val ordersDelegateAdapter = OrdersDelegateAdapter { order, _ ->
            openOrderDetailScreen(order)
        }

        val adapterDelegates = mapOf(
            OrdersDelegateAdapter.ID to ordersDelegateAdapter,
            LoadingDelegateAdapter.ID to LoadingDelegateAdapter(),
            EmptyListDelegateAdapter.ID to EmptyListDelegateAdapter()
        )

        val diffUtil = object : DiffUtil.ItemCallback<IListItem>() {
            override fun areItemsTheSame(oldItem: IListItem, newItem: IListItem) =
                if (oldItem is OrderVO && newItem is OrderVO) {
                    oldItem.id == newItem.id
                } else {
                    false
                }

            override fun areContentsTheSame(oldItem: IListItem, newItem: IListItem) =
                if (oldItem is OrderVO && newItem is OrderVO) {
                    oldItem == newItem
                } else {
                    false
                }
        }

        val layoutManager = LinearLayoutManager(context)
        val scrollListener = object : PaginationScrollListener(layoutManager) {
            override fun loadMore() = viewModel.load()
        }

        val swipeController = SwipeController(object : SwipeControllerActions() {
            override fun onRightClicked(position: Int) = viewModel.deleteOrder(position)
            override fun onLeftClicked(position: Int) = viewModel.sendOrderToEmail(position)
        }, ItemTouchHelper.RIGHT or ItemTouchHelper.LEFT)
        val itemTouchHelper = ItemTouchHelper(swipeController)
        itemTouchHelper.attachToRecyclerView(binding.list)

        adapter = BasePagingAdapter(adapterDelegates, diffUtil)
        binding.list.apply {
            this.adapter = this@OrdersFragment.adapter
            this.layoutManager = layoutManager
            addOnScrollListener(scrollListener)
        }
    }

    private fun initObservers() {
        viewModel.orders.observe(viewLifecycleOwner) {
            when (it.status) {
                DataStatus.LOADING -> {}
                DataStatus.SUCCESS -> adapter.submitList(it.data?.dataList)
                DataStatus.ERROR -> handleError(it.throwable)
            }
        }
        viewModel.message.observe(viewLifecycleOwner) { showToast(it) }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun openOrderDetailScreen(order: OrderVO) {
        findNavController().navigate(R.id.nav_order, bundleOf(OrderFragment.ORDER to order))
    }

}