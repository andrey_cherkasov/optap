package ru.optap.ui.orders

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import ru.optap.R
import ru.optap.databinding.ItemOrderBinding
import ru.optap.ui.base.listview.adapter.BaseDelegateAdapter
import ru.optap.ui.base.listview.adapter.BaseViewHolder
import ru.optap.ui.base.listview.adapter.IListItem
import ru.optap.ui.orders.data.OrderVO

class OrdersDelegateAdapter(
    private val listener: (order: OrderVO, position: Int) -> Unit
) : BaseDelegateAdapter {

    companion object {
        const val ID = R.layout.item_order
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = OrderViewHolder(
        ItemOrderBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
    )

    override fun onBindViewHolder(holder: BaseViewHolder, item: IListItem) = holder.onBind(item)

    override fun isForViewType(item: IListItem) = item is OrderVO

    override fun id() = ID


    inner class OrderViewHolder(private val binding: ItemOrderBinding) :
        BaseViewHolder(binding) {
        override fun onBind(item: IListItem) {
            val order = item as OrderVO
            Glide.with(binding.image.context)
                .load(order.image)
                .transform(CircleCrop())
                .placeholder(R.drawable.preloader)
                .into(binding.image)
            binding.name.text = order.fullname
            binding.phone.text = order.phone
            binding.orderNumber.text = order.orderNumber
            binding.date.text = order.createdAt
            binding.totalPrice.text = order.totalPriceString

            binding.alertIcon.isVisible = order.colored > 0
            binding.alertIcon.setImageResource(
                when (order.colored) {
                    1 -> R.drawable.red_alert
                    else -> R.drawable.gray_alert
                }
            )
            binding.root.setOnClickListener { listener(order, absoluteAdapterPosition) }
        }
    }

}