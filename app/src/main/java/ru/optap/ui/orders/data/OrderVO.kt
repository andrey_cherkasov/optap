package ru.optap.ui.orders.data

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import ru.optap.ui.base.listview.adapter.IListItem

@Parcelize
data class OrderVO(
    val id: Int,
    val orderNumber: String,
    val createdAt: String,
    val fullname: String,
    val phone: String,
    val image: String,
    val totalPrice: Int,
    val totalPriceString: String,
    val colored: Int
) : IListItem, Parcelable