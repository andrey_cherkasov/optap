package ru.optap.ui.orders

import ru.optap.ui.orders.data.OrderVO

interface OrdersView {
    fun openOrderDetailScreen(order: OrderVO)
}