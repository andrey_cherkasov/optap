package ru.optap.ui.cart

import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import ru.optap.data.local.db.CatalogDao
import ru.optap.data.local.db.CategoryDao
import ru.optap.data.local.db.ProductDao
import ru.optap.data.remote.DataApiInterface
import ru.optap.ui.base.exception.ServerErrorException
import ru.optap.ui.base.listview.adapter.IListItem
import ru.optap.ui.cart.data.OrderJson
import ru.optap.ui.cart.data.OrderJsonMapper
import ru.optap.ui.catalog.data.CategoryVO
import ru.optap.ui.catalog.data.CategoryMapper
import ru.optap.ui.catalogs.data.CatalogMapper
import ru.optap.ui.catalogs.data.CatalogVO
import ru.optap.ui.category.data.ProductVO
import ru.optap.ui.category.data.ProductMapper
import javax.inject.Inject

class CartRepository @Inject constructor(
    private val moshi: Moshi,
    private val productDao: ProductDao,
    private val categoryDao: CategoryDao,
    private val catalogDao: CatalogDao,
    private val categoryMapper: CategoryMapper,
    private val productMapper: ProductMapper,
    private val catalogMapper: CatalogMapper,
    private val orderJsonMapper: OrderJsonMapper,
    private val dataApi: DataApiInterface
) {

    private val disposable = CompositeDisposable()

    fun addProduct(product: ProductVO, category: CategoryVO) {
        disposable.add(Completable.fromCallable {
            if (categoryDao.get(product.categoryId) == null) {
                categoryDao.insert(categoryMapper.VoToDto().map(category))
            }

            val simpleProduct = product.copy(
                typeId = 0,
                count = product.count * when (product.typeId) {
                    1 -> product.packCount
                    2 -> product.boxCount
                    else -> 1
                }
            )

            val productDto = productDao.get(product.id)
            if (productDto == null) {
                productDao.insert(productMapper.VoToDto().map(simpleProduct))
            } else {
                productDto.count += simpleProduct.count
                productDao.update(productDto)
            }

        }
            .subscribeOn(Schedulers.io())
            .subscribe({}, { t -> t.printStackTrace() })
        )
    }

    fun get(): Single<List<IListItem>> = Single.fromCallable {
        val list = mutableListOf<IListItem>()
        val categories = categoryDao.getAll().map { categoryMapper.DtoToVo().map(it) }
        val products = productDao.getAll().map { productMapper.DtoToVO().map(it) }

        val categoryIterator = categories.iterator()
        while (categoryIterator.hasNext()) {
            val category = categoryIterator.next()
            list.add(category)
            list.addAll(products.filter { product -> product.categoryId == category.id })
        }

        list
    }

    fun clear() = Single.fromCallable {
        productDao.clear()
        categoryDao.clear()
        catalogDao.clear()
    }

    fun editProduct(product: ProductVO) = productDao.update(productMapper.VoToDto().map(product))

    fun getTotal() = productDao.getTotal()

    fun setCatalog(catalog: CatalogVO) {
        disposable.add(Completable.fromCallable {
            if (categoryDao.getAll().isEmpty()) {
                catalogDao.clear()
                catalogDao.insert(catalogMapper.VoToDto().map(catalog))
            }
        }.subscribeOn(Schedulers.io()).subscribe())
    }

    fun getCatalog() = Single.fromCallable {
        if (categoryDao.getAll().isEmpty()) throw Exception()
        catalogMapper.DtoToVo().map(catalogDao.get())
    }

    fun deleteItem(item: IListItem) = Single.fromCallable {
        if (item is CategoryVO) {
            productDao.deleteByCategory(item.id)
            categoryDao.delete(item.id)
        } else if (item is ProductVO) {
            productDao.delete(item.id)
            if (productDao.getAll(item.categoryId).isEmpty()) {
                categoryDao.delete(item.categoryId)
            }
        }
    }

    fun makeOrder() = Single.fromCallable {
        val items = productDao.getAll().map { orderJsonMapper.map(it) }
        if (items.isEmpty()) throw Exception()
        val type = Types.newParameterizedType(List::class.java, OrderJson::class.java)
        val adapter: JsonAdapter<List<OrderJson>> = moshi.adapter(type)
        val orderJson = adapter.toJson(items)
        val result = dataApi.makeOrder(orderJson).execute()
        if (!result.isSuccessful) throw ServerErrorException()
        result.body()!!
    }

}