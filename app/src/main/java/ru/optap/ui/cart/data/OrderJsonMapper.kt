package ru.optap.ui.cart.data

import ru.optap.data.Mapper
import ru.optap.ui.category.data.ProductDTO

class OrderJsonMapper : Mapper<ProductDTO, OrderJson> {
    override fun map(input: ProductDTO): OrderJson {
        return OrderJson(input.id, input.count, input.typeId)
    }
}