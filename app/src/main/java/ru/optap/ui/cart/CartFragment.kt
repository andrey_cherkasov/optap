package ru.optap.ui.cart

import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import ru.optap.R
import ru.optap.data.DataStatus
import ru.optap.databinding.FragmentCartBinding
import ru.optap.ui.base.listview.SwipeController
import ru.optap.ui.base.listview.SwipeControllerActions
import ru.optap.ui.base.listview.adapter.*
import ru.optap.ui.base.view.BaseFragment
import ru.optap.ui.catalog.data.CategoryVO
import ru.optap.ui.category.data.ProductVO
import ru.optap.ui.order.adapter.CategoryTinyDelegateAdapter
import ru.optap.ui.order.adapter.ProductCollapsedDelegateAdapter
import ru.optap.ui.order.adapter.ProductHugeDelegateAdapter
import ru.optap.ui.order.adapter.ProductTinyDelegateAdapter
import ru.optap.ui.order.contract.OrderItemActivityResultContract
import ru.optap.ui.order.data.ViewMode

@AndroidEntryPoint
class CartFragment : BaseFragment(), CartView {

    private var _binding: FragmentCartBinding? = null
    private val viewModel: CartViewModel by viewModels()
    private lateinit var adapter: BasePagingAdapter
    private var viewModeMenu: MenuItem? = null
    private var clearCartMenu: MenuItem? = null
    private lateinit var swipeController: SwipeController

    private val binding get() = _binding!!

    private val launcher = registerForActivityResult(OrderItemActivityResultContract()) { product ->
        product?.let { it -> viewModel.edit(it) }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentCartBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initList()
        initObservers()

        binding.fab.setOnClickListener { showConfirmDialog() }
        if (savedInstanceState == null) viewModel.refresh()
    }

    private fun initList() {
        val categoryTinyDelegateAdapter = CategoryTinyDelegateAdapter { _, position ->
            viewModel.performCategory(position)
        }
        val productTinyDelegateAdapter = ProductTinyDelegateAdapter { product, _ ->
            openDetailScreen(product)
        }
        val productHugeDelegateAdapter = ProductHugeDelegateAdapter { product, _ ->
            openDetailScreen(product)
        }

        val adapterDelegates = mapOf(
            CategoryTinyDelegateAdapter.ID to categoryTinyDelegateAdapter,
            ProductTinyDelegateAdapter.ID to productTinyDelegateAdapter,
            ProductHugeDelegateAdapter.ID to productHugeDelegateAdapter,
            ProductCollapsedDelegateAdapter.ID to ProductCollapsedDelegateAdapter(),
            LoadingDelegateAdapter.ID to LoadingDelegateAdapter(),
            EmptyListDelegateAdapter.ID to EmptyListDelegateAdapter()
        )

        val diffUtil = object : DiffUtil.ItemCallback<IListItem>() {
            override fun areItemsTheSame(oldItem: IListItem, newItem: IListItem) =
                if (oldItem is CategoryVO && newItem is CategoryVO) {
                    oldItem.id == newItem.id
                } else if (oldItem is ProductVO && newItem is ProductVO) {
                    oldItem.id == newItem.id
                } else {
                    false
                }

            override fun areContentsTheSame(oldItem: IListItem, newItem: IListItem) =
                if (oldItem is CategoryVO && newItem is CategoryVO) {
                    oldItem == newItem
                } else if (oldItem is ProductVO && newItem is ProductVO) {
                    oldItem == newItem
                } else {
                    false
                }
        }

        swipeController = SwipeController(object : SwipeControllerActions() {
            override fun onRightClicked(position: Int) = viewModel.deleteItem(position)
        }, ItemTouchHelper.LEFT)
        val itemTouchHelper = ItemTouchHelper(swipeController)
        itemTouchHelper.attachToRecyclerView(binding.list)

        adapter = BasePagingAdapter(adapterDelegates, diffUtil)
        binding.list.apply {
            this.adapter = this@CartFragment.adapter
            this.layoutManager = LinearLayoutManager(context)
        }
    }

    private fun initObservers() {
        viewModel.cart.observe(viewLifecycleOwner) {
            when (it.status) {
                DataStatus.LOADING -> {}
                DataStatus.SUCCESS -> {
                    if (it.data?.get(0) !is EmptyList) {
                        clearCartMenu?.isVisible = true
                        viewModeMenu?.isVisible = true
                        swipeController.isSwipeEnabled = true
                        binding.fab.translationX = 0f
                    } else {
                        clearCartMenu?.isVisible = false
                        viewModeMenu?.isVisible = false
                        swipeController.isSwipeEnabled = false
                        binding.fab.translationX = 500f
                    }

                    adapter.submitList(it.data)
                }
                DataStatus.ERROR -> handleError(it.throwable)
            }
        }
        viewModel.catalog.observe(viewLifecycleOwner) {
            (activity as AppCompatActivity).supportActionBar?.title =
                it?.name ?: getString(R.string.menu_cart)
        }
        viewModel.total.observe(viewLifecycleOwner) {
            (activity as AppCompatActivity).supportActionBar?.subtitle =
                if (it != null) getString(R.string.total_price_subtitle, it.toString()) else ""
        }
        viewModel.viewMode.observe(viewLifecycleOwner) { setViewMode(it) }
        viewModel.message.observe(viewLifecycleOwner) { showToast(it) }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.fragment_order, menu)
        viewModeMenu = menu.findItem(R.id.view_mode)
        clearCartMenu = menu.findItem(R.id.clear)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.view_mode -> viewModel.changeViewMode()
            R.id.clear -> viewModel.clear()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        (activity as AppCompatActivity).supportActionBar?.subtitle = ""
        _binding = null
    }

    override fun setViewMode(viewMode: Int) {
        if (viewMode == ViewMode.TINY) {
            viewModeMenu?.setIcon(R.drawable.ic_view_grid)
        } else if (viewMode == ViewMode.HUGE) {
            viewModeMenu?.setIcon(R.drawable.ic_format_list_bulleted_type)
        }
    }

    override fun openDetailScreen(product: ProductVO) = launcher.launch(product)


    override fun showConfirmDialog() {
        AlertDialog.Builder(requireContext())
            .setTitle(R.string.make_order_q)
            .setPositiveButton(R.string.ok) { _, _ -> viewModel.makeOrder() }
            .setNegativeButton(R.string.cancel) { dialog, _ -> dialog.dismiss() }
            .show()
    }

}