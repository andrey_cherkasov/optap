package ru.optap.ui.cart.data

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class OrderJson(
    @Json(name = "productId") val productId: Int,
    @Json(name = "count") val count: Int,
    @Json(name = "type") val type: Int
)