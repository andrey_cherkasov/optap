package ru.optap.ui.cart

import ru.optap.ui.category.data.ProductVO

interface CartView {
    fun setViewMode(viewMode: Int)
    fun openDetailScreen(product: ProductVO)
    fun showConfirmDialog()
}