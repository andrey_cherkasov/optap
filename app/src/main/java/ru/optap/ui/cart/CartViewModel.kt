package ru.optap.ui.cart

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ru.optap.data.Resource
import ru.optap.ui.base.SingleLiveEvent
import ru.optap.ui.base.listview.adapter.EmptyList
import ru.optap.ui.base.listview.adapter.IListItem
import ru.optap.ui.base.viewmodel.BaseListViewModel
import ru.optap.ui.catalog.data.CategoryVO
import ru.optap.ui.catalogs.data.CatalogVO
import ru.optap.ui.category.data.ProductVO
import ru.optap.ui.order.data.ViewMode
import javax.inject.Inject

@HiltViewModel
class CartViewModel @Inject constructor(
    private val cartRepository: CartRepository
) : BaseListViewModel() {

    private val cartLiveData: MutableLiveData<Resource<List<IListItem>>> = MutableLiveData()
    private val totalLiveData: MutableLiveData<Int?> = MutableLiveData()
    private val catalogLiveData: MutableLiveData<CatalogVO?> = MutableLiveData()
    private val viewModeLiveData: MutableLiveData<Int> = MutableLiveData(ViewMode.TINY)
    private val messageLiveData: SingleLiveEvent<String> = SingleLiveEvent()

    val cart: LiveData<Resource<List<IListItem>>> get() = cartLiveData
    val total: LiveData<Int?> = totalLiveData
    val catalog: LiveData<CatalogVO?> = catalogLiveData
    val viewMode: LiveData<Int> get() = viewModeLiveData
    val message: LiveData<String> get() = messageLiveData

    fun refresh() {
        disposable.add(
            cartRepository.getTotal()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ totalLiveData.value = it }, { t ->
                    totalLiveData.value = null
                    t.printStackTrace()
                })
        )

        disposable.add(
            cartRepository.getCatalog().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ catalog ->
                    catalogLiveData.value = catalog
                }, { t ->
                    catalogLiveData.value = null
                    t.printStackTrace()
                })
        )

        disposable.add(
            cartRepository.get()
                .subscribeOn(Schedulers.io())
                .map { it.ifEmpty { listOf<IListItem>(EmptyList()) } }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { cartLiveData.value = Resource.success(it) },
                    { t -> cartLiveData.value = Resource.error(t, null) }
                )
        )
    }

    fun changeViewMode() {
        viewModeLiveData.value =
            if (viewMode.value == ViewMode.TINY) ViewMode.HUGE else ViewMode.TINY

        disposable.add(Single.fromCallable {
            val list = cart.value?.data?.toMutableList() ?: mutableListOf()
            val iterator = list.listIterator()
            while (iterator.hasNext()) {
                val item = iterator.next()
                if (item is ProductVO) iterator.set(item.copy(viewMode = viewMode.value!!))
            }
            list
        }
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { cartLiveData.value = Resource.success(it) },
                { t -> t.printStackTrace() }
            )
        )
    }

    fun deleteItem(position: Int) {
        disposable.add(
            cartRepository.deleteItem(cart.value!!.data!![position])
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ refresh() }, { t -> t.printStackTrace() })
        )
    }

    fun clear() {
        disposable.add(
            cartRepository.clear()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    cartLiveData.value = Resource.success(listOf(EmptyList()))
                    catalogLiveData.value = null
                    totalLiveData.value = null
                }, { t -> t.printStackTrace() })
        )
    }

    fun edit(product: ProductVO) {
        disposable.add(
            cartRepository.editProduct(product)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ refresh() }, { t -> t.printStackTrace() })
        )
    }

    fun performCategory(position: Int) {
        disposable.add(Single.fromCallable {
            val list = cart.value?.data?.toMutableList() ?: mutableListOf()
            val category = list[position] as CategoryVO
            list[position] = category.copy(expand = !category.expand)
            for (i in (position + 1) until list.size) {
                val product = list[i] as? ProductVO ?: break
                list[i] = product.copy(expand = !product.expand)
            }
            list
        }
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { cartLiveData.value = Resource.success(it) },
                { t -> t.printStackTrace() }
            )
        )
    }

    fun makeOrder() {
        disposable.add(
            cartRepository.makeOrder()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    messageLiveData.value = it.meta.message
                    if (it.meta.success) clear()
                }, { t -> t.printStackTrace() })
        )
    }

}