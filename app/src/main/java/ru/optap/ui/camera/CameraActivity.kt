package ru.optap.ui.camera

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.Matrix
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.activity.viewModels
import androidx.camera.core.*
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import dagger.hilt.android.AndroidEntryPoint
import ru.optap.R
import ru.optap.data.DataStatus
import ru.optap.databinding.ActivityCameraBinding
import ru.optap.ui.base.view.BaseActivity
import ru.optap.ui.catalog.data.CategoryVO
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors


@AndroidEntryPoint
class CameraActivity : BaseActivity(), CameraView {

    companion object {
        const val CATEGORY = "CATEGORY"
        const val ANIM_TIME: Long = 150
    }

    private lateinit var binding: ActivityCameraBinding
    private lateinit var cameraExecutor: ExecutorService
    private val viewModel: CameraViewModel by viewModels()
    private var imageCapture: ImageCapture? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCameraBinding.inflate(LayoutInflater.from(this))
        setContentView(binding.root)
        setSupportActionBar(binding.appBarMain.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = getString(R.string.new_product_camera)

        val category = intent.getParcelableExtra<CategoryVO>(CATEGORY)
        if (category != null) {
            viewModel.setCategory(category)
        } else {
            finish()
        }

        startCamera()
        cameraExecutor = Executors.newSingleThreadExecutor()

        binding.imageCaptureButton.setOnClickListener { takePhoto() }
        binding.okButton.setOnClickListener { viewModel.createProduct(getBitmapFromView(binding.image)) }
        binding.cancelButton.setOnClickListener { showCameraControls() }

        viewModel.message.observe(this) { showToast(it) }
        viewModel.product.observe(this) {
            binding.loadingLayout.root.isVisible = false
            when (it.status) {
                DataStatus.LOADING -> binding.loadingLayout.root.isVisible = true
                DataStatus.SUCCESS -> {
                    val intent = Intent().putExtra(CATEGORY, true)
                    setResult(Activity.RESULT_OK, intent)
                    showCameraControls()
                }
                DataStatus.ERROR -> handleError(it.throwable)
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onDestroy() {
        super.onDestroy()
        cameraExecutor.shutdown()
    }

    private fun takePhoto() {
        val imageCapture = imageCapture ?: return
        imageCapture.takePicture(ContextCompat.getMainExecutor(this),
            object : ImageCapture.OnImageCapturedCallback() {
                override fun onCaptureSuccess(image: ImageProxy) {
                    binding.image.setImageBitmap(image.toBitmap())
                    image.close()
                    showImageControls()
                }

                override fun onError(exception: ImageCaptureException) {}
            }
        )
    }

    private fun startCamera() {
        val cameraProviderFuture = ProcessCameraProvider.getInstance(this)
        cameraProviderFuture.addListener({
            val cameraProvider: ProcessCameraProvider = cameraProviderFuture.get()
            val preview = Preview.Builder().build()
            preview.setSurfaceProvider(binding.viewFinder.surfaceProvider)
            imageCapture = ImageCapture.Builder().build()
            val cameraSelector = CameraSelector.DEFAULT_BACK_CAMERA

            try {
                cameraProvider.unbindAll()
                cameraProvider.bindToLifecycle(this, cameraSelector, preview, imageCapture)
            } catch (exc: Exception) {
                exc.printStackTrace()
            }
        }, ContextCompat.getMainExecutor(this))
    }

    fun ImageProxy.toBitmap(): Bitmap {
        val buffer = planes[0].buffer
        buffer.rewind()
        val bytes = ByteArray(buffer.capacity())
        buffer.get(bytes)
        val bmp = BitmapFactory.decodeByteArray(bytes, 0, bytes.size)
        val rotate = Matrix()
        rotate.postRotate(90f)
        return Bitmap.createBitmap(bmp, 0, 0, bmp.width, bmp.height, rotate, true)
    }

    private fun getBitmapFromView(view: View): Bitmap {
        val bitmap = Bitmap.createBitmap(view.width, view.height, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        view.draw(canvas)
        return bitmap
    }

    override fun showCameraControls() {
        binding.image.setImageBitmap(null)
        val delta = (binding.imageCaptureButton.height * 2).toFloat()
        binding.imageCaptureButton.animate().setDuration(ANIM_TIME).translationY(0f)
        binding.cancelButton.animate().setDuration(ANIM_TIME).translationY(delta)
        binding.okButton.animate().setDuration(ANIM_TIME).translationY(delta)
    }

    override fun showImageControls() {
        val delta = (binding.imageCaptureButton.height * 2).toFloat()
        binding.imageCaptureButton.animate().setDuration(ANIM_TIME).translationY(delta)
        binding.cancelButton.animate().setDuration(ANIM_TIME).translationY(0f)
        binding.okButton.animate().setDuration(ANIM_TIME).translationY(0f)
    }

}