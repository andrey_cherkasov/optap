package ru.optap.ui.camera.contract

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.activity.result.contract.ActivityResultContract
import ru.optap.ui.camera.CameraActivity
import ru.optap.ui.catalog.data.CategoryVO

class CameraActivityResultContract : ActivityResultContract<CategoryVO, Boolean?>() {
    override fun createIntent(context: Context, input: CategoryVO) =
        Intent(context, CameraActivity::class.java).putExtra(CameraActivity.CATEGORY, input)

    override fun parseResult(resultCode: Int, intent: Intent?): Boolean? = when {
        resultCode != Activity.RESULT_OK -> null
        else -> intent?.getBooleanExtra(CameraActivity.CATEGORY, false)
    }
}
