package ru.optap.ui.camera

import android.app.Application
import android.graphics.Bitmap
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.MultipartBody
import ru.optap.R
import ru.optap.data.Resource
import ru.optap.data.remote.DataApiInterface
import ru.optap.data.remote.ProgressEmittingRequestBody
import ru.optap.ui.base.SingleLiveEvent
import ru.optap.ui.base.exception.ApiErrorException
import ru.optap.ui.base.viewmodel.BaseViewModel
import ru.optap.ui.catalog.data.CategoryVO
import ru.optap.ui.category.data.Product
import java.io.File
import java.io.FileOutputStream
import javax.inject.Inject

@HiltViewModel
class CameraViewModel @Inject constructor(
    private val app: Application,
    private val dataApi: DataApiInterface
) : BaseViewModel() {

    private val productLiveData: MutableLiveData<Resource<Product>> = MutableLiveData()
    private val categoryLiveData: MutableLiveData<CategoryVO> = MutableLiveData()
    private val messageLiveData: SingleLiveEvent<String> = SingleLiveEvent()

    val category: LiveData<CategoryVO> = categoryLiveData
    val product: LiveData<Resource<Product>> = productLiveData
    val message: LiveData<String> = messageLiveData

    fun setCategory(category: CategoryVO) {
        categoryLiveData.value = category
    }

    fun createProduct(bitmap: Bitmap) {
        val imageFile = File(app.cacheDir.path.plus("/image.jpg"))
        val outStream = FileOutputStream(imageFile)
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outStream)

        val requestBody = ProgressEmittingRequestBody("", imageFile)
        val picture = MultipartBody.Part.createFormData("picture", imageFile.name, requestBody)

        disposable.add(
            dataApi.createProductRx(category.value!!.id, picture)
                .subscribeOn(Schedulers.io())
                .doOnSuccess { if (!it.meta.success) throw ApiErrorException(it.meta.message) }
                .doOnSubscribe { productLiveData.value = Resource.loading(null) }
                .map { it.data }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    productLiveData.value = Resource.success(it)
                    messageLiveData.value = app.getString(R.string.product_created)
                }, { t -> productLiveData.value = Resource.error(t, null) })
        )
    }

}