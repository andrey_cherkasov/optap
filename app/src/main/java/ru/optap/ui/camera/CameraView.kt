package ru.optap.ui.camera

interface CameraView {
    fun showCameraControls()
    fun showImageControls()
}