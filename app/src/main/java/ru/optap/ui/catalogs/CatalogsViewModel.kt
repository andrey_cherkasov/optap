package ru.optap.ui.catalogs

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ru.optap.data.DataStatus
import ru.optap.data.Resource
import ru.optap.data.model.DataList
import ru.optap.data.remote.DataApiInterface
import ru.optap.ui.base.exception.ApiErrorException
import ru.optap.ui.base.exception.ServerErrorException
import ru.optap.ui.base.listview.adapter.EmptyList
import ru.optap.ui.base.listview.adapter.IListItem
import ru.optap.ui.base.listview.adapter.Loading
import ru.optap.ui.base.viewmodel.BaseListViewModel
import ru.optap.ui.cart.CartRepository
import ru.optap.ui.catalogs.data.CatalogMapper
import ru.optap.ui.catalogs.data.CatalogVO
import javax.inject.Inject

@HiltViewModel
class CatalogsViewModel @Inject constructor(
    private val dataApi: DataApiInterface,
    private val catalogMapper: CatalogMapper,
    private val cartRepository: CartRepository
) : BaseListViewModel() {

    private val catalogsLiveData: MutableLiveData<Resource<DataList<IListItem>>> =
        MutableLiveData()

    val catalogs: LiveData<Resource<DataList<IListItem>>> get() = catalogsLiveData


    fun refresh() {
        page = 1
        catalogsLiveData.value = Resource.success(DataList(listOf(Loading()), page, 0))
        load()
    }

    fun load() {
        if (catalogs.value?.status == DataStatus.LOADING) return
        if (page > catalogs.value?.data?.lastPage ?: 1) return

        disposable.add(
            dataApi.getFavoriteCatalogs(page)
                .subscribeOn(Schedulers.io())
                .doOnSuccess { response ->
                    if (!response.meta.success) throw ApiErrorException(response.meta.message)
                }
                .map { it.data }
                .map {
                    DataList<IListItem>(
                        it.dataList.map { catalog -> catalogMapper.ToVo().map(catalog) },
                        page,
                        it.total
                    )
                }
                .doOnSubscribe {
                    catalogsLiveData.value = Resource.loading(catalogs.value?.data)
                }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it != null) {
                        val list = ArrayList<IListItem>(catalogs.value?.data!!.dataList.size + 30)
                        list.addAll(catalogs.value?.data?.dataList ?: listOf())
                        list.removeLast()

                        if (page == 1 && it.dataList.isEmpty()) list.add(EmptyList())
                        list.addAll(it.dataList)
                        if (page < it.lastPage) list.add(Loading())
                        catalogsLiveData.value = Resource.success(DataList(list, page, it.total))
                        page++
                    } else {
                        throw ServerErrorException()
                    }
                }, { t -> catalogsLiveData.value = Resource.error(t, null) })
        )
    }

    fun deleteCatalog(position: Int) {
        val catalogsList = catalogs.value?.data?.dataList?.toMutableList()
        disposable.add(
            dataApi.deleteFavoriteCatalog((catalogsList?.get(position) as CatalogVO).id)
                .subscribeOn(Schedulers.io())
                .doOnSuccess { if (!it.meta.success) throw ApiErrorException(it.meta.message) }
                .observeOn(AndroidSchedulers.mainThread())
                .map { it.meta.message }
                .subscribe({
                    catalogsList.removeAt(position)
                    catalogsLiveData.value = Resource.success(
                        DataList(
                            catalogsList,
                            page,
                            catalogs.value?.data?.total?.minus(1) ?: 0
                        )
                    )
                }, { })
        )
    }

    fun setCatalog(catalog: CatalogVO) = cartRepository.setCatalog(catalog)

}