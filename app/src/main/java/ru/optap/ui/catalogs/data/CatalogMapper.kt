package ru.optap.ui.catalogs.data

import ru.optap.data.Mapper

class CatalogMapper {

    inner class ToVo : Mapper<Catalog, CatalogVO> {
        override fun map(input: Catalog): CatalogVO {
            return CatalogVO(
                input.id,
                input.closed ?: 0,
                input.name ?: "",
                input.image ?: "",
                input.isUpdated ?: 0
            )
        }
    }

    inner class VoToDto : Mapper<CatalogVO, CatalogDTO> {
        override fun map(input: CatalogVO): CatalogDTO {
            return CatalogDTO(
                input.id,
                input.closed,
                input.name,
                input.image,
                input.isUpdated
            )
        }
    }

    inner class DtoToVo : Mapper<CatalogDTO, CatalogVO> {
        override fun map(input: CatalogDTO): CatalogVO {
            return CatalogVO(
                input.id,
                input.closed,
                input.name,
                input.image,
                input.isUpdated
            )
        }
    }

}