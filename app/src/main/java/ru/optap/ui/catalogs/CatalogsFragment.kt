package ru.optap.ui.catalogs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import ru.optap.R
import ru.optap.data.DataStatus
import ru.optap.databinding.FragmentCatalogsBinding
import ru.optap.ui.base.listview.PaginationScrollListener
import ru.optap.ui.base.listview.SwipeController
import ru.optap.ui.base.listview.SwipeControllerActions
import ru.optap.ui.base.listview.adapter.BasePagingAdapter
import ru.optap.ui.base.listview.adapter.EmptyListDelegateAdapter
import ru.optap.ui.base.listview.adapter.IListItem
import ru.optap.ui.base.listview.adapter.LoadingDelegateAdapter
import ru.optap.ui.base.view.BaseFragment
import ru.optap.ui.catalog.CatalogFragment
import ru.optap.ui.catalogs.data.CatalogVO

@AndroidEntryPoint
class CatalogsFragment : BaseFragment(), CatalogsView {

    private var _binding: FragmentCatalogsBinding? = null
    private val viewModel: CatalogsViewModel by viewModels()
    private lateinit var adapter: BasePagingAdapter

    private val binding get() = _binding!!


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentCatalogsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initList()
        initObservers()

        if (savedInstanceState == null) viewModel.refresh()
    }

    private fun initList() {
        val catalogsDelegateAdapter = CatalogsDelegateAdapter { catalog ->
            openCatalogScreen(catalog)
        }

        val adapterDelegates = mapOf(
            CatalogsDelegateAdapter.ID to catalogsDelegateAdapter,
            LoadingDelegateAdapter.ID to LoadingDelegateAdapter(),
            EmptyListDelegateAdapter.ID to EmptyListDelegateAdapter()
        )

        val diffUtil = object : DiffUtil.ItemCallback<IListItem>() {
            override fun areItemsTheSame(oldItem: IListItem, newItem: IListItem) =
                if (oldItem is CatalogVO && newItem is CatalogVO) {
                    oldItem.id == newItem.id
                } else {
                    false
                }

            override fun areContentsTheSame(oldItem: IListItem, newItem: IListItem) =
                if (oldItem is CatalogVO && newItem is CatalogVO) {
                    oldItem == newItem
                } else {
                    false
                }
        }

        val layoutManager = LinearLayoutManager(context)
        val scrollListener = object : PaginationScrollListener(layoutManager) {
            override fun loadMore() = viewModel.load()
        }

        val swipeController = SwipeController(object : SwipeControllerActions() {
            override fun onRightClicked(position: Int) = viewModel.deleteCatalog(position)
        }, ItemTouchHelper.LEFT)
        val itemTouchHelper = ItemTouchHelper(swipeController)
        itemTouchHelper.attachToRecyclerView(binding.list)

        adapter = BasePagingAdapter(adapterDelegates, diffUtil)
        binding.list.apply {
            this.adapter = this@CatalogsFragment.adapter
            this.layoutManager = layoutManager
            addOnScrollListener(scrollListener)
        }
    }

    private fun initObservers() {
        viewModel.catalogs.observe(viewLifecycleOwner) {
            when (it.status) {
                DataStatus.LOADING -> {}
                DataStatus.SUCCESS -> adapter.submitList(it.data?.dataList)
                DataStatus.ERROR -> handleError(it.throwable)
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun openCatalogScreen(catalog: CatalogVO) {
        viewModel.setCatalog(catalog)
        findNavController().navigate(
            R.id.nav_my_catalog,
            bundleOf(CatalogFragment.CATALOG to catalog)
        )
    }

}