package ru.optap.ui.catalogs

import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import ru.optap.R
import ru.optap.databinding.ItemCatalogsBinding
import ru.optap.ui.base.listview.adapter.BaseDelegateAdapter
import ru.optap.ui.base.listview.adapter.BaseViewHolder
import ru.optap.ui.base.listview.adapter.IListItem
import ru.optap.ui.catalogs.data.CatalogVO

class CatalogsDelegateAdapter constructor(
    private val listener: (catalog: CatalogVO) -> Unit
) : BaseDelegateAdapter {

    companion object {
        const val ID = R.layout.item_catalogs
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = CatalogsViewHolder(
        ItemCatalogsBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
    )

    override fun onBindViewHolder(holder: BaseViewHolder, item: IListItem) = holder.onBind(item)

    override fun isForViewType(item: IListItem) = item is CatalogVO

    override fun id() = ID


    inner class CatalogsViewHolder(private val binding: ItemCatalogsBinding) :
        BaseViewHolder(binding) {
        override fun onBind(item: IListItem) {
            val catalog = item as CatalogVO
            Glide.with(binding.image.context)
                .load(catalog.image)
                .transform(CircleCrop())
                .placeholder(R.drawable.preloader)
                .into(binding.image)
            binding.title.text = catalog.name
            binding.root.setOnClickListener { listener(catalog) }
        }
    }
}