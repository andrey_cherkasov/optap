package ru.optap.ui.catalogs.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Catalog")
data class CatalogDTO(
    @PrimaryKey val id: Int,
    @ColumnInfo(name = "closed") val closed: Int,
    @ColumnInfo(name = "name") val name: String,
    @ColumnInfo(name = "image") val image: String,
    @ColumnInfo(name = "is_updated") val isUpdated: Int,
)