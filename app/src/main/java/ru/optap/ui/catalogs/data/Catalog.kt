package ru.optap.ui.catalogs.data

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Catalog(
    @Json(name = "id") val id: Int,
    @Json(name = "closed") val closed: Int?,
    @Json(name = "name") val name: String?,
    @Json(name = "image") val image: String?,
    @Json(name = "is_updated") val isUpdated: Int?,
)