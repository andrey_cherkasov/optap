package ru.optap.ui.catalogs

import ru.optap.ui.catalogs.data.CatalogVO

interface CatalogsView {
    fun openCatalogScreen(catalog: CatalogVO)
}