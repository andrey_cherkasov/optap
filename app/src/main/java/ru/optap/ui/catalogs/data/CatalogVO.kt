package ru.optap.ui.catalogs.data

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import ru.optap.ui.base.listview.adapter.IListItem

@Parcelize
data class CatalogVO(
    val id: Int,
    val closed: Int,
    val name: String,
    val image: String,
    val isUpdated: Int
) : IListItem, Parcelable