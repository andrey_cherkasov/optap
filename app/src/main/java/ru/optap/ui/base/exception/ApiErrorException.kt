package ru.optap.ui.base.exception

import java.lang.Exception

class ApiErrorException(message: String?) : Exception(message)