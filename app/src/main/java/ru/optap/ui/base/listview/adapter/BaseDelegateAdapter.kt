package ru.optap.ui.base.listview.adapter

import android.view.ViewGroup

interface BaseDelegateAdapter {

    fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder

    fun onBindViewHolder(holder: BaseViewHolder, item: IListItem)

    fun isForViewType(item: IListItem): Boolean

    fun id(): Int

}