package ru.optap.ui.base.listview.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import ru.optap.R
import ru.optap.databinding.ItemLoadingBinding

class LoadingDelegateAdapter : BaseDelegateAdapter {

    companion object {
        const val ID = R.layout.item_loading
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = LoadingViewHolder(
        ItemLoadingBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
    )

    override fun onBindViewHolder(holder: BaseViewHolder, item: IListItem) {
        holder.onBind(item)
    }

    override fun isForViewType(item: IListItem): Boolean = item is Loading

    override fun id(): Int = ID


    inner class LoadingViewHolder(binding: ItemLoadingBinding) : BaseViewHolder(binding) {
        override fun onBind(item: IListItem) {}
    }

}