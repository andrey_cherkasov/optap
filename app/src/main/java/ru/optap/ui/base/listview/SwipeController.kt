package ru.optap.ui.base.listview

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.annotation.SuppressLint
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.RectF
import android.graphics.drawable.Drawable
import android.view.MotionEvent
import android.view.MotionEvent.ACTION_DOWN
import android.view.View
import android.view.animation.DecelerateInterpolator
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.toBitmap
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.ItemTouchHelper.ACTION_STATE_SWIPE
import androidx.recyclerview.widget.RecyclerView
import ru.optap.R
import kotlin.math.max
import kotlin.math.min


class SwipeController(
    private val buttonsActions: SwipeControllerActions,
    private val flags: Int
) : ItemTouchHelper.Callback() {

    internal enum class ButtonsState {
        GONE, LEFT_VISIBLE, RIGHT_VISIBLE
    }

    private var swipeBack = false
    private var buttonShowedState = ButtonsState.GONE
    private var buttonInstance: RectF? = null
    private var currentItemViewHolder: RecyclerView.ViewHolder? = null
    private var buttonWidth = 0f

    var isSwipeEnabled: Boolean = true


    override fun getMovementFlags(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder) =
        makeMovementFlags(0, if (isSwipeEnabled) flags else 0)

    override fun onMove(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        target: RecyclerView.ViewHolder
    ) = false

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {}

    override fun convertToAbsoluteDirection(flags: Int, layoutDirection: Int): Int {
        if (swipeBack) {
            swipeBack = buttonShowedState !== ButtonsState.GONE
            return 0
        }
        return super.convertToAbsoluteDirection(flags, layoutDirection)
    }

    override fun onChildDraw(
        c: Canvas,
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        dX: Float,
        dY: Float,
        actionState: Int,
        isCurrentlyActive: Boolean
    ) {
        buttonWidth = (viewHolder.itemView.width / 5).toFloat()
        if (actionState == ACTION_STATE_SWIPE) {
            var dX1 = 0f
            if (buttonShowedState != ButtonsState.GONE) {
                if (buttonShowedState == ButtonsState.LEFT_VISIBLE) dX1 = buttonWidth
                if (buttonShowedState == ButtonsState.RIGHT_VISIBLE) dX1 = -buttonWidth

                super.onChildDraw(
                    c,
                    recyclerView,
                    viewHolder,
                    dX1,
                    dY,
                    actionState,
                    isCurrentlyActive
                )
            } else {
                setTouchListener(recyclerView, viewHolder, dX / 3)
            }
        }

        if (buttonShowedState == ButtonsState.GONE) {
            val dX1 = if (dX > 0) min(dX / 3, buttonWidth) else max(dX / 3, -buttonWidth)
            super.onChildDraw(
                c,
                recyclerView,
                viewHolder,
                dX1,
                dY,
                actionState,
                isCurrentlyActive
            )
        }

        drawButtons(c, viewHolder)
        currentItemViewHolder = viewHolder
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun setTouchListener(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        dX: Float
    ) {
        recyclerView.setOnTouchListener { _, event ->
            swipeBack =
                event.action == MotionEvent.ACTION_CANCEL || event.action == MotionEvent.ACTION_UP

            if (swipeBack) {
                if (dX < -buttonWidth)
                    buttonShowedState = ButtonsState.RIGHT_VISIBLE
                else if (dX > buttonWidth)
                    buttonShowedState = ButtonsState.LEFT_VISIBLE

                if (buttonShowedState !== ButtonsState.GONE) {
                    setTouchDownListener(recyclerView, viewHolder)
                    setItemsClickable(recyclerView, false)
                }
            }
            false
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun setTouchDownListener(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder
    ) {
        recyclerView.setOnTouchListener { _, event ->
            if (event.action == ACTION_DOWN) setTouchUpListener(recyclerView, viewHolder)
            false
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun setTouchUpListener(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder
    ) {
        recyclerView.setOnTouchListener { _, event ->
            if (event.action == MotionEvent.ACTION_UP) {
                val animationX: ObjectAnimator = ObjectAnimator.ofFloat(
                    viewHolder.itemView,
                    View.TRANSLATION_X,
                    1f
                )

                AnimatorSet().apply {
                    duration = 100
                    interpolator = DecelerateInterpolator()
                    play(animationX)
                    start()
                }

                recyclerView.setOnTouchListener { _, _ -> false }
                setItemsClickable(recyclerView, true)
                swipeBack = false
                if (buttonInstance != null && buttonInstance!!.contains(event.x, event.y)) {
                    if (buttonShowedState == ButtonsState.LEFT_VISIBLE) {
                        buttonsActions.onLeftClicked(viewHolder.absoluteAdapterPosition)
                    } else if (buttonShowedState == ButtonsState.RIGHT_VISIBLE) {
                        buttonsActions.onRightClicked(viewHolder.absoluteAdapterPosition)
                    }
                }
                buttonShowedState = ButtonsState.GONE
                currentItemViewHolder = null
            }
            false
        }
    }

    private fun setItemsClickable(recyclerView: RecyclerView, isClickable: Boolean) {
        for (i in 0 until recyclerView.childCount) {
            recyclerView.getChildAt(i).isClickable = isClickable
        }
    }

    private fun drawButtons(c: Canvas, viewHolder: RecyclerView.ViewHolder) {
        val itemView = viewHolder.itemView
        val context = itemView.context
        val p = Paint()

        val leftButton = RectF(
            itemView.left.toFloat(),
            itemView.top.toFloat(), itemView.left + buttonWidth,
            itemView.bottom.toFloat()
        )
        p.color = ContextCompat.getColor(context, R.color.sendBackground)
        c.drawRect(leftButton, p)
        ContextCompat.getDrawable(context, R.drawable.ic_email_send_outline)?.let {
            drawIcon(it, c, leftButton, p)
        }

        val rightButton = RectF(
            itemView.right - buttonWidth,
            itemView.top.toFloat(), itemView.right.toFloat(), itemView.bottom.toFloat()
        )
        p.color = ContextCompat.getColor(context, R.color.deleteBackground)
        c.drawRect(rightButton, p)
        ContextCompat.getDrawable(context, R.drawable.ic_delete_outline)?.let {
            drawIcon(it, c, rightButton, p)
        }

        buttonInstance = null
        if (buttonShowedState === ButtonsState.LEFT_VISIBLE) {
            buttonInstance = leftButton
        } else if (buttonShowedState === ButtonsState.RIGHT_VISIBLE) {
            buttonInstance = rightButton
        }
    }

    private fun drawIcon(d: Drawable, c: Canvas, button: RectF, p: Paint) {
        val a = (buttonWidth * 0.35).toInt()
        val bitmap = d.toBitmap(a, a)
        c.drawBitmap(bitmap, button.centerX() - a / 2, button.centerY() - a / 2, p)
    }

}