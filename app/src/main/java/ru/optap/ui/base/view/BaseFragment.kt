package ru.optap.ui.base.view

import android.widget.Toast
import androidx.fragment.app.Fragment
import ru.optap.R
import ru.optap.ui.base.exception.ApiErrorException
import ru.optap.ui.base.exception.NoConnectivityException
import ru.optap.ui.base.exception.ServerErrorException

open class BaseFragment : Fragment(), BaseView {

    override fun showToast(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    override fun handleError(error: Throwable?) {
        when (error) {
            is ApiErrorException -> error.message?.let { showToast(it) }
            is NoConnectivityException -> error.message?.let { showToast(it) }
            is ServerErrorException -> error.message?.let { showToast(it) }
            else -> {
                showToast(getString(R.string.error))
                error?.printStackTrace()
            }
        }
    }

}