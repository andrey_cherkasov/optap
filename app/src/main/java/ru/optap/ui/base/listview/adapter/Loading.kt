package ru.optap.ui.base.listview.adapter

class Loading : IListItem {
    override fun equals(other: Any?): Boolean = this === other
    override fun hashCode(): Int = javaClass.hashCode()
}