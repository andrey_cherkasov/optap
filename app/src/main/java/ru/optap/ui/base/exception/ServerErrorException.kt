package ru.optap.ui.base.exception

import java.lang.Exception

class ServerErrorException : Exception("Server error")