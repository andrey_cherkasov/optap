package ru.optap.ui.base.listview

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

abstract class PaginationScrollListener(private val layoutManager: LinearLayoutManager) :
    RecyclerView.OnScrollListener() {

    companion object {
        const val ITEMS_PER_PAGE = 20
    }

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)
        val firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition()
        if ((layoutManager.childCount + firstVisibleItemPosition) >= layoutManager.itemCount
            && firstVisibleItemPosition >= 0
        ) loadMore()
    }

    abstract fun loadMore()

}