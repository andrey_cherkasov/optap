package ru.optap.ui.base.view

interface BaseView {

    fun showToast(message: String)

    fun handleError(error: Throwable?)

}