package ru.optap.ui.base.listview.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import ru.optap.R
import ru.optap.databinding.ItemEmptyListBinding

class EmptyListDelegateAdapter : BaseDelegateAdapter {

    companion object {
        const val ID = R.layout.item_empty_list
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = EmptyListViewHolder(
        ItemEmptyListBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
    )

    override fun onBindViewHolder(holder: BaseViewHolder, item: IListItem) = holder.onBind(item)

    override fun isForViewType(item: IListItem): Boolean = item is EmptyList

    override fun id(): Int = ID


    inner class EmptyListViewHolder(binding: ItemEmptyListBinding) : BaseViewHolder(binding) {
        override fun onBind(item: IListItem) {}
    }

}