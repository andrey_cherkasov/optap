package ru.optap.ui.base.exception

import java.lang.Exception

class EmptyListException : Exception("list is empty")