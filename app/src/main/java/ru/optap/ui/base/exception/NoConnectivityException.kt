package ru.optap.ui.base.exception

class NoConnectivityException(message: String?) : Exception(message)