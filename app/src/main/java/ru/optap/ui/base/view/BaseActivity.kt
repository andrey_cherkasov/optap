package ru.optap.ui.base.view

import android.content.Context
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import ru.optap.R
import ru.optap.ui.base.exception.ApiErrorException
import ru.optap.ui.base.exception.NoConnectivityException
import ru.optap.ui.base.exception.ServerErrorException
import ru.optap.util.RuntimeLocaleChanger

open class BaseActivity : AppCompatActivity(), BaseView {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.statusBarColor = ContextCompat.getColor(this, R.color.primaryColor)
    }

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(RuntimeLocaleChanger().wrapContext(newBase))
    }

    override fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun handleError(error: Throwable?) {
        when (error) {
            is ApiErrorException -> error.message?.let { showToast(it) }
            is NoConnectivityException -> error.message?.let { showToast(it) }
            is ServerErrorException -> error.message?.let { showToast(it) }
            else -> {
                showToast(getString(R.string.error))
                error?.printStackTrace()
            }
        }
    }

}