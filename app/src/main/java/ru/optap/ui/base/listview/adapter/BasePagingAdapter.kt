package ru.optap.ui.base.listview.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter

class BasePagingAdapter(
    private val delegateAdapters: Map<Int, BaseDelegateAdapter>,
    diffCallback: DiffUtil.ItemCallback<IListItem>
) : ListAdapter<IListItem, BaseViewHolder>(diffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        return delegateAdapters.getValue(viewType).onCreateViewHolder(parent, viewType)
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.onBind(currentList[position])
    }

    override fun getItemViewType(position: Int): Int {
        for (delegateAdapter in delegateAdapters.values) {
            if (delegateAdapter.isForViewType(currentList[position]))
                return delegateAdapter.id()
        }

        throw NullPointerException("Can not get viewType for position $position")
    }

    override fun getItemCount(): Int = currentList.size

}