package ru.optap.ui.base.view

interface LoadingView<T> {

    fun showLoading(loading: Boolean)

    fun setData(data: T)

}