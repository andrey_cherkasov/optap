package ru.optap.ui.base.listview.adapter

fun interface OnItemListViewClickListener<T> {
    fun onClick(item: T, position: Int)
}