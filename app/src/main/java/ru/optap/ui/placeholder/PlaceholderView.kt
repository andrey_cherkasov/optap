package ru.optap.ui.placeholder

import ru.optap.data.model.User

interface PlaceholderView {
    fun openMainScreen(user: User)
    fun openLoginScreen()
}