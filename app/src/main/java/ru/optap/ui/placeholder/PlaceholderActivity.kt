package ru.optap.ui.placeholder

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import androidx.activity.viewModels
import dagger.hilt.android.AndroidEntryPoint
import ru.optap.data.DataStatus
import ru.optap.data.model.User
import ru.optap.databinding.ActivityPlaceholderBinding
import ru.optap.ui.base.view.BaseActivity
import ru.optap.ui.login.LoginActivity
import ru.optap.ui.main.MainActivity

@AndroidEntryPoint
class PlaceholderActivity : BaseActivity(), PlaceholderView {

    private lateinit var binding: ActivityPlaceholderBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val viewModel: PlaceholderViewModel by viewModels()
        binding = ActivityPlaceholderBinding.inflate(LayoutInflater.from(this))
        setContentView(binding.root)

        viewModel.user.observe(this) { t ->
            when (t.status) {
                DataStatus.SUCCESS -> t.data?.let { openMainScreen(it) }
                else -> openLoginScreen()
            }
        }
    }

    override fun openMainScreen(user: User) {
        startActivity(
            Intent(this, MainActivity::class.java).putExtra(MainActivity.USER, user)
        )
    }

    override fun openLoginScreen() = startActivity(Intent(this, LoginActivity::class.java))

}