package ru.optap.ui.placeholder

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ru.optap.data.Resource
import ru.optap.data.local.SharedPreferencesHelper
import ru.optap.data.model.User
import ru.optap.data.remote.AuthApiInterface
import ru.optap.ui.base.viewmodel.BaseViewModel
import javax.inject.Inject

@HiltViewModel
class PlaceholderViewModel @Inject constructor(
    private val preferencesHelper: SharedPreferencesHelper,
    private val authApi: AuthApiInterface
) : BaseViewModel() {

    private val userLiveData: MutableLiveData<Resource<User>> = MutableLiveData()

    val user: LiveData<Resource<User>> get() = userLiveData

    init {
        loadUser()
    }

    private fun loadUser() {
        val userSingle: Single<User> = Single.create {
            val localUser = preferencesHelper.loadUser()
            if (localUser != null) {
                val remoteUser = authApi.getUser().execute().body()?.data
                if (remoteUser != null) {
                    preferencesHelper.saveUser(remoteUser)
                    it.onSuccess(remoteUser)
                } else {
                    it.onSuccess(localUser)
                }
            } else {
                it.onError(Throwable("not authorized"))
            }
        }

        disposable.add(
            userSingle.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    userLiveData.value = Resource.success(it)
                }, {
                    userLiveData.value = Resource.error(it, null)
                })
        )
    }

}