package ru.optap.ui.catalog.instance

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.core.view.isVisible
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.nguyenhoanglam.imagepicker.ui.imagepicker.registerImagePicker
import dagger.hilt.android.AndroidEntryPoint
import ru.optap.R
import ru.optap.data.DataStatus
import ru.optap.databinding.ActivityCatalogInstanceBinding
import ru.optap.ui.base.view.BaseActivity
import ru.optap.ui.base.view.LoadingView
import ru.optap.ui.catalog.data.CategoryVO
import ru.optap.util.Utils
import javax.inject.Inject

@AndroidEntryPoint
class CategoryInstanceActivity : BaseActivity(), LoadingView<CategoryVO> {

    companion object {
        const val CATEGORY = "CATEGORY"
        const val PARENT_ID = "PARENT_ID"
    }

    @Inject
    lateinit var utils: Utils

    private lateinit var binding: ActivityCatalogInstanceBinding
    private val viewModel: CategoryInstanceViewModel by viewModels()
    private var category: CategoryVO? = null
    private var parentId: Int = 0

    private val launcher = registerImagePicker { images ->
        if (images.isNotEmpty()) viewModel.setImage(images[0])
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCatalogInstanceBinding.inflate(LayoutInflater.from(this))
        setContentView(binding.root)
        setSupportActionBar(binding.appBarMain.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        parentId = intent.getIntExtra(PARENT_ID, 0)
        category = intent.getParcelableExtra(CATEGORY)

        Glide.with(this)
            .load(category?.image ?: getString(R.string.default_category_image_url))
            .transform(CircleCrop())
            .into(binding.image)
        category?.let {
            binding.nameEditText.setText(it.name)
            binding.descriptionEditText.setText(it.description)
            binding.packCountEditText.setText(it.packCount.toString())
            binding.boxCountEditText.setText(it.boxCount.toString())
            binding.defaultPriceEditText.setText(it.price.toString())
        }

        supportActionBar?.title =
            getString(if (category != null) R.string.edit_category_title else R.string.new_category_title)

        binding.editImageButton.setOnClickListener { launcher.launch(utils.getSingleImagePickerConfig()) }

        viewModel.image.observe(this) {
            Glide.with(this).load(it.uri).transform(CircleCrop()).into(binding.image)
        }
        viewModel.category.observe(this) {
            showLoading(false)
            when (it.status) {
                DataStatus.LOADING -> showLoading(true)
                DataStatus.SUCCESS -> it.data?.let { it1 -> setData(it1) }
                DataStatus.ERROR -> handleError(it.throwable)
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.activity_catalog_instance, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.ok) {
            viewModel.updateCategory(
                category?.id ?: 0,
                category?.catalogId ?: parentId,
                binding.nameEditText.text.toString(),
                binding.descriptionEditText.text.toString(),
                binding.defaultPriceEditText.text.toString().toIntOrNull() ?: 0,
                binding.packCountEditText.text.toString().toIntOrNull() ?: 0,
                binding.boxCountEditText.text.toString().toIntOrNull() ?: 0,
            )
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun showLoading(loading: Boolean) {
        binding.loadingLayout.root.isVisible = loading
    }

    override fun setData(data: CategoryVO) {
        val result = Intent().putExtra(CATEGORY, data)
        setResult(Activity.RESULT_OK, result)
        finish()
    }

}