package ru.optap.ui.catalog

import ru.optap.ui.catalog.data.CategoryVO

interface CatalogView {
    fun openCatalogNewInstanceScreen()
    fun openCategoryScreen(category: CategoryVO)
}