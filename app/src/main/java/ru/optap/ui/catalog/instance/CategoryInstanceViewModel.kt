package ru.optap.ui.catalog.instance

import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.nguyenhoanglam.imagepicker.model.Image
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.MultipartBody
import ru.optap.data.Resource
import ru.optap.data.remote.DataApiInterface
import ru.optap.data.remote.ProgressEmittingRequestBody
import ru.optap.ui.base.exception.ApiErrorException
import ru.optap.ui.base.viewmodel.BaseViewModel
import ru.optap.ui.catalog.data.CategoryMapper
import ru.optap.ui.catalog.data.CategoryVO
import ru.optap.util.Utils
import java.io.File
import javax.inject.Inject

@HiltViewModel
class CategoryInstanceViewModel @Inject constructor(
    private val dataApi: DataApiInterface,
    private val categoryMapper: CategoryMapper,
    private val utils: Utils
) : BaseViewModel() {

    private val imageLiveData: MutableLiveData<Image> = MutableLiveData()
    private val categoryLiveData: MutableLiveData<Resource<CategoryVO>> = MutableLiveData()

    val image: LiveData<Image> get() = imageLiveData
    val category: LiveData<Resource<CategoryVO>> get() = categoryLiveData

    fun setImage(image: Image) {
        imageLiveData.value = image
    }

    fun updateCategory(
        id: Int,
        parentId: Int,
        name: String,
        description: String,
        price: Int,
        packCount: Int,
        boxCount: Int
    ) {
        val imagePath = utils.getRealPathFromUri(image.value?.uri ?: Uri.EMPTY)
        val imageFile = File(imagePath)
        val requestBody = ProgressEmittingRequestBody("", imageFile)
        val picture = MultipartBody.Part.createFormData("picture", imageFile.name, requestBody)

        disposable.add(
            dataApi.updateCategory(
                id,
                parentId,
                name,
                description,
                price,
                packCount,
                boxCount,
                false,
                picture
            )
                .subscribeOn(Schedulers.io())
                .doOnSuccess { if (!it.meta.success) throw ApiErrorException(it.meta.message) }
                .map { it.data }
                .map { categoryMapper.ToVo().map(it) }
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { categoryLiveData.value = Resource.loading(null) }
                .subscribe(
                    { categoryLiveData.value = Resource.success(it) },
                    { categoryLiveData.value = Resource.error(it, null) }
                )
        )
    }

}