package ru.optap.ui.catalog.data

import ru.optap.data.Mapper

class CategoryMapper {

    inner class ToVo : Mapper<Category, CategoryVO> {
        override fun map(input: Category): CategoryVO {
            return CategoryVO(
                input.id,
                input.catalogId ?: 0,
                input.name ?: "",
                input.description ?: "",
                input.images ?: "",
                input.price ?: 0,
                input.packCount ?: 0,
                input.boxCount ?: 0,
                input.image ?: "",
                input.isUpdated ?: 0,
                isDeleted = false,
                expand = true
            )
        }
    }

    inner class VoToDto : Mapper<CategoryVO, CategoryDTO> {
        override fun map(input: CategoryVO): CategoryDTO {
            return CategoryDTO(
                input.id,
                input.catalogId,
                input.name,
                input.description,
                input.images,
                input.price,
                input.packCount,
                input.boxCount,
                input.image,
                input.isUpdated
            )
        }
    }

    inner class DtoToVo : Mapper<CategoryDTO, CategoryVO> {
        override fun map(input: CategoryDTO): CategoryVO {
            return CategoryVO(
                input.id,
                input.catalogId ?: 0,
                input.name ?: "",
                input.description ?: "",
                input.images ?: "",
                input.price ?: 0,
                input.packCount ?: 0,
                input.boxCount ?: 0,
                input.image ?: "",
                input.isUpdated ?: 0,
                isDeleted = false,
                expand = true
            )
        }
    }

}