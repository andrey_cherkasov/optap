package ru.optap.ui.catalog

import android.os.Bundle
import android.view.*
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.GridLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import ru.optap.R
import ru.optap.data.DataStatus
import ru.optap.databinding.FragmentCatalogBinding
import ru.optap.ui.base.listview.*
import ru.optap.ui.base.listview.adapter.BasePagingAdapter
import ru.optap.ui.base.listview.adapter.EmptyListDelegateAdapter
import ru.optap.ui.base.listview.adapter.IListItem
import ru.optap.ui.base.listview.adapter.LoadingDelegateAdapter
import ru.optap.ui.base.view.BaseFragment
import ru.optap.ui.catalog.data.CategoryVO
import ru.optap.ui.catalog.instance.contract.CategoryCreateResultContract
import ru.optap.ui.catalogs.data.CatalogVO
import ru.optap.ui.category.contract.CategoryActivityResultContract
import ru.optap.ui.main.MainActivity

@AndroidEntryPoint
class CatalogFragment : BaseFragment(), CatalogView {

    companion object {
        const val CATALOG = "CATALOG"
    }

    private var _binding: FragmentCatalogBinding? = null
    private val viewModel: CatalogViewModel by viewModels()

    private lateinit var adapter: BasePagingAdapter
    private var showDeletedMenu: MenuItem? = null
    private var subscribeMenu: MenuItem? = null

    private val binding get() = _binding!!

    private val deleteRestoreResultLauncher =
        registerForActivityResult(CategoryActivityResultContract()) { result ->
            result?.let { viewModel.deleteRestoreCategory(it) }
        }

    private val createResultLauncher =
        registerForActivityResult(CategoryCreateResultContract()) { result ->
            result?.let { viewModel.createCategory(it) }
        }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentCatalogBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val catalog = arguments?.getParcelable<CatalogVO>(CATALOG)
        catalog?.let {
            viewModel.setCatalog(it)
        }
        (activity as MainActivity).supportActionBar?.title =
            catalog?.name ?: getString(R.string.menu_my_catalog)


        initList()
        initObservers()

        if (viewModel.isEditable) {
            binding.fab.setImageResource(R.drawable.ic_plus)
            binding.fab.setOnClickListener { openCatalogNewInstanceScreen() }
        } else {
            binding.fab.translationX = 500f
            binding.fab.setImageResource(R.drawable.ic_heart_outline)
            binding.fab.setOnClickListener { viewModel.performFavorite() }
        }

        if (savedInstanceState == null) viewModel.refresh()
    }

    private fun initList() {
        val catalogDelegateAdapter = CatalogDelegateAdapter()
        catalogDelegateAdapter.setOnItemListViewClickListener { item, _ -> openCategoryScreen(item) }

        val adapterDelegates = mapOf(
            CatalogDelegateAdapter.ID to catalogDelegateAdapter,
            LoadingDelegateAdapter.ID to LoadingDelegateAdapter(),
            EmptyListDelegateAdapter.ID to EmptyListDelegateAdapter()
        )

        val diffUtil = object : DiffUtil.ItemCallback<IListItem>() {
            override fun areItemsTheSame(oldItem: IListItem, newItem: IListItem) =
                if (oldItem is CategoryVO && newItem is CategoryVO) {
                    oldItem.id == newItem.id
                } else {
                    false
                }

            override fun areContentsTheSame(oldItem: IListItem, newItem: IListItem) =
                if (oldItem is CategoryVO && newItem is CategoryVO) {
                    oldItem == newItem
                } else {
                    false
                }
        }

        val layoutManager = GridLayoutManager(context, 2)
        layoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                return when (adapter.getItemViewType(position)) {
                    CatalogDelegateAdapter.ID -> 1
                    else -> 2
                }
            }
        }

        val scrollListener = object : PaginationScrollListener(layoutManager) {
            override fun loadMore() = viewModel.load()
        }

        adapter = BasePagingAdapter(adapterDelegates, diffUtil)
        binding.list.apply {
            this.adapter = this@CatalogFragment.adapter
            this.layoutManager = layoutManager
            addOnScrollListener(scrollListener)
        }
    }

    private fun initObservers() {
        viewModel.catalog.observe(viewLifecycleOwner) {
            when (it.status) {
                DataStatus.LOADING -> {}
                DataStatus.SUCCESS -> adapter.submitList(it.data?.dataList)
                DataStatus.ERROR -> handleError(it.throwable)
            }
        }

        viewModel.deletedView.observe(viewLifecycleOwner) {
            binding.fab.isVisible = !it
            showDeletedMenu?.title =
                getString(if (it) R.string.show_current_items else R.string.show_deleted_items)
        }
        viewModel.favorite.observe(viewLifecycleOwner) {
            binding.fab.setImageResource(if (it) R.drawable.ic_heart else R.drawable.ic_heart_outline)
            binding.fab.translationX = 0f
        }

        viewModel.subscription.observe(viewLifecycleOwner) {
            subscribeMenu?.setIcon(if (it) R.drawable.ic_bell_remove_outline else R.drawable.ic_bell_ring_outline)
            subscribeMenu?.setTitle(if (it) R.string.unsubscribe else R.string.subscribe)
            subscribeMenu?.isVisible = true
        }
        viewModel.message.observe(viewLifecycleOwner) { showToast(it) }
    }

    @Override
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.fragment_catalog, menu)
        showDeletedMenu = menu.findItem(R.id.show_deleted)
        subscribeMenu = menu.findItem(R.id.subscribe)
        showDeletedMenu?.isVisible = viewModel.isEditable
        subscribeMenu?.isVisible = false
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.show_deleted -> viewModel.changeDeletedView()
            R.id.subscribe -> viewModel.performSubscription()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun openCatalogNewInstanceScreen() = createResultLauncher.launch(null)

    override fun openCategoryScreen(category: CategoryVO) =
        deleteRestoreResultLauncher.launch(category)

}