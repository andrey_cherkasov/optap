package ru.optap.ui.catalog

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ru.optap.data.DataStatus
import ru.optap.data.Resource
import ru.optap.data.model.DataList
import ru.optap.data.remote.DataApiInterface
import ru.optap.ui.base.SingleLiveEvent
import ru.optap.ui.base.exception.ApiErrorException
import ru.optap.ui.base.exception.ServerErrorException
import ru.optap.ui.base.listview.adapter.EmptyList
import ru.optap.ui.base.listview.adapter.IListItem
import ru.optap.ui.base.listview.adapter.Loading
import ru.optap.ui.base.viewmodel.BaseListViewModel
import ru.optap.ui.catalog.data.CategoryMapper
import ru.optap.ui.catalog.data.CategoryVO
import ru.optap.ui.catalogs.data.CatalogVO
import javax.inject.Inject

@HiltViewModel
class CatalogViewModel @Inject constructor(
    private val dataApi: DataApiInterface,
    private val categoryMapper: CategoryMapper
) : BaseListViewModel() {

    private var catalogId = 0
    private val catalogLiveData: MutableLiveData<Resource<DataList<IListItem>>> = MutableLiveData()
    private val deletedViewLiveData: MutableLiveData<Boolean> = MutableLiveData(false)
    private val favoriteLiveData: MutableLiveData<Boolean> = MutableLiveData()
    private val subscriptionLiveData: MutableLiveData<Boolean> = MutableLiveData()
    private val messageLiveData: SingleLiveEvent<String> = SingleLiveEvent()

    val catalog: LiveData<Resource<DataList<IListItem>>> get() = catalogLiveData
    val deletedView: LiveData<Boolean> get() = deletedViewLiveData
    val favorite: LiveData<Boolean> get() = favoriteLiveData
    val subscription: LiveData<Boolean> get() = subscriptionLiveData
    val message: LiveData<String> get() = messageLiveData
    val isEditable: Boolean get() = catalogId == 0

    fun setCatalog(catalog: CatalogVO) {
        catalogId = catalog.id
        if (!isEditable) {
            checkFavorite()
            checkSubscription()
        }
    }

    fun refresh() {
        page = 1
        catalogLiveData.value = Resource.success(DataList(listOf(Loading()), page, 0))
    }

    fun load() {
        if (catalog.value?.status == DataStatus.LOADING) return
        if (page > catalog.value?.data?.lastPage ?: 1) return

        val catalogSingle = if (isEditable) {
            dataApi.getVendorCatalog(page, if (deletedView.value == true) 1 else 0)
        } else {
            dataApi.getCatalog(catalogId, page)
        }

        disposable.add(catalogSingle
            .subscribeOn(Schedulers.io())
            .doOnSuccess { response ->
                if (!response.meta.success) throw ApiErrorException(response.meta.message)
            }
            .map { it.data }
            .map {
                DataList<IListItem>(
                    it.dataList.map { category -> categoryMapper.ToVo().map(category) }
                        .map { categoryVO -> categoryVO.copy(isDeleted = deletedView.value!!) },
                    page,
                    it.total
                )
            }
            .doOnSubscribe {
                catalogLiveData.value = Resource.loading(catalog.value?.data)
            }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it != null) {
                    val list = ArrayList<IListItem>(catalog.value?.data!!.dataList.size + 30)
                    list.addAll(catalog.value?.data?.dataList ?: listOf())
                    list.removeLast()

                    if (page == 1 && it.dataList.isEmpty()) list.add(EmptyList())
                    list.addAll(it.dataList)
                    if (page < it.lastPage) list.add(Loading())
                    catalogLiveData.value = Resource.success(DataList(list, page, it.total))
                    page++
                } else {
                    throw ServerErrorException()
                }
            }, { t -> catalogLiveData.value = Resource.error(t, null) })
        )
    }

    fun changeDeletedView() {
        deletedViewLiveData.value = !deletedView.value!!
        refresh()
    }

    fun deleteRestoreCategory(category: CategoryVO) {
        disposable.add(Single.fromCallable {
            val list = catalog.value?.data?.dataList?.toMutableList() ?: mutableListOf()
            val total = catalog.value?.data?.total ?: 0

            val iterator = list.listIterator()
            while (iterator.hasNext()) {
                val item = iterator.next()
                if (item is CategoryVO && item.id == category.id) {
                    if (category.isDeleted != deletedView.value) {
                        iterator.remove()
                    } else {
                        category.isUpdated = 0
                        iterator.set(category)
                    }
                    break
                }
            }

            DataList(list, page, total)
        }
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { catalogLiveData.value = Resource.success(it) },
                { t -> t.printStackTrace() }
            )
        )
    }

    fun createCategory(category: CategoryVO) {
        val list = catalog.value?.data!!.dataList.toMutableList()
        if (list[0] is EmptyList) list.removeAt(0)
        list.add(0, category)
        catalogLiveData.value = Resource.success(catalog.value!!.data!!.copy(list))
    }

    fun performSubscription() {
        disposable.add(
            dataApi.performSubscription(catalogId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    messageLiveData.value = it.meta.message
                    subscriptionLiveData.value = it.data?.subscribed == 1
                }, { t -> t.printStackTrace() })
        )
    }

    fun performFavorite() {
        val favoriteSingle = if (favorite.value!!) {
            dataApi.deleteFavorites(catalogId)
        } else {
            dataApi.addToFavorites(catalogId)
        }

        disposable.add(
            favoriteSingle
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    favoriteLiveData.value = !favorite.value!!
                }, { t -> t.printStackTrace() })
        )
    }

    private fun checkFavorite() {
        disposable.add(
            dataApi.checkFavorite(catalogId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { favoriteLiveData.value = it.meta.success },
                    { t -> t.printStackTrace() }
                )
        )
    }

    private fun checkSubscription() {
        disposable.add(
            dataApi.checkSubscription(catalogId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { subscriptionLiveData.value = it.data?.subscribed == 1 },
                    { t -> t.printStackTrace() }
                )
        )
    }

}