package ru.optap.ui.catalog.instance.contract

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.activity.result.contract.ActivityResultContract
import ru.optap.ui.catalog.data.CategoryVO
import ru.optap.ui.catalog.instance.CategoryInstanceActivity

class CategoryUpdateResultContract : ActivityResultContract<CategoryVO?, CategoryVO?>() {

    override fun createIntent(context: Context, input: CategoryVO?): Intent =
        Intent(
            context,
            CategoryInstanceActivity::class.java
        ).putExtra(CategoryInstanceActivity.CATEGORY, input)

    override fun parseResult(resultCode: Int, intent: Intent?): CategoryVO? = when {
        resultCode != Activity.RESULT_OK -> null
        else -> intent?.getParcelableExtra(CategoryInstanceActivity.CATEGORY)
    }

}