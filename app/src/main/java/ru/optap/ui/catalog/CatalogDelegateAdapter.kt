package ru.optap.ui.catalog

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import com.bumptech.glide.Glide
import ru.optap.R
import ru.optap.databinding.ItemCategoryBinding
import ru.optap.ui.base.listview.adapter.BaseDelegateAdapter
import ru.optap.ui.base.listview.adapter.BaseViewHolder
import ru.optap.ui.base.listview.adapter.IListItem
import ru.optap.ui.base.listview.adapter.OnItemListViewClickListener
import ru.optap.ui.catalog.data.CategoryVO

class CatalogDelegateAdapter : BaseDelegateAdapter {

    companion object {
        const val ID = R.layout.item_category
    }

    private var listener: OnItemListViewClickListener<CategoryVO>? = null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = CategoryViewHolder(
        ItemCategoryBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
    )

    override fun onBindViewHolder(holder: BaseViewHolder, item: IListItem) = holder.onBind(item)

    override fun isForViewType(item: IListItem) = item is CategoryVO

    override fun id() = ID

    fun setOnItemListViewClickListener(listener: OnItemListViewClickListener<CategoryVO>) {
        this.listener = listener
    }


    inner class CategoryViewHolder(private val binding: ItemCategoryBinding) :
        BaseViewHolder(binding) {

        override fun onBind(item: IListItem) {
            val category = item as CategoryVO
            if (category.id == 0) {
                binding.root.isVisible = false
                return
            }
            binding.root.isVisible = true
            binding.title.text =
                if (category.name == "") binding.root.context.getString(R.string.untitled) else category.name
            binding.badgeNew.isVisible = category.isUpdated != 0
            Glide.with(binding.image.context)
                .load(category.image)
                .placeholder(R.drawable.preloader)
                .into(binding.image)

            binding.image.setOnClickListener {
                listener?.onClick(category, absoluteAdapterPosition)
            }
        }

    }
}

