package ru.optap.ui.catalog.data

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Category(
    @Json(name = "id") val id: Int,
    @Json(name = "catalog_id") val catalogId: Int?,
    @Json(name = "name") val name: String?,
    @Json(name = "description") val description: String?,
    @Json(name = "images") val images: String?,
    @Json(name = "price") val price: Int?,
    @Json(name = "pack_count") val packCount: Int?,
    @Json(name = "box_count") val boxCount: Int?,
    @Json(name = "image") val image: String?,
    @Json(name = "is_updated") val isUpdated: Int?
)