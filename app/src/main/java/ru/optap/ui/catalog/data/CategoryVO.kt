package ru.optap.ui.catalog.data

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

import ru.optap.ui.base.listview.adapter.IListItem

@Parcelize
data class CategoryVO(
    val id: Int,
    val catalogId: Int,
    val name: String,
    val description: String,
    val images: String,
    val price: Int,
    val packCount: Int,
    val boxCount: Int,
    val image: String,
    var isUpdated: Int,
    val isDeleted: Boolean,
    val expand: Boolean
) : IListItem, Parcelable {
    companion object {
        fun createEmpty(): CategoryVO = CategoryVO(
            0,
            0,
            "",
            "",
            "",
            0,
            0,
            0,
            "",
            0,
            isDeleted = false,
            expand = true
        )
    }
}
