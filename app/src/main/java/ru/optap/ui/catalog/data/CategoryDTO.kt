package ru.optap.ui.catalog.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Categories")
data class CategoryDTO(
    @PrimaryKey val id: Int,
    @ColumnInfo(name = "catalog_id") val catalogId: Int?,
    @ColumnInfo(name = "name") val name: String?,
    @ColumnInfo(name = "description") val description: String?,
    @ColumnInfo(name = "images") val images: String?,
    @ColumnInfo(name = "price") val price: Int?,
    @ColumnInfo(name = "pack_count") val packCount: Int?,
    @ColumnInfo(name = "box_count") val boxCount: Int?,
    @ColumnInfo(name = "image") val image: String?,
    @ColumnInfo(name = "is_updated") val isUpdated: Int?
)