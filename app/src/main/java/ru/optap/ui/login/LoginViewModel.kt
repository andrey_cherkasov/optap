package ru.optap.ui.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ru.optap.data.Resource
import ru.optap.data.local.SharedPreferencesHelper
import ru.optap.data.model.User
import ru.optap.data.remote.AuthApiInterface
import ru.optap.ui.base.viewmodel.BaseViewModel
import ru.optap.ui.base.exception.ApiErrorException
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(
    private val authApiInterface: AuthApiInterface,
    private val preferencesHelper: SharedPreferencesHelper
) : BaseViewModel() {

    private val userLiveData: MutableLiveData<Resource<User>> = MutableLiveData()
    private val validationLiveData: MutableLiveData<LoginValidationState> = MutableLiveData(
        LoginValidationState(phone = true, password = true)
    )

    fun getUser(): LiveData<Resource<User>> = userLiveData
    fun getValidation(): LiveData<LoginValidationState> = validationLiveData


    fun login(countryId: String, phone: String, password: String) {
        val login = phone.trim().replace("[^0-9]".toRegex(), "")
        validationLiveData.value = LoginValidationState(
            phone = login.length == 10,
            password = password.isNotEmpty()
        )

        if (validationLiveData.value!!.isInvalidData()) return

        disposable.add(
            authApiInterface.login(
                countryId,
                login,
                password,
                preferencesHelper.loadFCMToken()
            )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { userLiveData.value = Resource.loading(null) }
                .doOnSuccess { if (!it.meta.success) throw ApiErrorException(it.meta.message) }
                .map { it.data }
                .subscribe(
                    { user ->
                        user?.let { preferencesHelper.saveUser(it) }
                        userLiveData.value = Resource.success(user)
                    }, { t -> userLiveData.value = Resource.error(t, null) }
                )
        )
    }

}