package ru.optap.ui.login

import ru.optap.ui.dialog.country.data.Country
import ru.optap.data.model.User

interface LoginView {
    //    fun showInviteCodeDialog(phone: String)
    fun showValidationErrors(validation: LoginValidationState)
    fun openLangDialog()
    fun openCountryDialog()
    fun changeCountry(county: Country)
    fun openMainScreen(user: User)
    fun openRegisterScreen()
    fun openRestoreScreen()
    fun openDialer()
}