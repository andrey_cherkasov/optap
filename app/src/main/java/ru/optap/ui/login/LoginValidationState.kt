package ru.optap.ui.login

data class LoginValidationState(
    var phone: Boolean,
    var password: Boolean
) {
    fun isInvalidData(): Boolean = !(phone && password)
}