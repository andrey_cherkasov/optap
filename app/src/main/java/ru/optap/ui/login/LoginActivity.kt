package ru.optap.ui.login

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.method.PasswordTransformationMethod
import android.view.LayoutInflater
import android.view.View
import androidx.activity.viewModels
import androidx.core.view.isVisible
import dagger.hilt.android.AndroidEntryPoint
import ru.optap.BuildConfig
import ru.optap.R
import ru.optap.data.DataStatus
import ru.optap.ui.dialog.country.data.Country
import ru.optap.data.model.User
import ru.optap.databinding.ActivityLoginBinding
import ru.optap.ui.base.view.BaseActivity
import ru.optap.ui.base.view.LoadingView
import ru.optap.ui.dialog.country.CountryDialog
import ru.optap.ui.dialog.country.CountryDialogViewModel
import ru.optap.ui.dialog.locale.LocaleDialog
import ru.optap.ui.dialog.locale.LocaleDialogViewModel
import ru.optap.ui.main.MainActivity
import ru.optap.ui.register.RegisterActivity
import ru.optap.ui.restore.RestoreActivity
import ru.tinkoff.decoro.MaskImpl
import ru.tinkoff.decoro.parser.UnderscoreDigitSlotsParser
import ru.tinkoff.decoro.slots.Slot
import ru.tinkoff.decoro.watchers.MaskFormatWatcher
import java.util.*


@AndroidEntryPoint
class LoginActivity : BaseActivity(), LoginView, LoadingView<User> {

    private lateinit var binding: ActivityLoginBinding
    private val viewModel: LoginViewModel by viewModels()
    private val localeViewModel: LocaleDialogViewModel by viewModels()
    private val countryViewModel: CountryDialogViewModel by viewModels()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(LayoutInflater.from(this))
        setContentView(binding.root)

        val slots: Array<Slot> = UnderscoreDigitSlotsParser().parseSlots("(___) ___ - __ - __")
        val mask: MaskImpl = MaskImpl.createTerminated(slots)
        mask.isHideHardcodedHead = true
        MaskFormatWatcher(mask).installOn(binding.phoneEditText)

        binding.passwordEditText.transformationMethod = PasswordTransformationMethod()

        binding.signUpButton.setOnClickListener { openRegisterScreen() }
        binding.restoreButton.setOnClickListener { openRestoreScreen() }
        binding.supportTelTextView.setOnClickListener { openDialer() }
        binding.langImageButton.setOnClickListener { openLangDialog() }
        binding.countryLogo.setOnClickListener { openCountryDialog() }
        binding.loginButton.setOnClickListener {
            viewModel.login(
                countryViewModel.getSelectCountry().value!!.code,
                binding.phoneEditText.text.toString(),
                binding.passwordEditText.text.toString()
            )
        }

        viewModel.getValidation().observe(this) { showValidationErrors(it) }
        viewModel.getUser().observe(this) {
            showLoading(false)
            when (it.status) {
                DataStatus.LOADING -> showLoading(true)
                DataStatus.SUCCESS -> it.data?.let { it1 -> setData(it1) }
                DataStatus.ERROR -> handleError(it.throwable)
            }
        }

        localeViewModel.getLocales().observe(this) {
            binding.langImageButton.visibility = View.VISIBLE
        }
        countryViewModel.getCountries().observe(this) {
            binding.countryLogo.visibility = View.VISIBLE
        }

        countryViewModel.getSelectCountry().observe(this) { changeCountry(it) }
    }

    override fun onResume() {
        super.onResume()
        binding.phoneInputLayout.hint = getString(R.string.phone)
        binding.passwordInputLayout.hint = getString(R.string.password)
        viewModel.getValidation().value?.let { showValidationErrors(it) }

        if (BuildConfig.DEBUG) {
            binding.phoneEditText.setText("9991848228")
            binding.passwordEditText.setText("123456")
        }
    }

    override fun showLoading(loading: Boolean) {
        binding.loadingLayout.root.isVisible = loading
    }

    override fun setData(data: User) {
        openMainScreen(data)
    }

    override fun showValidationErrors(validation: LoginValidationState) {
        binding.phoneInputLayout.error =
            if (validation.phone) "" else getString(R.string.invalid_phone)

        binding.passwordInputLayout.error =
            if (validation.password) "" else getString(R.string.invalid_field_empty)
    }

    override fun openLangDialog() {
        LocaleDialog().show(supportFragmentManager, LocaleDialog.TAG)
    }

    override fun openCountryDialog() {
        CountryDialog().show(supportFragmentManager, CountryDialog.TAG)
    }

    override fun changeCountry(county: Country) {
        binding.countryLogo.setImageResource(
            resources.getIdentifier(
                county.icon,
                "drawable",
                packageName
            )
        )
        binding.phoneInputLayout.prefixText = String.format(
            Locale.getDefault(),
            getString(R.string.country_code_placeholder),
            county.code
        )
    }

    override fun openMainScreen(user: User) {
        startActivity(Intent(this, MainActivity::class.java).apply {
            flags =
                Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
            putExtra(MainActivity.USER, user)
        })
    }

    override fun openRegisterScreen() {
        startActivity(Intent(this, RegisterActivity::class.java))
    }

    override fun openRestoreScreen() {
        startActivity(Intent(this, RestoreActivity::class.java))
    }

    override fun openDialer() {
        startActivity(Intent(Intent.ACTION_DIAL).apply {
            data = Uri.parse("tel:" + getString(R.string.support_phone))
        })
    }

}