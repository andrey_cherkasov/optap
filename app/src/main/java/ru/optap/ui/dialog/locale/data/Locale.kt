package ru.optap.ui.dialog.locale.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Locale(
    @PrimaryKey val id: Int,
    @ColumnInfo(name = "language") val language: String,
    @ColumnInfo(name = "locale") val locale: String,
    @ColumnInfo(name = "selected") var selected: Boolean
)