package ru.optap.ui.dialog.password

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ru.optap.data.Resource
import ru.optap.data.remote.AuthApiInterface
import ru.optap.ui.base.exception.ApiErrorException
import ru.optap.ui.base.viewmodel.BaseViewModel
import javax.inject.Inject

@HiltViewModel
class PasswordDialogViewModel @Inject constructor(
    private val authApi: AuthApiInterface
) : BaseViewModel() {

    private val changePasswordLiveData: MutableLiveData<Resource<String>> = MutableLiveData()
    private val validationLiveData: MutableLiveData<PasswordValidationState> =
        MutableLiveData(PasswordValidationState(true))

    val changePassword: LiveData<Resource<String>> = changePasswordLiveData
    val validation: LiveData<PasswordValidationState> = validationLiveData

    fun changePassword(currentPassword: String, newPassword: String) {
        validationLiveData.value = PasswordValidationState(newPassword.length >= 6)
        if (validation.value!!.isInvalidData()) return

        disposable.add(
            authApi.changePassword(currentPassword, newPassword)
                .subscribeOn(Schedulers.io())
                .doOnSubscribe { changePasswordLiveData.value = Resource.loading(null) }
                .doOnSuccess { if (!it.meta.success) throw ApiErrorException(it.meta.message) }
                .map { it.meta.message }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { changePasswordLiveData.value = Resource.success(it) },
                    { t -> changePasswordLiveData.value = Resource.error(t, null) }
                )
        )
    }

}