package ru.optap.ui.dialog.terms

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import dagger.hilt.android.AndroidEntryPoint
import ru.optap.data.model.User
import ru.optap.databinding.DialogTermsOfUseBinding


@AndroidEntryPoint
class TermsOfUseDialog : DialogFragment() {

    companion object {
        private const val USER_TYPE = "USER_TYPE"
        const val TAG = "TermsOfUseDialog"
        fun getInstance(userType: Int): TermsOfUseDialog {
            return TermsOfUseDialog().apply {
                arguments = bundleOf(USER_TYPE to userType)
            }
        }
    }

    private var _binding: DialogTermsOfUseBinding? = null
    private val binding get() = _binding!!
    private val viewModel: TermsOfUseViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = DialogTermsOfUseBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.okButton.setOnClickListener { dismiss() }
        binding.loadingLayout.root.isVisible = true
        viewModel.getTerms().observe(this) {
            binding.termsText.loadData(it, "text/html", "UTF-8")
            binding.termsText.webViewClient = object : WebViewClient() {
                override fun onPageFinished(webView: WebView, url: String) {
                    binding.loadingLayout.root.isVisible = false
                }
            }
        }

        if (savedInstanceState == null) {
            viewModel.loadTerms(arguments?.getInt(USER_TYPE) ?: User.CUSTOMER)
        }
    }

    override fun onStart() {
        super.onStart()
        val dialog = dialog
        if (dialog != null) {
            dialog.window!!.setLayout(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

}