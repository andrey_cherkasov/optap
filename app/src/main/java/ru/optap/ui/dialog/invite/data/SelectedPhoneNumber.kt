package ru.optap.ui.dialog.invite.data

import ru.optap.ui.dialog.country.data.Country

data class SelectedPhoneNumber(
    val country: Country,
    val phoneNumber: String
)