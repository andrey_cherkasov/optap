package ru.optap.ui.dialog.password

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import dagger.hilt.android.AndroidEntryPoint
import ru.optap.R
import ru.optap.data.DataStatus
import ru.optap.databinding.DialogPasswordBinding

@AndroidEntryPoint
class PasswordDialog : DialogFragment(), PasswordView {

    private var _binding: DialogPasswordBinding? = null
    private val viewModel: PasswordDialogViewModel by viewModels()

    private val binding get() = _binding!!

    companion object {
        const val TAG = "PasswordDialog"
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = DialogPasswordBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.okButton.setOnClickListener {
            viewModel.changePassword(
                binding.passwordEditText.text.toString(),
                binding.newPasswordEditText.text.toString()
            )
        }
        binding.cancelButton.setOnClickListener { dismiss() }

        viewModel.validation.observe(viewLifecycleOwner) { showValidationErrors(it) }
        viewModel.changePassword.observe(viewLifecycleOwner) {
            binding.root.isEnabled = true
            when (it.status) {
                DataStatus.LOADING -> binding.root.isEnabled = false
                DataStatus.SUCCESS -> {
                    showToast(it.data)
                    dismiss()
                }
                DataStatus.ERROR -> showToast(it.throwable?.message)
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun showValidationErrors(validation: PasswordValidationState) {
        binding.newPasswordInputLayout.error =
            if (validation.newPassword) "" else getString(R.string.invalid_password)
    }

    private fun showToast(message: String?) =
        Toast.makeText(context, message ?: getString(R.string.error), Toast.LENGTH_SHORT).show()

}