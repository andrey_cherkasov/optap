package ru.optap.ui.dialog.password

interface PasswordView {
    fun showValidationErrors(validation: PasswordValidationState)
}