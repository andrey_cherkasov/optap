package ru.optap.ui.dialog.support

import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.text.SpannableString
import android.text.Spanned
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import ru.optap.R
import ru.optap.databinding.DialogSupportBinding


class SupportDialog : DialogFragment() {

    companion object {
        const val TAG = "SupportDialog"
    }

    private var _binding: DialogSupportBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = DialogSupportBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.okButton.setOnClickListener { dismiss() }

        val spannableString =
            SpannableString(getString(R.string.support_text))
        spannableString.setSpan(
            object : ClickableSpan() {
                override fun onClick(widget: View) = openDialer()

                override fun updateDrawState(ds: TextPaint) {
                    ds.color = Color.BLUE
                    ds.isUnderlineText = false
                }
            },
            resources.getInteger(R.integer.support_phone_start),
            resources.getInteger(R.integer.support_phone_end),
            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
        )

        binding.text.text = spannableString
        binding.text.movementMethod = LinkMovementMethod.getInstance()
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    fun openDialer() {
        startActivity(Intent(Intent.ACTION_DIAL).apply {
            data = Uri.parse("tel:" + getString(R.string.support_phone_2))
        })
    }

}