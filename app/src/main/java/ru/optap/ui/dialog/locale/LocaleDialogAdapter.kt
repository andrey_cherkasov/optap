package ru.optap.ui.dialog.locale

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import ru.optap.ui.dialog.locale.data.Locale
import ru.optap.databinding.ItemDialogLocaleBinding
import ru.optap.ui.base.listview.adapter.OnItemListViewClickListener

class LocaleDialogAdapter(
    val onItemListViewClickListener: OnItemListViewClickListener<Locale>,
    diffCallback: DiffUtil.ItemCallback<Locale>
) : ListAdapter<Locale, LocaleDialogAdapter.LocaleViewHolder>(diffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = LocaleViewHolder(
        ItemDialogLocaleBinding.inflate(LayoutInflater.from(parent.context), parent, false)
    )

    override fun onBindViewHolder(holder: LocaleViewHolder, position: Int) =
        holder.onBind(getItem(position), position)

    override fun getItemCount(): Int = currentList.size


    inner class LocaleViewHolder(private val binding: ItemDialogLocaleBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun onBind(locale: Locale, position: Int) {
            binding.text.text = locale.language
            binding.check.isChecked = locale.selected
            binding.root.setOnClickListener {
                if (!locale.selected) onItemListViewClickListener.onClick(locale.copy(), position)
            }
        }
    }

}