package ru.optap.ui.dialog.locale

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.DiffUtil
import ru.optap.R
import ru.optap.ui.dialog.locale.data.Locale
import ru.optap.databinding.DialogLocaleBinding

class LocaleDialog : DialogFragment() {

    private val viewModel: LocaleDialogViewModel by activityViewModels()
    private var _binding: DialogLocaleBinding? = null

    private val binding get() = _binding!!

    companion object {
        const val TAG = "LocaleDialog"
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = DialogLocaleBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val activity = requireActivity()
        binding.header.text = getString(R.string.app_lang)
        val adapter = LocaleDialogAdapter(
            { locale, _ -> viewModel.changeLocale(locale) },
            object : DiffUtil.ItemCallback<Locale>() {
                override fun areItemsTheSame(oldItem: Locale, newItem: Locale): Boolean =
                    oldItem.id == newItem.id

                override fun areContentsTheSame(oldItem: Locale, newItem: Locale): Boolean =
                    oldItem == newItem
            }
        )
        binding.list.adapter = adapter
        viewModel.getLocales().observe(activity) { adapter.submitList(it.toMutableList()) }
        viewModel.getSelectedLocale().observe(activity) { activity.recreate() }
        binding.okButton.setOnClickListener { dismiss() }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}