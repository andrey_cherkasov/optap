package ru.optap.ui.dialog.product

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import ru.optap.ui.base.viewmodel.BaseViewModel
import ru.optap.ui.cart.CartRepository
import ru.optap.ui.catalog.data.CategoryVO
import ru.optap.ui.category.data.ProductVO
import javax.inject.Inject

@HiltViewModel
class ProductViewModel @Inject constructor(
    private val cartRepository: CartRepository
) : BaseViewModel() {

    private lateinit var category: CategoryVO
    private val productLiveData: MutableLiveData<ProductVO> = MutableLiveData()

    val product: LiveData<ProductVO> get() = productLiveData

    fun setProduct(product: ProductVO, category: CategoryVO) {
        productLiveData.value = product
        this.category = category
    }

    fun setCount(count: Int) {
        productLiveData.value = product.value?.copy(
            count = count,
            countString = count.toString()
        )
    }

    fun changeCountType(type: Int) {
        productLiveData.value = product.value?.copy(typeId = type)
    }

    fun addProduct() = cartRepository.addProduct(product.value!!, category)

}