package ru.optap.ui.dialog.invite.data

data class Invite(
    val phone: String,
    val message: String
)