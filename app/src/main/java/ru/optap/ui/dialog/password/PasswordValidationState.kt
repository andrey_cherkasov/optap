package ru.optap.ui.dialog.password

data class PasswordValidationState(
    var newPassword: Boolean
) {
    fun isInvalidData(): Boolean = !newPassword
}