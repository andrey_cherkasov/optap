package ru.optap.ui.dialog.terms

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ru.optap.data.model.ResponseData
import ru.optap.data.model.User
import ru.optap.data.remote.AuthApiInterface
import ru.optap.ui.base.exception.ApiErrorException
import ru.optap.ui.base.viewmodel.BaseViewModel
import javax.inject.Inject

@HiltViewModel
class TermsOfUseViewModel @Inject constructor(
    private val authApi: AuthApiInterface
) : BaseViewModel() {

    private val termsTextLiveData: MutableLiveData<String> = MutableLiveData()

    fun getTerms(): LiveData<String> = termsTextLiveData

    fun loadTerms(type: Int) {
        val termsSingle: Single<ResponseData<String>> =
            if (type == User.VENDOR) authApi.termsVendor() else authApi.termsCustomer()
        disposable.add(
            termsSingle.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSuccess { if (!it.meta.success) throw ApiErrorException(it.meta.message) }
                .map { it.data }
                .subscribe({ termsTextLiveData.value = it }, {})
        )
    }

}