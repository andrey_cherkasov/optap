package ru.optap.ui.dialog.country

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ru.optap.data.local.db.CountryDao
import ru.optap.ui.base.viewmodel.BaseViewModel
import ru.optap.ui.dialog.country.data.Country
import javax.inject.Inject

@HiltViewModel
class CountryDialogViewModel @Inject constructor(
    countryDao: CountryDao
) : BaseViewModel() {

    private val countriesLiveData: MutableLiveData<List<Country>> = MutableLiveData()
    private val selectedCountryLiveData: MutableLiveData<Country> = MutableLiveData()

    fun getCountries(): LiveData<List<Country>> = countriesLiveData
    fun getSelectCountry(): LiveData<Country> = selectedCountryLiveData

    init {
        disposable.add(
            countryDao.getCountries()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    countriesLiveData.value = it
                    selectedCountryLiveData.value = it[0]
                }, {})
        )
    }

    fun selectCountry(country: Country) {
        selectedCountryLiveData.value = country
    }

}