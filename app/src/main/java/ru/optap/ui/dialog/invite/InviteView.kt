package ru.optap.ui.dialog.invite

import ru.optap.ui.dialog.country.data.Country
import ru.optap.ui.dialog.invite.data.Invite
import ru.optap.ui.dialog.invite.data.InviteValidationState

interface InviteView {
    fun showValidationErrors(validation: InviteValidationState)
    fun openCountryDialog()
    fun changeCountry(country: Country)
    fun showChooseContactScreen()
    fun sendInvite()
    fun showInviteNext(invite: Invite)
}