package ru.optap.ui.dialog.invite.data

data class InviteValidationState(
    val phone: Boolean
) {
    fun isInvalidData(): Boolean = !phone
}