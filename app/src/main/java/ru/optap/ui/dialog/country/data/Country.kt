package ru.optap.ui.dialog.country.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Country(
    @PrimaryKey val id: Int,
    @ColumnInfo(name = "name") val name: String,
    @ColumnInfo(name = "prefix") val prefix: String,
    @ColumnInfo(name = "code") val code: String,
    @ColumnInfo(name = "icon") val icon: String,
)