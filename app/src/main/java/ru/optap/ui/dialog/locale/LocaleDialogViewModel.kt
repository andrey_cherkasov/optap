package ru.optap.ui.dialog.locale

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ru.optap.data.local.db.LocaleDao
import ru.optap.ui.dialog.locale.data.Locale
import ru.optap.ui.base.viewmodel.BaseViewModel
import ru.optap.ui.base.SingleLiveEvent
import java.util.concurrent.TimeUnit
import javax.inject.Inject

@HiltViewModel
class LocaleDialogViewModel @Inject constructor(
    private val localeDao: LocaleDao
) : BaseViewModel() {

    private val localesLiveData: MutableLiveData<List<Locale>> = MutableLiveData()
    private val selectedLocaleLiveData: SingleLiveEvent<Locale> = SingleLiveEvent()

    init {
        disposable.addAll(
            localeDao.getAll().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { localesLiveData.value = it },
                    { localesLiveData.value = arrayListOf() }
                )
        )
    }

    fun getLocales(): LiveData<List<Locale>> = localesLiveData
    fun getSelectedLocale(): LiveData<Locale> = selectedLocaleLiveData

    fun changeLocale(locale: Locale) {
        locale.selected = true
        disposable.add(
            localeDao.setSelected(locale.id)
                .subscribeOn(Schedulers.io())
                .delay(100, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ selectedLocaleLiveData.value = locale }, { t -> t.printStackTrace() })
        )
    }

}