package ru.optap.ui.dialog.country

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ru.optap.R
import ru.optap.databinding.ItemDialogCountryBinding
import ru.optap.ui.base.listview.adapter.OnItemListViewClickListener
import ru.optap.ui.dialog.country.data.Country
import java.util.*


class CountryDialogAdapter(
    private val items: List<Country>,
    private val listener: OnItemListViewClickListener<Country>
) : RecyclerView.Adapter<CountryDialogAdapter.CountryViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = CountryViewHolder(
        ItemDialogCountryBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
    )

    override fun onBindViewHolder(holder: CountryViewHolder, position: Int) =
        holder.onBind(items[position], position)

    override fun getItemCount() = items.size


    inner class CountryViewHolder(private val binding: ItemDialogCountryBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun onBind(item: Country, position: Int) {
            val context = binding.root.context
            val icon: Int = context.resources.getIdentifier(
                item.icon, "drawable", context.packageName
            )

            binding.text.text = item.name
            binding.image.setImageResource(icon)
            binding.code.text = String.format(
                Locale.getDefault(),
                context.resources.getString(R.string.country_code_placeholder),
                item.code
            )

            binding.root.setOnClickListener { listener.onClick(item, position) }
        }
    }

}