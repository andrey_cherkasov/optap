package ru.optap.ui.dialog.invite

import android.annotation.SuppressLint
import android.app.Application
import android.database.Cursor
import android.net.Uri
import android.provider.ContactsContract
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ru.optap.R
import ru.optap.data.Resource
import ru.optap.data.local.SharedPreferencesHelper
import ru.optap.data.local.db.CountryDao
import ru.optap.data.model.User
import ru.optap.data.remote.DataApiInterface
import ru.optap.ui.base.viewmodel.BaseViewModel
import ru.optap.ui.dialog.country.data.Country
import ru.optap.ui.dialog.invite.data.Invite
import ru.optap.ui.dialog.invite.data.InviteValidationState
import ru.optap.ui.dialog.invite.data.SelectedPhoneNumber
import javax.inject.Inject

@HiltViewModel
class InviteDialogViewModel @Inject constructor(
    private val app: Application,
    private val dataApi: DataApiInterface,
    private val countryDao: CountryDao,
    preferencesHelper: SharedPreferencesHelper,
) : BaseViewModel() {

    private val userLiveData: MutableLiveData<User> = MutableLiveData()
    private val inviteLiveData: MutableLiveData<Invite> = MutableLiveData()
    private val selectedPhoneNumberLiveData: MutableLiveData<Resource<SelectedPhoneNumber>> =
        MutableLiveData()
    private val validationLiveData: MutableLiveData<InviteValidationState> =
        MutableLiveData(InviteValidationState(true))

    val user: LiveData<User> = userLiveData
    val invite: LiveData<Invite> = inviteLiveData
    val selectedPhoneNumber: LiveData<Resource<SelectedPhoneNumber>> get() = selectedPhoneNumberLiveData
    val validation: LiveData<InviteValidationState> get() = validationLiveData

    init {
        userLiveData.value = preferencesHelper.loadUser()
    }

    @SuppressLint("Range")
    fun getPhoneNumber(uri: Uri) {
        disposable.add(Single.fromCallable {
            var clearPhoneNumber: String? = null
            var country: Country? = null
            val cursor: Cursor? = app.contentResolver?.query(
                uri,
                null,
                null,
                null,
                null
            )

            if (cursor != null && cursor.moveToFirst()) {
                val contactId =
                    cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID))
                val idResults =
                    cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))

                val phoneCursor: Cursor?
                if (idResults.toInt() == 1) {
                    phoneCursor = app.contentResolver!!.query(
                        ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                        null,
                        ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + contactId,
                        null,
                        null
                    )

                    if (phoneCursor != null && phoneCursor.moveToNext()) {
                        var contactNumber = phoneCursor.getString(
                            phoneCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)
                        )

                        contactNumber = contactNumber.trim().replace("[^0-9]".toRegex(), "")
                        var code = contactNumber.substring(0, contactNumber.length - 10)
                        if (code == "8") code = "7"
                        clearPhoneNumber = contactNumber.substring(contactNumber.length - 10)
                        country = countryDao.getCountry(code)

                        phoneCursor.close()
                    }
                }
                cursor.close()
            }

            if (country != null && clearPhoneNumber?.length == 10) {
                SelectedPhoneNumber(country, clearPhoneNumber)
            } else {
                throw Exception()
            }
        }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                selectedPhoneNumberLiveData.value = Resource.success(it)
                validationLiveData.value = InviteValidationState(true)
            }, { t -> selectedPhoneNumberLiveData.value = Resource.error(t, null) })
        )
    }

    fun sendInvite(country: Country, phone: String) {
        val login = phone.trim().replace("[^0-9]".toRegex(), "")
        validationLiveData.value = InviteValidationState(login.length == 10)
        if (validationLiveData.value!!.isInvalidData()) return

        if (user.value!!.role == User.VENDOR) {
            disposable.add(
                dataApi.sendSms(phone, "")
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({
                        inviteLiveData.value = Invite(
                            country.code + phone,
                            app.getString(R.string.invite_text_vendor)
                        )
                    }, { t -> t.printStackTrace() })
            )
        } else {
            inviteLiveData.value = Invite(
                country.code + phone,
                app.getString(R.string.invite_text_customer)
            )
        }
    }

}