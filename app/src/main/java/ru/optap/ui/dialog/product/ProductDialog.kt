package ru.optap.ui.dialog.product

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import dagger.hilt.android.AndroidEntryPoint
import ru.optap.R
import ru.optap.databinding.DialogProductBinding
import ru.optap.ui.catalog.data.CategoryVO
import ru.optap.ui.category.data.ProductVO
import kotlin.math.max

@AndroidEntryPoint
class ProductDialog : DialogFragment() {

    companion object {
        const val TAG = "ProductDialog"
        const val PRODUCT = "PRODUCT"
        const val CATEGORY = "CATEGORY"

        fun getInstance(product: ProductVO, category: CategoryVO): ProductDialog {
            return ProductDialog().apply {
                arguments = bundleOf(PRODUCT to product, CATEGORY to category)
            }
        }
    }

    private var _binding: DialogProductBinding? = null
    private val viewModel: ProductViewModel by viewModels()

    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = DialogProductBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val selectedColor = ContextCompat.getColor(requireContext(), R.color.secondaryColor)
        val unselectedColor = binding.pack.textColors.defaultColor

        val product = arguments?.getParcelable<ProductVO>(PRODUCT)
        val category = arguments?.getParcelable<CategoryVO>(CATEGORY)
        product?.let {
            Glide.with(this)
                .load(it.image)
                .placeholder(R.drawable.preloader)
                .transform(CircleCrop())
                .into(binding.image)

            binding.name.text = if (it.name == "") getString(R.string.untitled) else it.name
            binding.price.text = it.priceString
            binding.packCount.text = getString(R.string.amount_per_pack_value, it.packCount)
            binding.boxCount.text = getString(R.string.amount_per_box_value, it.boxCount)
            binding.countEditText.setText(it.count.toString())

            binding.pack.isEnabled = it.packCount != 0
            binding.box.isEnabled = it.boxCount != 0

            viewModel.setProduct(it, category!!)
        }

        viewModel.product.observe(this) {
            val factor = when (it.typeId) {
                1 -> it.packCount
                2 -> it.boxCount
                else -> 1
            }
            binding.totalPrice.text = (it.price * it.count * factor).toString()

            binding.pcs.setTextColor(unselectedColor)
            binding.pack.setTextColor(unselectedColor)
            binding.box.setTextColor(unselectedColor)

            when (it.typeId) {
                1 -> binding.pack.setTextColor(selectedColor)
                2 -> binding.box.setTextColor(selectedColor)
                else -> binding.pcs.setTextColor(selectedColor)
            }
        }

        binding.minus.setOnClickListener {
            binding.countEditText.setText(max(viewModel.product.value!!.count - 1, 0).toString())
        }
        binding.plus.setOnClickListener {
            binding.countEditText.setText(viewModel.product.value?.count?.plus(1).toString())
        }

        binding.pcs.setOnClickListener { viewModel.changeCountType(0) }
        binding.pack.setOnClickListener { viewModel.changeCountType(1) }
        binding.box.setOnClickListener { viewModel.changeCountType(2) }

        binding.countEditText.addTextChangedListener { text ->
            viewModel.setCount(text.toString().toIntOrNull() ?: 0)
        }

        binding.okButton.setOnClickListener {
            viewModel.addProduct()
            Toast.makeText(context, R.string.product_added_to_cart, Toast.LENGTH_SHORT).show()
            dismiss()
        }
    }

    override fun onStart() {
        super.onStart()
        dialog?.let {
            it.window!!.setLayout(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}