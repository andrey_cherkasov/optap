package ru.optap.ui.dialog.invite

import android.Manifest
import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import dagger.hilt.android.AndroidEntryPoint
import ru.optap.R
import ru.optap.data.DataStatus
import ru.optap.databinding.DialogInviteBinding
import ru.optap.ui.dialog.country.CountryDialog
import ru.optap.ui.dialog.country.CountryDialogViewModel
import ru.optap.ui.dialog.country.data.Country
import ru.optap.ui.dialog.invite.contract.ContactResultContract
import ru.optap.ui.dialog.invite.data.Invite
import ru.optap.ui.dialog.invite.data.InviteValidationState
import ru.tinkoff.decoro.MaskImpl
import ru.tinkoff.decoro.parser.UnderscoreDigitSlotsParser
import ru.tinkoff.decoro.slots.Slot
import ru.tinkoff.decoro.watchers.MaskFormatWatcher
import java.util.*

@AndroidEntryPoint
class InviteDialog : DialogFragment(), InviteView {

    companion object {
        const val TAG = "InviteDialog"
    }

    private var _binding: DialogInviteBinding? = null
    private val viewModel: InviteDialogViewModel by viewModels()
    private val countryViewModel: CountryDialogViewModel by activityViewModels()

    private val binding get() = _binding!!

    private val requestPermissionLauncher =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) {
            if (it) launcher.launch(null)
        }

    private val launcher = registerForActivityResult(ContactResultContract()) {
        it?.let { it1 -> viewModel.getPhoneNumber(it1) }
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = DialogInviteBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val slots: Array<Slot> = UnderscoreDigitSlotsParser().parseSlots("(___) ___ - __ - __")
        val mask: MaskImpl = MaskImpl.createTerminated(slots)
        mask.isHideHardcodedHead = true
        MaskFormatWatcher(mask).installOn(binding.phoneEditText)

        binding.okButton.setOnClickListener { sendInvite() }
        binding.countryLogo.setOnClickListener { openCountryDialog() }
        binding.chooseContact.setOnClickListener { showChooseContactScreen() }

        countryViewModel.getCountries().observe(this) {
            binding.countryLogo.visibility = View.VISIBLE
        }

        countryViewModel.getSelectCountry().observe(this) { changeCountry(it) }
        viewModel.validation.observe(viewLifecycleOwner) { showValidationErrors(it) }
        viewModel.invite.observe(viewLifecycleOwner) { showInviteNext(it) }
        viewModel.selectedPhoneNumber.observe(viewLifecycleOwner) {
            when (it.status) {
                DataStatus.LOADING -> {}
                DataStatus.SUCCESS -> it.data?.let { it1 ->
                    changeCountry(it1.country)
                    binding.phoneEditText.setText(it1.phoneNumber)
                }
                DataStatus.ERROR -> showToast(getString(R.string.unsupported_phone))
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun showValidationErrors(validation: InviteValidationState) {
        binding.phoneInputLayout.error =
            if (validation.phone) "" else getString(R.string.invalid_phone)
    }

    override fun openCountryDialog() =
        CountryDialog().show(parentFragmentManager, CountryDialog.TAG)

    override fun changeCountry(country: Country) {
        binding.countryLogo.setImageResource(
            resources.getIdentifier(
                country.icon,
                "drawable",
                requireContext().packageName
            )
        )
        binding.phoneInputLayout.prefixText = String.format(
            Locale.getDefault(),
            getString(R.string.country_code_placeholder),
            country.code
        )
    }

    override fun showChooseContactScreen() {
        when (PackageManager.PERMISSION_GRANTED) {
            ContextCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.READ_CONTACTS
            ) -> launcher.launch(null)
            else -> requestPermissionLauncher.launch(Manifest.permission.READ_CONTACTS)
        }
    }

    override fun sendInvite() {
        val country = countryViewModel.getSelectCountry().value!!
        var clearPhoneNumber = binding.phoneEditText.text.toString()
        clearPhoneNumber = clearPhoneNumber.trim().replace("[^0-9]".toRegex(), "")
        viewModel.sendInvite(country, clearPhoneNumber)
    }

    override fun showInviteNext(invite: Invite) {
        when (binding.inviteType.checkedRadioButtonId) {
            R.id.sms -> {
                val sendIntent = Intent(Intent.ACTION_VIEW)
                sendIntent.data = Uri.parse("sms:+${invite.phone}")
                sendIntent.putExtra("sms_body", invite.message)
                startActivity(sendIntent)
            }
            R.id.whatsapp -> {
                val uri = Uri.parse("https://wa.me/+${invite.phone}?text=${invite.message}")
                try {
                    startActivity(Intent(Intent.ACTION_VIEW, uri))
                } catch (e: ActivityNotFoundException) {
                    try {
                        startActivity(
                            Intent(
                                Intent.ACTION_VIEW,
                                Uri.parse("market://details?id=com.whatsapp")
                            )
                        )
                    } catch (e1: ActivityNotFoundException) {
                        startActivity(
                            Intent(
                                Intent.ACTION_VIEW,
                                Uri.parse("https://play.google.com/store/apps/details?id=com.whatsapp")
                            )
                        )
                    }
                }
            }
        }
        dismiss()
    }

    private fun showToast(message: String) {
        Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
    }

}