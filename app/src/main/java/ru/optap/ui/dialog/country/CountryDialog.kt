package ru.optap.ui.dialog.country

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.activityViewModels
import ru.optap.R
import ru.optap.databinding.DialogCountryBinding
import ru.optap.ui.base.listview.adapter.OnItemListViewClickListener
import ru.optap.ui.dialog.country.data.Country

class CountryDialog : DialogFragment(), OnItemListViewClickListener<Country> {

    private var _binding: DialogCountryBinding? = null
    private val viewModel: CountryDialogViewModel by activityViewModels()

    private val binding get() = _binding!!

    companion object {
        const val TAG = "CountryDialog"
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = DialogCountryBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.header.text = getString(R.string.country)
        binding.list.adapter =
            viewModel.getCountries().value?.let { CountryDialogAdapter(it, this::onClick) }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onClick(item: Country, position: Int) {
        viewModel.selectCountry(item)
        dismiss()
    }

}