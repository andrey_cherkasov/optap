package ru.optap.ui.restore

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.activity.viewModels
import androidx.core.view.isVisible
import dagger.hilt.android.AndroidEntryPoint
import ru.optap.R
import ru.optap.data.DataStatus
import ru.optap.ui.dialog.country.data.Country
import ru.optap.data.model.Meta
import ru.optap.databinding.ActivityRestoreBinding
import ru.optap.ui.base.view.BaseActivity
import ru.optap.ui.base.view.LoadingView
import ru.optap.ui.dialog.country.CountryDialog
import ru.optap.ui.dialog.country.CountryDialogViewModel
import ru.optap.ui.dialog.locale.LocaleDialog
import ru.optap.ui.dialog.locale.LocaleDialogViewModel
import ru.optap.ui.login.LoginActivity
import ru.optap.ui.register.RegisterActivity
import ru.tinkoff.decoro.MaskImpl
import ru.tinkoff.decoro.parser.UnderscoreDigitSlotsParser
import ru.tinkoff.decoro.slots.Slot
import ru.tinkoff.decoro.watchers.MaskFormatWatcher
import java.util.*

@AndroidEntryPoint
class RestoreActivity : BaseActivity(), RestoreView, LoadingView<Meta> {

    private lateinit var binding: ActivityRestoreBinding
    private val viewModel: RestoreViewModel by viewModels()
    private val localeViewModel: LocaleDialogViewModel by viewModels()
    private val countryViewModel: CountryDialogViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRestoreBinding.inflate(LayoutInflater.from(this))
        setContentView(binding.root)

        val slots: Array<Slot> = UnderscoreDigitSlotsParser().parseSlots("(___) ___ - __ - __")
        val mask: MaskImpl = MaskImpl.createTerminated(slots)
        mask.isHideHardcodedHead = true
        MaskFormatWatcher(mask).installOn(binding.phoneEditText)

        binding.signUpButton.setOnClickListener { openRegisterScreen() }
        binding.supportTelTextView.setOnClickListener { openDialer() }
        binding.langImageButton.setOnClickListener { openLangDialog() }
        binding.countryLogo.setOnClickListener { openCountryDialog() }
        binding.loginButton.setOnClickListener { openLoginScreen() }
        binding.restoreButton.setOnClickListener {
            viewModel.restore(
                binding.phoneEditText.text.toString()
            )
        }

        viewModel.getPhoneValidation().observe(this) { showPhoneError(it) }
        viewModel.getRestore().observe(this) {
            showLoading(false)
            when (it.status) {
                DataStatus.LOADING -> showLoading(true)
                DataStatus.SUCCESS -> it.data?.let { it1 -> setData(it1) }
                DataStatus.ERROR -> handleError(it.throwable)
            }
        }

        localeViewModel.getLocales().observe(this) {
            binding.langImageButton.visibility = View.VISIBLE
        }
        countryViewModel.getCountries().observe(this) {
            binding.countryLogo.visibility = View.VISIBLE
        }

        countryViewModel.getSelectCountry().observe(this) { changeCountry(it) }
    }

    override fun onResume() {
        super.onResume()
        binding.phoneInputLayout.hint = getString(R.string.phone)
        viewModel.getPhoneValidation().value?.let { showPhoneError(it) }
    }

    override fun showLoading(loading: Boolean) {
        binding.loadingLayout.root.isVisible = loading
    }

    override fun setData(data: Meta) {
        showToast(data.message)
        openLoginScreen()
    }

    override fun showPhoneError(isValid: Boolean) {
        binding.phoneInputLayout.error = if (isValid) "" else getString(R.string.invalid_phone)
    }

    override fun openRegisterScreen() {
        startActivity(Intent(this, RegisterActivity::class.java))
    }

    override fun openLoginScreen() {
        startActivity(Intent(this, LoginActivity::class.java))
    }

    override fun openDialer() {
        startActivity(Intent(Intent.ACTION_DIAL).apply {
            data = Uri.parse("tel:" + getString(R.string.support_phone))
        })
    }

    override fun openLangDialog() {
        LocaleDialog().show(supportFragmentManager, LocaleDialog.TAG)
    }

    override fun openCountryDialog() {
        CountryDialog().show(supportFragmentManager, CountryDialog.TAG)
    }

    override fun changeCountry(county: Country) {
        binding.countryLogo.setImageResource(
            resources.getIdentifier(
                county.icon,
                "drawable",
                packageName
            )
        )
        binding.phoneInputLayout.prefixText = String.format(
            Locale.getDefault(),
            getString(R.string.country_code_placeholder),
            county.code
        )
    }

}