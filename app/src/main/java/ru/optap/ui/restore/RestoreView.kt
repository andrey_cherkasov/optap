package ru.optap.ui.restore

import ru.optap.ui.dialog.country.data.Country

interface RestoreView {
    fun showPhoneError(isValid: Boolean)
    fun openRegisterScreen()
    fun openLoginScreen()
    fun openDialer()
    fun openLangDialog()
    fun openCountryDialog()
    fun changeCountry(county: Country)
}