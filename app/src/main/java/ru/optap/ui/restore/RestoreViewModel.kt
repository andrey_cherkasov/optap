package ru.optap.ui.restore

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ru.optap.data.Resource
import ru.optap.data.model.Meta
import ru.optap.data.remote.AuthApiInterface
import ru.optap.ui.base.exception.ApiErrorException
import ru.optap.ui.base.viewmodel.BaseViewModel
import javax.inject.Inject

@HiltViewModel
class RestoreViewModel @Inject constructor(
    private val authApi: AuthApiInterface
) : BaseViewModel() {

    private val restoreLiveData: MutableLiveData<Resource<Meta>> = MutableLiveData()
    private val phoneValidationLiveData: MutableLiveData<Boolean> = MutableLiveData(true)

    fun getPhoneValidation(): LiveData<Boolean> = phoneValidationLiveData
    fun getRestore(): LiveData<Resource<Meta>> = restoreLiveData

    fun restore(phone: String) {
        val login = phone.trim().replace("[^0-9]".toRegex(), "")
        if (login.length != 10) {
            phoneValidationLiveData.value = false
            return
        }

        disposable.add(
            authApi.restore(login)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { restoreLiveData.value = Resource.loading(null) }
                .map { it.meta }
                .doOnSuccess { if (!it.success) throw  ApiErrorException(it.message) }
                .subscribe(
                    { restoreLiveData.value = Resource.success(it) },
                    { restoreLiveData.value = Resource.error(it, null) }
                )
        )
    }

}