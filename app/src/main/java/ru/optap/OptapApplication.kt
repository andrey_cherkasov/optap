package ru.optap

import android.app.Application
import androidx.hilt.work.HiltWorkerFactory
import androidx.work.Configuration
import dagger.hilt.android.HiltAndroidApp
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import ru.optap.data.DataGenerator
import ru.optap.data.local.db.CountryDao
import ru.optap.data.local.db.LocaleDao
import javax.inject.Inject

@HiltAndroidApp
class OptapApplication : Application(), Configuration.Provider {

    @Inject
    lateinit var localeDao: LocaleDao

    @Inject
    lateinit var countryDao: CountryDao

    @Inject
    lateinit var workerFactory: HiltWorkerFactory

    private val disposable: CompositeDisposable = CompositeDisposable()

    override fun onCreate() {
        super.onCreate()

        if (localeDao.getAll().blockingFirst().isEmpty()) {
            localeDao.insertAll(DataGenerator.getAvailableLocales()).blockingAwait()
        }

        disposable.add(
            countryDao.getCountries()
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe({
                    if (it.isEmpty()) {
                        disposable.add(
                            countryDao.insertAll(DataGenerator.getCountries())
                                .subscribeOn(Schedulers.io())
                                .observeOn(Schedulers.io())
                                .subscribe()
                        )
                    }
                }, { t -> t.printStackTrace() })
        )
    }

    override fun getWorkManagerConfiguration() = Configuration.Builder()
        .setWorkerFactory(workerFactory)
        .build()

}